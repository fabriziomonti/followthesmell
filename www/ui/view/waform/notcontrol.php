<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waNotControlView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlNotControl $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; '
			>
			<?php
			}
			
		?>
			<div 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				class='waform_notcontrol <?=$this->getControlClass()?>' 
				<?=$this->getControlAttributes()?>
				style=''
			>
				<?=$this->value?>
			</div>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlNotControl $data)
		{
		return null;
		}
		
	//**************************************************************************
	}
//******************************************************************************


