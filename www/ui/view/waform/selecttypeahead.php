<?php 
namespace followthesmell;
	
require_once __DIR__ . "/select.php";

//******************************************************************************
class waSelectTypeaheadView extends waSelectView 
	{
	protected static $jsLoaded = false;
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlSelectTypeahead $data)
		{
		waControlView::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}

		
		?>

			<input 
				type='hidden' 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				value='<?=$this->value?>'
			>
			<input 
				type='text' 
				data-provide='typeahead'
				autocomplete='off'
				id='<?=$this->form->name?>_<?=$this->name?>_typeahead' 
				name='<?=$this->name?>_typeahead' 
				value='<?=htmlspecialchars($this->text, ENT_QUOTES | ENT_HTML5)?>'
				<?=$this->getControlAttributes()?> 
				style=''
				class='form-control <?=$this->getControlClass()?>'
			>

		</div>

		<?php
		
		if (!self::$jsLoaded)
			{
			?>
			<script type="text/javascript" src="ui/js/vendor/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
			<?php
			self::$jsLoaded = true;
			}
			
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


