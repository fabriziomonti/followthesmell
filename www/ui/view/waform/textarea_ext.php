<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waTextArea_extView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlTextArea $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<textarea 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				cols='<?=$this->columns?>' 
				rows='<?=$this->rows?>' 
				<?=$this->getControlAttributes()?> 
				style='<?=$this->getControlStyle()?>'
				class='form-control mceEditor <?=$this->getControlClass()?>'><?=$this->value?></textarea>
		</div>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


