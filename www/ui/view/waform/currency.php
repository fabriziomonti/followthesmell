<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waCurrencyView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlCurrency $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$maxChars = $this->integerNr + 
						floor($this->integerNr / 3) +
						($this->integerNr % 3 > 0 ? 1 : 0) +
						$this->decimalNr;
		$value = "";
		if ($this->value || !$this->emptyOnZero)
			{
			$value = number_format($this->value, $this->decimalNr, ",", ".");
			}

		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group col-xs-12 col-sm-3 col-md-2 col-lg-2'>
				<input 
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$value?>' 
					maxlength='<?=$maxChars?>' 
					size='<?=$maxChars?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: right;'
					class='form-control <?=$this->getControlClass()?>'
				/>
			</div>
		</div>

		<!--	parcheggiamo le proprieta' particolari nel controllo html, in modo che-->
		<!--	poi la classe applicativa  possa ritrovarli-->
		<script type='text/Javascript'>
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.decimalNr  =  "<?=$this->decimalNr?>";
		</script>
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlCurrency $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$retval = str_replace(".", "", $_POST[$data->name]);
		$retval = str_replace(",", ".", $retval);
		return $retval;
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


