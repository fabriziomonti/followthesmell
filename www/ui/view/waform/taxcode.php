<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waTaxCodeView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlTaxCode $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<input 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				value='<?=htmlspecialchars($this->value, ENT_QUOTES | ENT_HTML5)?>'
				maxlength='<?=$this->maxChars?>' 
				size='<?=$this->maxChars?>' 
				<?=$this->getControlAttributes()?> 
				style=''
				class='form-control <?=$this->getControlClass()?>'
			/>
		</div>

		<!--	parcheggiamo le proprieta' particolari nel controllo html, in modo che-->
		<!--	poi la classe applicativa  possa ritrovarli-->
		<script type='text/Javascript'>
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.manageCF  =  "<?=$this->manageCF?>";
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.managePI  =  "<?=$this->managePI?>";
		</script>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


