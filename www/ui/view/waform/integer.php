<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waIntegerView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlText $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group col-xs-12 col-sm-3 col-md-2 col-lg-2'>
				<input 
					type='integer'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$this->value?>' 
					maxlength='<?=$this->maxChars?>' 
					size='<?=$this->maxChars?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: right; '
					class='form-control <?=$this->getControlClass()?>'
				/>
			</div>
		</div>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


