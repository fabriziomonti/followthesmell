<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waLabelView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlLabel $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if ($this->labelHaveControl())
			{
			?>
			<div 
				class='form-group'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='<?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
		<label 
			id='lbl_<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?> 
			style='<?=$this->getControlStyle()?>'
			class='<?=$this->getControlClass()?>'
		><?=$this->value?>
		
		</label>

		<?php if ($data->help) : 
			$a = 3;
			?>
				<a href="#" 
					style='<?=$this->getControlStyle()?>'
					onclick="document.waPage.showHelp('<?=$data->help->section?>', '<?=$data->help->page?>', '<?=$data->name?>', '<?=addslashes($data->help->title)?>', '<?= addslashes($data->value)?>')" 
					tabindex="-1"
				   >
					<span class="glyphicon glyphicon-info-sign"></span>
				</a>
		<?php endif; ?>
				
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlLabel $data)
		{
		return null;
		}
		
	//**************************************************************************
	}
//******************************************************************************


