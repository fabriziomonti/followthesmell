<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waBooleanView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControl $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$checked = $this->value ? "checked" : "";
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; '
			>
			<?php
			}
			
		?>
			<div class='input-group col-xs-1'>
				<input 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					type='checkbox' <?=$checked?> 
					<?=$this->getControlAttributes()?> 
					style='width: 1.5rem; height: 1.5rem; <?=$this->getControlStyle()?>'
				/>
			</div>
		</div>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControl $data)
		{
		return $_POST[$data->name] == 'on' ? 1 : 0;
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


