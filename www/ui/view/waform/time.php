<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waTimeView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlTime $data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		$value = $data->showSeconds ? $data->value : substr($data->value, 0, 5); 
		$format = $data->showSeconds ? "HH:mm:SS" : "HH:mm"; 
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date col-xs-12 col-sm-2 col-md-2 col-lg-2' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$value?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center; '
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							format: '<?=$format?>',
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlTime $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$value = trim($_POST[$data->name]);
		if (!$value)
			{
			return false;
			}
			
		$retval = substr($value, 0, 5) . ":";
		$retval .= $data->showSeconds ? substr($value, 6, 2) : "00";

		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


