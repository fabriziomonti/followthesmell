<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waPhoneView extends waControlView
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$value = json_decode($this->value);
		$value->prefix = $value->prefix ? $value->prefix : "+39";
		
		?>
		<div 
			class='form-group waform_phone_container'
			id='<?=$this->form->name?>_<?=$this->name?>_phone_container' 
			style='text-align: right; <?=$this->getControlStyle()?>'
		>
			
			
			<div class="">
				<div class="row">
					<div class='col-sm-4' style='text-align: left;'>
						<select 
							id='<?=$this->form->name?>_<?=$this->name?>_prefix' 
							name='<?=$this->name?>_prefix' 
							<?=$this->getControlAttributes()?> 
							class='form-control <?=$this->getControlClass()?>'
						>

							<option value=''></option>

							<?php
							foreach ($value->prefix_list as $key => $val)
								{
								?>
								<option value='<?=$key?>' <?=($key == $value->prefix ? "selected='selected'" : "")?>><?=$val?> <?=$key?></option>
								<?php
								}
							?>

						</select>
					</div>

					<div class='col-sm-8' style='text-align: left;'>
			
						<input 
							class='form-control <?=$this->getControlClass()?>'
							id='<?=$this->form->name?>_<?=$this->name?>_number' 
							name='<?=$this->name?>_number' 
							value='<?=$value->number?>'
							<?=$this->getControlAttributes()?> 
						>

					</div>
				</div>
			</div>
		</div>
		<div class="clearfix visible-sm"></div>

		<!--chiusura label--> 
		</div>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		$retval = new \stdClass();
		$retval->prefix = $_POST[$data->name . "_prefix"] ? $_POST[$data->name . "_prefix"] : "+39";
		$retval->number = $_POST[$data->name . "_number"];
			
		return $retval;
		}
		
		
	
	
	
	
	}	// fine classe waPhone
//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************



