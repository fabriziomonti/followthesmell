<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waUploadView extends waControlView 
	{
	protected static $jsLoaded = false;
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlUpload $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		$id = $this->form->name . "_" . $this->name;
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$id?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>

			<div id='waform_upload_container_<?=$id?>'>
				<input 
					id='<?=$id?>' 
					name='<?=$this->name?>' 
					type='file' 
					<?=$this->getControlAttributes()?> 
					style=''
					class='form-control file <?=$this->getControlClass()?>'
					data-show-upload='false'
				/>

				<input 
					type='checkbox' 
					name='waform_booleandeletefile_<?=$id?>' 
					value='1'
					style='display: none'
				/>

			</div>
				
		</div>

		<script type="text/javascript">
			// appoggiamo nel controllo un di info che ci saranno utili in fase
			// di inizializzazione 
			var ctrl = document.getElementById("<?=$id?>");
			ctrl.showPage = '<?=$this->showPage?>';
			ctrl.fileSize = '<?=$this->fileSize?>';
		</script>

		<?php
		
		if (!self::$jsLoaded)
			{
			?>
			<link rel="stylesheet" href="ui/js/vendor/bootstrap-fileinput/4.3.4/css/fileinput.min.css" />
			<script type="text/javascript" src="ui/js/vendor/bootstrap-fileinput/4.3.4/js/fileinput.min.js"></script>
			<?php
			self::$jsLoaded = true;
			}
			
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlUpload $data)
		{
		$retval = null;
		if ($_FILES[$data->name] || isset($_POST["waform_booleandeletefile_" . $this->form->name . "_$data->name"]))
			{
			$retval = (object) $_FILES[$data->name];
			$retval->delete = $_POST["waform_booleandeletefile_" . $this->form->name . "_$data->name"] ? 1 : 0;
			}
			
		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


