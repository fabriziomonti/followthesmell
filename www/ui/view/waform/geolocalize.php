<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waGeolocalizeView extends waControlView
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$value = json_decode($this->value);
		$components = $value->address_blob ? \followthesmell::normalizeAddress(json_decode($value->address_blob)->address_components) : new \stdClass();
		
		?>
		<div 
			class='form-group waform_geolocalize_container'
			id='<?=$this->form->name?>_<?=$this->name?>_geolocalize_container' 
			style='text-align: right; <?=$this->getControlStyle()?>'
		>

<!--			<textarea 
				class='form-control <?=$this->getControlClass()?> waform_geolocalize_complete_address'
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				<?=$this->getControlAttributes()?> 
				style=''
				autocomplete='off'
				placeholder="scrivi qui il tuo indirizzo completo"
			><?=htmlspecialchars($value->address, ENT_QUOTES | ENT_HTML5)?></textarea>-->

			<input
				class='form-control <?=$this->getControlClass()?> waform_geolocalize_complete_address'
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				<?=$this->getControlAttributes()?> 
				style=''
				autocomplete='uzza'
				placeholder="scrivi qui il tuo indirizzo completo"
				value="<?=htmlspecialchars($value->address, ENT_QUOTES | ENT_HTML5)?>"
			>

			<!--<br>-->

			<textarea 
				id='<?=$this->form->name?>_<?=$this->name?>_blob' 
				name='<?=$this->name?>_address_blob' 
				style='display: none;'
			><?=htmlspecialchars($value->address_blob, ENT_QUOTES | ENT_HTML5)?></textarea>
			
			<?php
			// https://possibile.atlassian.net/browse/VIVA-224 :
			//  bloccare i campi via e civico e note finché non viene scritto 
			//  qualcosa nel primo rigo.
			if (!$value->address_blob)
				{
				$this->readOnly = true;
				}
			
			?>
			
			<div class="form-group" style="display: none;">
				<div class="row">
					<div class='col-sm-8' style='text-align: left;'>
						indirizzo (via/viale/corso/piazza…)
						<input 
							class='form-control <?=$this->getControlClass()?>'
							id='<?=$this->form->name?>_<?=$this->name?>_street' 
							name='<?=$this->name?>_street' 
							value='<?=htmlspecialchars($components->street, ENT_QUOTES | ENT_HTML5)?>'
							<?=$this->getControlAttributes()?> 
							style=''
						>
					</div>

					<div class='col-sm-3' style='text-align: left;'>
						civico
						<input 
							class='form-control <?=$this->getControlClass()?>'
							id='<?=$this->form->name?>_<?=$this->name?>_house_number' 
							name='<?=$this->name?>_house_number' 
							value='<?=htmlspecialchars($components->house_number, ENT_QUOTES | ENT_HTML5)?>'
							<?=$this->getControlAttributes()?> 
							style=''
						>
					</div>			
				</div>
			</div>
			<div class="clearfix visible-sm"></div>

			<div class="form-group"  style="display: none;">
				<div class="row">
					<div class='col-sm-2' style='text-align: left;'>
						cap
						<input 
							class='form-control disabled'
							id='<?=$this->form->name?>_<?=$this->name?>_postal_code'
							name='<?=$this->name?>_postal_code' 
							value='<?=htmlspecialchars($components->postal_code, ENT_QUOTES | ENT_HTML5)?>'
							disabled='disabled'
							style=''
						>

					</div>				
					<div class='col-sm-7' style='text-align: left;'>
						città
						<input 
							class='form-control disabled'
							id='<?=$this->form->name?>_<?=$this->name?>_city' 
							name='<?=$this->name?>_city' 
							value='<?=htmlspecialchars($components->city, ENT_QUOTES | ENT_HTML5)?>'
							disabled='disabled'
							style=''
						>

					</div>

					<div class='col-sm-2' style='text-align: left;'>
						prov.
						<input 
							class='form-control disabled'
							id='<?=$this->form->name?>_<?=$this->name?>_province_short'
							name='<?=$this->name?>_province_short' 
							value='<?=htmlspecialchars($components->province_short, ENT_QUOTES | ENT_HTML5)?>'
							disabled='disabled'
							style=''
						>
					</div>
			
				</div>
			</div>
			<div class="clearfix visible-sm"></div>
			
			<div class="form-group" style="display: none">
				<div class="row">
					<div class='col-sm-11' style='text-align: left;'>
						note (interno, piano, c/o, ecc.)
						<input 
							class='form-control <?=$this->getControlClass()?>'
							id='<?=$this->form->name?>_<?=$this->name?>_notes' 
							name='<?=$this->name?>_notes' 
							value='<?=htmlspecialchars($components->notes, ENT_QUOTES | ENT_HTML5)?>'
							<?=$this->getControlAttributes()?> 
							style=''
						>
					</div>

				</div>
			</div>
			<div class="clearfix visible-sm"></div>

		</div>

		<!--chiusura label--> 
		</div>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		if (!isset($_POST[$data->name]) || !isset($_POST[$data->name . "_address_blob"]))
			return null;
		$retval = new \stdClass();
		$retval->address = $_POST[$data->name];
		$retval->address_blob = $_POST[$data->name . "_address_blob"];
			
		return $retval;
		}
		
		
	
	
	
	
	}	// fine classe waGeolocalize
//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************



