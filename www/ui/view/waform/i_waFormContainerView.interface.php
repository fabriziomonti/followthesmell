<?php
namespace followthesmell;

//***************************************************************************
//****  interfaccia i_waFormContainerView ********************************************
//***************************************************************************
/**
* i_waFormContainerView
*
* interfaccia che tutti i controlli container (frame, tab, ...) devono rispettare
*
*/
interface i_waFormContainerView
{
	/**
	 * apre il container
	 */
    public function open(\waLibs\waFormDataControlContainer $data);

	/**
	 * chiude il container
	 */
	public function close();
}