<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waCaptchaView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlText $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		// modifica: non leggiamo da sessione (casino...); scriviamo in tmp
		@mkdir (APPL_TMP_DIRECTORY . "/captchas");
		file_put_contents(APPL_TMP_DIRECTORY . "/captchas/$this->value", $_SESSION["WAFORM_CAPTCHA_CODE_$this->value"]);
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>

			<img 
				id='<?=$this->form->name?>_captcha_img_<?=$this->name?>' 
				src='<?=$this->form->waFormPath . "/../.."?>/ui/img/captcha.php?k=<?=$this->value?>' 
		/>
			<input 
				type='hidden' 
				name='k_<?=$this->name?>' 
				value='<?=htmlspecialchars($this->value, ENT_QUOTES | ENT_HTML5)?>'
			/>
			<input 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				maxlength='<?=$this->maxChars?>' 
				size='<?=$this->maxChars?>' 
				<?=$this->getControlAttributes()?> 
				style=''
				class='form-control <?=$this->getControlClass()?>'
			/>
		</div>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlText $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
			
		$retval = new \stdClass();
		$retval->k = $_POST["k_$data->name"];
		$retval->v = $_POST[$data->name];
		return $retval;
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


