<?php 
namespace followthesmell;
	
require_once __DIR__ . "/i_waFormContainerView.interface.php";
require_once __DIR__ . "/control.php";

//******************************************************************************
class waFrameView extends waControlView implements i_waFormContainerView
	{
	public $data;
	
	//**************************************************************************
	public function open(\waLibs\waFormDataControlContainer $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$css = $this->readOnly ? "waform_disabled" : "";
		$css .= $this->mandatory ? " waform_mandatory" : "";
		$disabled = $this->readOnly ? "disabled" : "";
		
		?>
		<div 
			id='<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?>
			style='<?=$this->getControlStyle()?>'
			class='waframe <?=$this->getControlClass()?>'
		>

			<?php
			if ($this->value)
				{
				?>
				<div class="waframe_label">
					<?=$this->value?>
				</div>
				<?php
			
				}

			// stuff starts here...
				
		}
		
	//**************************************************************************
	public function close()
		{
		//... stuff ends here
		?>
		<div style='clear: both; height: 1px;'></div>
		</div>
		<?php
		}
		
	//**************************************************************************
	}
//******************************************************************************


