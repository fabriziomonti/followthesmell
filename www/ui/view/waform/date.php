<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waDateView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControl $data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		$value = strlen($data->value) ? 
								substr($data->value, 8, 2) . "/" .
								substr($data->value, 5, 2) . "/" .
								substr($data->value, 0, 4) 
							: "" ;
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$value?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center;'
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							format: 'DD/MM/YYYY',
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControl $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$value = trim($_POST[$data->name]);
		return $value ? 
						substr($value, 6, 4) . "-" .
						substr($value, 3, 2) . "-" .
						substr($value, 0, 2)
					: false;
		}
		
	//**************************************************************************
	}
//******************************************************************************


