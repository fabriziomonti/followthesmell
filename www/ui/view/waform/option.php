<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waOptionView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlOption $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
		<div id='waform_radio_container_<?=$this->form->name?>_<?=$this->name?>' style=''>
			<?php

			$idx = 0;
			foreach ($this->list as $key => $val)
				{
				list($realKey, $rest) = explode("|", $key);
				$checked = $realKey == $this->value ? "checked" : "";
				?>
				<div style='white-space: nowrap;'>
					<input 
						type='radio' 
						id='<?=$this->form->name?>_<?=$this->name?>[<?=$idx?>]' 
						name='<?=$this->name?>' 
						value='<?=$key?>' 
						<?=$checked?> 
						<?=$this->getControlAttributes()?>
						class='<?=$this->getControlClass()?>'
						>

					<label 
						id='waform_lblradio_<?=$this->form->name?>_<?=$this->name?>[<?=$idx?>]' 
						<?=$this->getControlAttributes()?>
					>
						<?=$val?>
					</label>
				</div>
				<?php
				$idx++;
				}

			?>
			</div>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	}
//******************************************************************************


