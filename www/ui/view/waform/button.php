<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waButtonView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlButton $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$class = $this->submit ? "btn-primary" : "btn-secondary";
		$class = $this->cancel ? "btn-secondary" : $class;
		$class = $this->delete ? "btn-danger" : $class;
		$value = htmlspecialchars($this->value, ENT_QUOTES | ENT_HTML5);
		?>

		<button 
			id='<?=$this->form->name?>_<?=$this->name?>' 
			name='<?=$this->name?>' 
			value='<?=$value?>'
			type='<?=$this->submit ? "submit" : "button"?>' 
			<?=$this->getControlAttributes()?> 
			style='<?=$this->getControlStyle()?>'
			class='btn <?=$class?> <?=$this->getControlClass()?>'
		>
			<?=$value?>
		</button>

		<!--	parcheggiamo le proprieta' particolari nel controllo html, in modo che-->
		<!--	poi la classe applicativa  possa ritrovarli-->
		<script type='text/Javascript'>
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.cancel = <?=$this->cancel ? 1 : 0?>;
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.delete = <?=$this->delete ? 1 : 0?>;
			document.getElementById('<?=$this->form->name?>').<?=$this->name?>.submit = <?=$this->submit ? 1 : 0?>;
		</script>
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlButton $data)
		{
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		return $_POST[$data->name] ? 1 : 0;
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


