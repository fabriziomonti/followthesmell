<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waDateTimeView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlDateTime $data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		$value = strlen($data->value) ? 
						substr($data->value, 8, 2) . "/" .
						substr($data->value, 5, 2) . "/" .
						substr($data->value, 0, 4) . " " .
						substr($data->value, 11, 2) . ":" .
						substr($data->value, 14, 2) . ":" .
						substr($data->value, 17, 2)
					: "" ;
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$value?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center;'
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							format: 'DD/MM/YYYY HH:mm:SS',
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormDataControlDateTime $data)
		{
		
		if ($_POST[$data->name] === null)
			{
			return null;
			}
		
		$value = trim($_POST[$data->name]);
		if (!$value)
			{
			return false;
			}
			
		$retval = substr($value, 6, 4) . "-" .
					substr($value, 3, 2) . "-" .
					substr($value, 0, 2) . " " .
					substr($value, 11, 2) . ":" .
					substr($value, 14, 2) . ":";
		$retval .= $data->showSeconds ? substr($value, 17, 2) : "00";
		
		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


