<?php 
namespace followthesmell;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waMultiSelectView extends waControlView 
	{
	
	//**************************************************************************
	public function transform(\waLibs\waFormDataControlMultiSelect $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<select 
				multiple 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>[]' 
				<?=$this->getControlAttributes()?> 
				style='height: 16rem; '
				class='form-control <?=$this->getControlClass()?>'
			>

				<?php
				if ($this->emptyRow)
					{
					?>
					<option value=''></option>
					<?php
					}

				foreach ($this->list as $key => $val)
					{
					list($realKey, $rest) = explode("|", $key);
					$selected = in_array($realKey, $this->value) ? "selected" : "";
					$disabled = in_array($realKey, $this->notSelectableList) ? "disabled" : "";
					?>
					<option value='<?=$key?>' <?=$selected?> <?=$disabled?>><?=$val?></option>
					<?php
					}
				?>

			</select>
		</div>

		<?php
		
		}
		
	//**************************************************************************
	}
//******************************************************************************


