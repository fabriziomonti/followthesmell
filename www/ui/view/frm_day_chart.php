<?php 
namespace followthesmell;
include_once __DIR__ . "/waapplication/waapplication.php";

//******************************************************************************
class frm_day_chart_view extends waapplication_view
	{
	
	/**
	 * dati in input
	 * @var waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;

		$this->setHead();
		$this->setItem($this->data->page->items["waMenu"]);
		$hit_nr = 0;
		foreach($data->page->items["hit_list"]->value as $value)
			{
			$hit_nr += $value;
			}
		$this->data->page->items["title"]->value .= $hit_nr ? " ($hit_nr)" : "";
		$this->setTitle($this->data->page->items["title"]);

		?>
		<div style="height: 2rem;"></div>
		<canvas id="myChart" style="position: relative; height:60vh; width:80vw"></canvas>
		<script src="//cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

		<script type="text/javascript">
			var labels = [<?php
							$weekdays = ["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
							$virgola = "";
							foreach ($data->page->items["hit_list"]->value as $registration_date => $how_many)
								{
								echo "$virgola '" . $weekdays[date("w", $registration_date)] . date(" d/m/Y", $registration_date) . "'";
								$virgola = ",";
								}

						?>];
			var data = [<?php
							$virgola = "";
							foreach ($data->page->items["hit_list"]->value as $registration_date => $how_many)
								{
								echo "$virgola $how_many";
								$virgola = ",";
								}

						?>];
			var ctx = document.getElementById('myChart').getContext('2d');
			var chart = new Chart(ctx, {
				// The type of chart we want to create
				type: 'line',

				// The data for our dataset
				data: {
					labels: labels,
					datasets: [{
						backgroundColor: 'rgb(255, 99, 132)',
						borderColor: 'rgb(255, 99, 132)',
						data: data
					}]
				},

				// Configuration options go here
				options: {
					legend : {display: false},
					scales	: {
						yAxes: [{
						  scaleLabel: {
							display: true,
							labelString: 'Nr. segnalazioni'
						  }
						}],
						xAxes: [{
						  scaleLabel: {
							display: true,
							labelString: 'Data'
						  }
						}]						
					}
				}
			});			
		</script>

		<?php 
		$this->setPeriodNavigation();
		$this->setItem($this->data->page->items["waForm"]);
		$this->setVersiondata($this->data->page->items["version_data"]);
		$this->setPageResources();
		$this->setJavascriptObject();
		$this->setFooter();
		
		}
		
	//**************************************************************************
	protected function setItem(\waLibs\waApplicationDataPageItem $item = null)
		{
		if (!$item)
			{
			return;
			}
			
		?>
		<div class="waapplication_<?=$item->name?>">
			<?=$item->value?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	protected function setPeriodNavigation()
		{
		if (!$this->data->page->items["from"]->value || !$this->data->page->items["to"]->value)
			{
			return;
			}
		
		$from = $this->data->page->items["from"]->value;
		$to = $this->data->page->items["to"]->value;
		echo "<div style='float:left'>Indietro" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) - 1) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) - 1) . "'>giorno</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) - 7) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) - 7) . "'>settimana</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from) - 1, date("j", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to) - 1, date("j", $to)) . "'>mese</a>" .
				"</div>";
		echo "<div style='float:right'>Avanti" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) + 1) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) + 1) . "'>giorno</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) + 7) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) + 7) . "'>settimana</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from) + 1, date("j", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to) + 1, date("j", $to)) . "'>mese</a>" .
				"</div><div style='clear: both; height: 0px;'></div>";
			
			
		}
		
	//**************************************************************************
	}
//******************************************************************************


