<?php
namespace followthesmell;

//******************************************************************************
class wamenu_view implements \waLibs\i_waMenuView
	{

	//**************************************************************************
	public function transform(\waLibs\waMenuData $data)
		{
		?>
		<!-- SmartMenus jQuery Bootstrap Addon CSS -->
		<link href='ui/js/vendor/jquery.smartmenus/1.0.0/addons/bootstrap/jquery.smartmenus.bootstrap.min.css' rel='stylesheet'>

		<!-- SmartMenus jQuery plugin -->
		<script type='text/javascript' src='ui/js/vendor/jquery.smartmenus/1.0.0/jquery.smartmenus.min.js'></script>

		<!-- SmartMenus jQuery Bootstrap Addon -->
		<script type='text/javascript' src='ui/js/vendor/jquery.smartmenus/1.0.0/addons/bootstrap/jquery.smartmenus.bootstrap.min.js'></script>		

		<!-- Navbar -->
		<div class='navbar navbar-default' role='navigation' style='display:none;'>
			
			<div class='navbar-header followthesmell_menu'>
				<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse.followthesmell_menu'>
					<span class='sr-only'>Toggle navigation</span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
				</button>
			</div>

			<div class="navbar-collapse collapse followthesmell_menu">

				<!-- nav -->
				<ul class="nav navbar-nav">
					
				<?php 
				foreach ($data->items as $item)
					{
					$this->showItem($item);
					}
				?>

				</ul>
			</div><!--/.nav-collapse -->
		</div>
		
		<script type='text/javascript'>
			jQuery('.domain_institutional_menu').on('show.bs.collapse', function () {
				jQuery('.followthesmell_menu').collapse('hide');
			});
			jQuery('.followthesmell_menu').on('show.bs.collapse', function () {
				jQuery('.domain_institutional_menu').collapse('hide');
			});

			jQuery(window).resize(followthesmell_menuArrange);
			followthesmell_menuArrange();

			//------------------------------------------------------------------
			function followthesmell_menuArrange() {

				var mobile = jQuery("div.navbar-header.domain_institutional_menu button").is(":visible");
				if (mobile) {
					jQuery("div.navbar.navbar-default").css(
						{
						"background-color": "#f8f8f8",
						"border": "#e7e7e7 1px solid",
						"margin-top": ".6rem"
						}
					);
					jQuery("div.navbar-collapse.collapse.followthesmell_menu").css(
						{
						"border": "none",
						"border-top": "#e7e7e7 1px solid"
						}
					);
					jQuery("div.navbar-collapse.collapse.domain_institutional_menu").css(
						{
						"border": "none",
						"border-top": "#e7e7e7 1px solid",
						"margin-bottom": "0"
						}
					);
				}
				else {
					jQuery("div.navbar.navbar-default").css(
						{
						"background-color": "transparent",
						"border": "none",
						"margin-top": "-1.6rem"
						}
					);
					jQuery("div.navbar-collapse.collapse.followthesmell_menu").css(
						{
						"border": "#e7e7e7 1px solid"
						}
					);
					jQuery("div.navbar-collapse.collapse.domain_institutional_menu").css(
						{
						"border": "#e7e7e7 1px solid",
						"margin-bottom": "0.6rem"
						}
					);
				}
			}
			
		</script>

		<?php
		}

	//**************************************************************************
	private function showItem(\waLibs\waMenuDataItem $item)
		{

		$myurl = $item->url ? $item->url : "#";
		$target = $item->target ? "target='$item->target'" : "";
		$class = $item->selected ? "class='active'" : "";
		?>
		<li id='<?=$item->id ?>' <?=$class?> >
			<a href='<?=$myurl?>' <?=$target?> >
				<?=$item->label?>
				<?php if (isset($item->options->badge)) :?> 
					<span class='badge <?=$item->options->badge_class?>'>
						<?=$item->options->badge?>
					</span>
				<?php endif;?>
				<?php if (count($item->items)) :?> 
					<span class="caret"></span>
				<?php endif;?>
			</a>
			<?php
			if (count($item->items))
				{
				?>
				<ul class='dropdown-menu'>
					
				<?php
				foreach ($item->items as $item)
					{
					$this->showItem($item);
					}
				?>

				</ul>
				<?php
				}
			
			?>
		</li>

		<?php
		}

//******************************************************************************
	}

//******************************************************************************


