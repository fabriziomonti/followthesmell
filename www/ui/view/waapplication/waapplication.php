<?php 
namespace followthesmell;

//******************************************************************************
class waapplication_view implements \waLibs\i_waApplicationView
	{
	
	/**
	 * dati in input
	 * @var waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;

		$this->setHead();
		$this->setItems();
		$this->setPageResources();
		$this->setJavascriptObject();
		$this->setFooter();
		
		}
		
	//**************************************************************************
	protected function setHead($img = '')
		{
		if ($img == '')
			{
			$img =  $this->data->domain . "/" . $this->data->workingDirectory . "/ui/img/logo.png";
			}                
		?>
		
		<!DOCTYPE html>
		<html>
			<head>
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

				<!-- Bootstrap core CSS -->
				<link href='ui/js/vendor/twitter-bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'>

				<link href='<?=$this->data->waApplicationPath?>/uis/waapplication_default/css/waapplication.css' rel='stylesheet'/>

				<script type='text/javascript' src='ui/js/vendor/mootools/1.6.0/mootools-core.min.js'></script>
				<script type='text/javascript' src='ui/js/vendor/jquery/2.2.4/jquery.min.js'></script>
				<script type='text/javascript' src='ui/js/vendor/twitter-bootstrap/3.3.6/js/bootstrap.min.js'></script>
				<script type='text/javascript' src='ui/js/vendor/jquery-scrolltop-button/jquery-scrolltop-btn.js'></script>

				<!-- fontawesome icon  -->
				<link href='ui/js/vendor/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css' rel='stylesheet'>
				 <!--<script type='text/javascript' src="ui/js/vendor/fontawesome-free-5.0.8/svg-with-js/js/fontawesome.js"></script>--> 

				<!-- Google Maps API -->
				<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?libraries=geometry,places&language=it-IT&key=<?=$this->data->page->items["google_maps_api_key"]->value?>'></script>

				<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
				<script type='text/javascript'>
					jQuery.noConflict();			
				</script>

				<!--fancybox-->
				<link rel="stylesheet" href="ui/js/vendor/fancybox/2.1.5/jquery.fancybox.min.css" />
				<script type="text/javascript" src="ui/js/vendor/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
				<script type="text/javascript" src="ui/js/vendor/fancybox/2.1.5/jquery.fancybox.pack.js"></script>

				<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/strmanage.js'></script>
				<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/waapplication.js'></script>

				<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
				<!--<link rel="icon" href="ui/img/logo.png" type="image/png" />-->
                                
				<title>
					<?=$this->data->page->items["title"]->value?>
					|
					<?=$this->data->title?>
				</title>
                <meta charset="UTF-8">                
                                
			</head>
			<body >
				<noscript>
					<hr />
					<div style='text-align: center'>
						<b>
							Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
							disabilitata. Sei pregato di abilitare Javascript per il dominio <?=$this->data->domain?>
							e ricaricare la pagina.
						</b>
					</div>
					<hr />
				</noscript>

				<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
				<?php if ($this->data->navigationMode == \waLibs\waApplication::NAV_INNER) : ?>
					<a class="iframe" id="fb_iframe" href="" style="display:none;"></a>
				<?php endif; ?>

		<?php
		}
		
	//**************************************************************************
	protected function setItems()
		{
		?>
					
				<!-- creazione degli itemi costitutivi della pagina (titolo, tabelle, moduli, testo libero, ecc.-->
		<?php 
		foreach ($this->data->page->items as $item) : 
			if ($item->name == "version_data") :
				$this->setVersiondata($item);
			elseif ($item->name == "title") :
				$this->setTitle($item);
			elseif ($item->name == "message") :
				$this->setMessage($item);
			elseif ($item->name == "google_maps_api_key") :
				// già fatto
			elseif ($item->name == "help") :
				// già fatto
			elseif ($item->name == "waapplication_action_back") :
				$this->setBackButton();
			elseif ($item->name == "waapplication_action_close") :
				$this->setCloseButton();
			elseif ($item->name == "is_mobile") :
				// fa niente
			elseif ($item->name == "is_user_logged") :
				// fa niente
			elseif ($item->name == "association_title") :
				// fa niente
			else :
				$this->setItem($item);
			endif;
		endforeach; 
		?>

		<?php 
		}
		
	//**************************************************************************
	// definisce le risorse (css, js, ecc...) specifiche della pagina
	protected function setPageResources()
		{
		$version = "v=" . urlencode($this->data->page->items["version_data"]->value->nr);
		?>

		<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
		<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
		<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
		<!-- - un css del dominio (directory_di_lavoro/ui/css/nome_dominio.css)-->
		<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
		<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
		<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
		<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->name?>.css?<?=$version?>' rel='stylesheet'/>
		<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->page->name?>.css?<?=$version?>' rel='stylesheet'/>
		<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->domain?>.css?<?=$version?>' rel='stylesheet'/>

		<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->name?>.js?<?=$version?>'></script>
		<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->page->name?>.js?<?=$version?>'></script>

		<?php 
		}
		
	//**************************************************************************
	protected function setJavascriptObject()
		{
		?>

		<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
		<!-- e i metodi di default dell'applicazione. -->
		<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
		<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
		<script type='text/javascript'>
			if (!document.waPage)
				document.waPage = new followthesmell();
			document.waPage.navigationMode = '<?=$this->data->navigationMode?>';
			document.waPage.isUserLogged = <?=$this->data->page->items["is_user_logged"]->value?>;
			document.waPage.title = "<?=$this->data->title?>";
			<?php if ($this->data->page->returnValues) : ?>
				document.waPage.updateParent('<?=$this->data->page->returnValues?>');
				<?php if ($this->data->page->close) : ?>
					document.waPage.closePage();
				<?php endif; ?>
			<?php endif; ?>
		</script>
		<?php 
		
		// se siamo in ambiente di test e se non l'abbiamo ancora fatto, diamo 
		// il messaggio all'utente che è in ambiente di test
		if ($this->data->domain == "test.followthesmell.webappls.com" && !$_SESSION["test_warning_already_shown"])
//		if ($this->data->domain == "localhost" && !$_SESSION["test_warning_already_shown"])
			{
			$_SESSION["test_warning_already_shown"] = true;
			?>
			<script type='text/javascript'>
				jQuery(document).ready(function() {
					document.waPage.alert("Attenzione: sei in ambiente di test.\n\n" +
											"Tutte le segnalazioni che genererai in questa" +
											" sessione saranno consegnate a indirizzi fittizi," +
											" e quindi assolutamente senza alcun valore.");
				});
			</script>
			<?php 
			
			}
		
		}
		
	//**************************************************************************
	protected function setFooter()
		{
		?>

				<!--modal per alert/confirm-->
				<div  id="waapplication_alert" class="modal fade">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title"><?=$this->data->title?></h4>
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer">
								<button id="waapplication_alert_close" type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
								<button id="waapplication_confirm_cancel" type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
								<button id="waapplication_confirm_confirm" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->				

			</body>
		</html>

		<?php
		
		}
		
	//**************************************************************************
	protected function setItem(\waLibs\waApplicationDataPageItem $item)
		{
			
			?>
			<div class="waapplication_<?=$item->name?>">
				<?=$item->value?>
			</div>
			<?php
			
		}
		
	//**************************************************************************
	protected function setVersionData(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?=$item->name?> container">
			versione <?=$item->value->nr?> - <?=$item->value->date?>
			<br>
			<a href='privacy.php' target='_blank'>Privacy</a>
            <!--- <a href='tbl_faq.php' target='_blank'>F.A.Q.</a>-->
			<br>
            <a href='cookies.php' target='_blank'>Cookies</a>
			<br>
			<a href="mailto:<?=$item->value->supportEmailAddress?>?subject=[FollowTheSmell]">Assistenza</a>			
			<!--logo--> 
<!--			<div  id="waapplication_logo">
			</div>
			<div  id="waapplication_logo_domain">
			</div>-->
		</div>
		<?php
		
		}
		
	//**************************************************************************
	protected function setTitle(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?=$item->name?>">
			<?=nl2br($item->value)?>
			<?php 
			if ($this->data->page->items["help"]) :
				// a volte il titol della pagina potrebbe contenere dettagli insignificanti
				list($title, $rest) = explode("\n", $item->value, 2);
				?>
				<a href="#" onclick="document.waPage.showHelp('<?=$this->data->sectionName?>', '<?=$this->data->page->name?>', '', '<?=addslashes($title)?>', '')" tabindex="-1">
					<span class="glyphicon glyphicon-info-sign"></span>
				</a>
			<?php endif; ?>
		</div>
		<?php
		
		}

	//**************************************************************************
	protected function setMessage(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?=$item->name?>">
			<?=nl2br($item->value)?>
		</div>
		<?php
		
		}

	//**************************************************************************
	protected function setBackButton()
		{
		?>
		<div class="waapplication_action_back">
			<form>
				<button type='button' class='btn' value='&lt;&lt; Torna' onclick='document.waPage.showMessageBackButtonPressed()'>
					&lt;&lt; Torna
				</button>
			</form>
		</div>
		<?php
		
		}

	//**************************************************************************
	protected function setCloseButton()
		{
		?>
		<div class="waapplication_action_close">
			<form>
				<button type='button' class='btn' value='Chiudi' onclick='document.waPage.showMessageCloseButtonPressed()'>
					Chiudi
				</button>
			</form>
		</div>
		<?php
		
		}

	//**************************************************************************
	}
//******************************************************************************


