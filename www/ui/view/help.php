<?php 
namespace followthesmell;

//******************************************************************************
class help_view implements \waLibs\i_waApplicationView
	{
	
	/**
	 * dati in input
	 * @var waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;
		
		$winitle

		?>
		
			<!DOCTYPE html>
			<html>
				<head>
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
					
					<!-- Bootstrap core CSS -->
					<link href='ui/js/vendor/twitter-bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'>

					<link href='<?=$this->data->waApplicationPath?>/uis/waapplication_default/css/waapplication.css' rel='stylesheet'/>
					
					<script type='text/javascript' src='ui/js/vendor/mootools/1.6.0/mootools-core.min.js'></script>
					<script type='text/javascript' src='ui/js/vendor/jquery/2.2.4/jquery.min.js'></script>
					<script type='text/javascript' src='ui/js/vendor/twitter-bootstrap/3.3.6/js/bootstrap.min.js'></script>

					<!-- Google Maps API -->
					<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?libraries=geometry,places&language=it-IT&key=<?=$this->data->page->items["google_maps_api_key"]->value?>'></script>

					<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
					<script type='text/javascript'>
						jQuery.noConflict();			
					</script>

					<!--fancybox-->
					<link rel="stylesheet" href="ui/js/vendor/fancybox/2.1.5/jquery.fancybox.min.css" />
					<script type="text/javascript" src="ui/js/vendor/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
					<script type="text/javascript" src="ui/js/vendor/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
					
					<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/strmanage.js'></script>
					<script type='text/javascript' src='<?=$this->data->waApplicationPath?>/uis/waapplication_default/js/waapplication.js'></script>

					<title>
						<?=$this->setWindowTitle()?>
					</title>
					<meta charset="utf-8">


				</head>
				<body >
					<noscript>
						<hr />
						<div style='text-align: center'>
							<b>
								Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
								disabilitata. Sei pregato di abilitare Javascript per il dominio <?=$this->data->domain?>
								e ricaricare la pagina.
							</b>
						</div>
						<hr />
					</noscript>

					<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
					<?php if ($this->data->navigationMode == \waLibs\waApplication::NAV_INNER) : ?>
						<a class="iframe" id="fb_iframe" href="" style="display:none;"></a>
					<?php endif; ?>

					<!-- creazione degli itemi costitutivi della pagina (titolo, tabelle, moduli, testo libero, ecc.-->
					<?php 
					$this->setTitle();
					$this->setText($this->data->page->items["text"]);
					$this->setButtons();
					$this->setVersiondata($this->data->page->items["version_data"]);
					?>
						
					<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
					<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
					<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
					<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
					<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
					<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
					<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->name?>.css' rel='stylesheet'/>
					<link href='<?=$this->data->workingDirectory?>/ui/css/<?=$this->data->page->name?>.css' rel='stylesheet'/>
					<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->name?>.js'></script>
					<script type='text/javascript' src='<?=$this->data->workingDirectory?>/ui/js/<?=$this->data->page->name?>.js'></script>

					<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
					<!-- e i metodi di default dell'applicazione. -->
					<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
					<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
					<script type='text/javascript'>
						if (!document.waPage)
							document.waPage = new followthesmell();
						document.waPage.navigationMode = '<?=$this->data->navigationMode?>';
						<?php if ($this->data->page->returnValues) : ?>
							document.waPage.updateParent('<?=$this->data->page->returnValues?>');
							<?php if ($this->data->page->close) : ?>
								document.waPage.closePage();
							<?php endif; ?>
						<?php endif; ?>
					</script>

					<!--modal per alert/confirm-->
					<div  id="waapplication_alert" class="modal fade">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title"><?=$data->title?></h4>
								</div>
								<div class="modal-body">
								</div>
								<div class="modal-footer">
									<button id="waapplication_alert_close" type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
									<button id="waapplication_confirm_cancel" type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
									<button id="waapplication_confirm_confirm" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->				

				</body>
			</html>

		<?php
		}
		
	//**************************************************************************
	protected function setText(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_help_text">
			<?=$item->value?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	protected function setVersionData(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?=$item->name?>">
			version <?=$item->value->nr?> - <?=$item->value->date?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	protected function setTitle()
		{
		$items = & $this->data->page->items;
		$tile = $items["title"]->value .
				($items["label"]->value ? "<br/>" . $items["label"]->value : "");
		?>
		<div class="waapplication_title">
			Help
			<br/>
			<?=nl2br($tile)?>
		</div>
		<?php
		}

	//**************************************************************************
	protected function setWindowTitle()
		{
		$items = & $this->data->page->items;
		echo $items["title"]->value .
						($items["label"]->value ? " | " . $items["label"]->value : "") .
						" | " . $this->data->title .
						" | help";
				
		}

	//**************************************************************************
	protected function setButtons()
		{
		$items = & $this->data->page->items;

		?>
		<div class="waapplication_help_buttons">
			<form>
				<?php if ($items["is_sys_admin"]->value) : 
					$form = "frm_help.php?section=" . $items["section"]->value .
							"&page=" . $items["page"]->value .
							"&control=" . $items["control"]->value .
							"&title=" . urlencode($items["title"]->value) .
							"&label=" . urlencode($items["label"]->value) .
							"&id=" . urlencode($items["id"]->value);
					?>
					<button type="button" class="btn btn-primary" onclick="document.waPage.openPage('<?=$form?>')">
						Modifica
					</button>
				<?php endif; ?>
				<button type="button" class="btn btn-secondary" onclick="document.waPage.closePage()">
					Chiudi
				</button>
			</form>
		</div>
		<?php
		}

	//**************************************************************************
	}
//******************************************************************************


