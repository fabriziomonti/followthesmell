<?php 
namespace followthesmell;

//******************************************************************************
class watable_actions_left_view implements \waLibs\i_waTableView
	{
	
	/**
	 * dati in input
	 * @var \waLibs\waTableData
	 */
	protected $data = null;

	protected $hasSortOptions = true;
	protected $hasFilterOptions = true;

	//**************************************************************************
	public function transform(\waLibs\waTableData $data)
		{
		$this->data = $data;
		
		$this->hasSortOptions = $this->checkHasSortOptions();
		$this->hasFilterOptions = $this->checkHasFilterOptions();

		$this->setCssLink();
		$this->setJavascriptLink();
		$this->setSortOrderFrame();
		$this->setPageActions();
		$this->setNavbar();
		$this->setTable();
		$this->setNavbar();
		$this->setJavascriptObjects();
		
		}
		
	//**************************************************************************
	protected function setCssLink()
		{
		?>

		<!-- Bootstrap core CSS -->

		<link href='<?=$this->data->waTablePath?>/uis/watable_default/css/watable.css' rel='stylesheet'/>

		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptLink()
		{
		?>
		<!-- Bootstrap core JavaScript
		================================================== -->
		
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/strmanage.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/wacolumn.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/warow.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/watable.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	protected function setSortOptions()
		{
		for ($idx = 0; $idx < 3; $idx++)
			{
			?>
			<div class='row'>
				<div class='col-sm-6' style='text-align: center;'>
					<select class='form-control' name='watable_of[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						<?php
						foreach ($this->data->columnHeaders as $hdr) :
							if ($hdr->sort) :
								?>
								<option  value='<?=$hdr->name?>' <?=$this->data->orders[$idx]->field == $hdr->name ? "selected" : "" ?> >
									<?=$hdr->label?>
								</option>
								<?php 
							endif;
						endforeach;
						?>
					</select>
				</div>
				
				<div class='col-sm-6' style='text-align: center;'>
					<select class='form-control' name='watable_om[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						<?php foreach ($this->data->orderModes as $mode) :
							$label = $mode->name;
							$label = $mode->name == "Ascending" ? "ascendente" : $label;
							$label = $mode->name == "Descending" ? "discendente" : $label;
							?>
							<option  value='<?=$mode->value?>' <?=$this->data->orders[$idx]->mode == $mode->value ? "selected" : "" ?> >
								<?=$label?>	
						</option>
						<?php endforeach;?>
					</select>
				</div>
			
			</div>
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setFilterOptions()
		{
		for ($idx = 0; $idx < 5; $idx++)
			{
			?>

			<div class='row'>
				<div class='col-sm-4' style='text-align: center;'>
					<select class='form-control' name='watable_ff[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						
						<?php
						foreach ($this->data->columnHeaders as $hdr) :
							if ($hdr->filter) :
								?>
								<option  value='<?=$hdr->name?>' <?=$this->data->filters[$idx]->field == $hdr->name ? "selected" : "" ?> >
									<?=$hdr->label?>
								</option>
								<?php 
							endif;
						endforeach;
						?>
					</select>
				</div>			
				
				<div class='col-sm-4' style='text-align: center;'>
					<select class='form-control' name='watable_fm[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>

						<?php foreach ($this->data->filterModes as $mode) :
							$label = $mode->name;
							$label = $mode->name == "lower" ? "minore" : $label;
							$label = $mode->name == "lower-equal" ? "minore-uguale" : $label;
							$label = $mode->name == "equal" ? "uguale" : $label;
							$label = $mode->name == "greater-equal" ? "maggiore-uguale" : $label;
							$label = $mode->name == "greater" ? "maggiore" : $label;
							$label = $mode->name == "not equal" ? "diverso" : $label;
							$label = $mode->name == "contains" ? "contiene" : $label;
							?>
							<option  value='<?=$mode->value?>' <?=$this->data->filters[$idx]->mode == $mode->value ? "selected" : "" ?> >
								<?=$label?>
							</option>
						<?php endforeach;?>
					</select>
				</div>			

				<div class='col-sm-4' style='text-align: center;'>
					<?php
					$fieldType = $this->getColType($this->data->filters[$idx]->field);
					$value = htmlspecialchars($this->data->filters[$idx]->value, ENT_QUOTES | ENT_HTML5);
					if ($fieldType == \waLibs\waDB::DATE)
						{
						$value = substr($value, 8, 2) . "/" .substr($value, 5, 2) . "/" . substr($value, 0, 4);
						}
					elseif ($fieldType == \waLibs\waDB::DATETIME)
						{
						$value = substr($value, 8, 2) . "/" .substr($value, 5, 2) . "/" . substr($value, 0, 4) . " " . substr($value, 11, 2) . ":" . substr($value, 14, 2);
						}
					elseif ($fieldType == \waLibs\waDB::TIME)
						{
						$value = substr($value, 0, 2) . ":" . substr($value, 3, 2);
						}
					?>
					<input 
						class='form-control'
						name='watable_fv[<?=$this->data->name?>][<?=$idx?>]' 
						value='<?=$value?>' 
					>
				</div>			

			</div>			
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setSortOrderFrame()
		{
		?>

		<div id='<?=$this->data->name?>_order_filter_frame' class='watable_order_filter_frame' style='visibility: hidden'>
			
			<p>Ordinamento/Filtro <?=$this->data->title?></p>

			<form  action='<?=$this->data->uri?>' id='<?=$this->data->name?>_order_filter_form' onsubmit='return document.<?=$this->data->name?>.filter(this)'>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-6' style='text-align: center;'>
						Ordinamento
					</div>
					<div class='col-sm-6' style='text-align: center;'>
						Modo
					</div>
				</div>
				<?php
				$this->setSortOptions();
				?>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-4' style='text-align: center;'>
						Filtro
					</div>
					<div class='col-sm-4' style='text-align: center;'>
						Modo
					</div>
					<div class='col-sm-4' style='text-align: center;'>
						Valore
					</div>
				</div>
				<?php
				$this->setFilterOptions();
				?>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-6' style='text-align: center;'>
						<input 
							type='submit' 
							class='btn'
							value='Ordina/Filtra' 
						>
					</div>
					<div class='col-sm-6' style='text-align: center;'>
						<input 
							type='button' 
							class='btn'
							value='Annulla' 
							onclick='document.<?=$this->data->name?>.closeOrderFilter()' 
						>
					</div>
				</div>
				
			</form>
		</div>

		
		<?php
		}
		
	//**************************************************************************
	protected function setPageActions()
		{
		?>
			
		<form action='<?=$this->data->uri?>' id='<?=$this->data->name?>_page_actions' class='watable' onsubmit='return document.<?=$this->data->name?>.quickSearch()'>
			<div class="row">
				
				<div class='col-sm-12'>
					<div class='btn-group'>
						<?php
						foreach ($this->data->pageActions as $action)
							{
							$label = $action->label;
							if ($label == "Filter" && !$this->hasFilterOptions && !$this->hasSortOptions)
								{
								continue;
								}
							$label = $label == "New" ? "Nuovo" : $label;
							$label = $label == "Filter" ? "Filtro" : $label;
							$label = $label == "No Filter" ? "No filtro" : $label;
							?>
							<button 
								type='button' 
								class='btn'
								name='<?=$action->name?>' 
								id='<?=$action->name?>' 
								title='<?=$action->label?>' 
								value='<?=$action->label?>' 
								onclick='document.<?=$this->data->name?>.action_<?=$this->data->name?>_<?=$action->name?>()'
							>
								<?=$label?>
							</button>
							<?php
							}

						// bottoni di esportazione (csv, xls, pdf)
						$this->setExportActions();
						?>
						
					</div>
				</div>

				<?php
				// controlli per l'immissione testo di ricerca rapida
				$this->setQuickSearchControls();
				?>
				
			</div>
		</form>
			
		
		<?php
		}
		
	//**************************************************************************
	protected function setQuickSearchControls()
		{
		if (!$this->hasFilterOptions)
			{
			return;
			}
		
		$value = htmlspecialchars($this->data->quickSearch, ENT_QUOTES | ENT_HTML5);

		?>
		<div class='col-sm-12 watable_quick_search'>
			<div class='input-group'>
				<input 
					class='form-control'
					placeholder="Cerca"
					name='watable_qs[<?=$this->data->name?>]' value='<?=$value?>'
				>
				 <div class='input-group-btn'>
					<button 
						type='submit' 
						title='Cerca' 
						value='Cerca' 							
						class='btn'
					>
						Cerca
					</button>
				</div>
			</div>
		</div>
		<?php
		}
		
	//**************************************************************************
	protected function setExportActions()
		{
		if ($this->data->title != "Segnalazioni") return;
		$qoe = strpos($this->data->uri, "?") === false ? "?" : "&";
		?>
		<button 
			type='button' 
			class='btn'
			title='CSV' 
			value='CSV' 
			onclick='document.location.href="<?=$this->data->uri . $qoe . "watable_export_csv[" . $this->data->name . "]=1"?>"'
		>
			CSV
		</button>
		<button 
			type='button' 
			class='btn'
			title='XLS' 
			value='XLS' 
			onclick='document.location.href="<?=$this->data->uri . $qoe . "watable_export_xls[" . $this->data->name . "]=1"?>"'
		>
			XLS
		</button>
		<button 
			type='button' 
			class='btn'
			title='PDF' 
			value='PDF' 
			onclick='var w = window.open("<?=$this->data->uri . $qoe . "watable_export_pdf[" . $this->data->name . "]=1"?>")'
		>
			PDF
		</button>
		
		<?php
		}
		
	//**************************************************************************
	protected function setNavbar()
		{
		if ($this->data->navbar->totalPageNr <= 1) 
			{
			return;
			}
			
		$tblName = $this->data->name;
		
		$first = $this->data->navbar->currentPageNr - 2 < 0 ? 0 : $this->data->navbar->currentPageNr - 2;
		$last = $first + 4 < $this->data->navbar->totalPageNr ? $first + 4 : $this->data->navbar->totalPageNr - 1;
		
		?>
		<div class='row watable_navbar'>

			<ul class="pagination">

				<?php
				$class = $this->data->navbar->currentPageNr == 0 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == 0 ? 
							"void(0)" : 
							"document.$tblName.goToPage(\"" . ($this->data->navbar->currentPageNr - 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("0")'>&laquo;&laquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&laquo;</a>
				</li>

				<?php
				for ($li = $first; $li <= $last; $li++)
					{
					?>
					<li <?=$li == $this->data->navbar->currentPageNr ? "class='active'" : ""?>>
						<a href='javascript:document.<?=$this->data->name?>.goToPage("<?= $li ?>")'><?=$li + 1?></a></li>
					<?php
					}
					
				$class = $this->data->navbar->currentPageNr >= $this->data->navbar->totalPageNr - 1 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == $this->data->navbar->totalPageNr - 1 ? 
							"void(0)" : 
							"document." . $this->data->name . '.goToPage("' . ($this->data->navbar->currentPageNr + 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&raquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("<?=$this->data->navbar->totalPageNr - 1?>")'>&raquo;&raquo;</a>
				</li>
			</ul>			
			
		</div>
		<?php
		}
		
	//**************************************************************************
	protected function setTable()
		{
		?>
		<form id='<?=$this->data->name?>' action='' method='post' class='watable'>
			<div class="table-responsive"> 
				<table class='table table-bordered table-striped table-hover'>
					<?php
					$this->setCaption();
					$this->setHeaders();
					$this->setTotalsRow();
					$this->setRows();
					?>
				</table>
			</div>
		</form>
			
		<?php
		}
		
	//**************************************************************************
	protected function setRowActions(\waLibs\waTableDataRow $row)
		{
		$tblName = $this->data->name;

		?>
		<!--colonna delle azioni su record--> 
		<td class='btn-action' style='width: 1%; white-space: nowrap;'>			
                    <?php
                        $html_button_group = '';
                        $html_li_group = '';
                        $nr_prime_button = 0; //numero di buttoni primari nelle azioni
                        $nr_menu_li = 0;      //numero di azioni secondarie, nel menu a tendina
                        foreach ($row->enableableActions as $idx => $enabled)
                        {
                        if (!$enabled) 
                                {
                                continue;
                                }
                        $label = $this->data->recordActions[$idx]->label;
                        $label = $label == "Details" ? "Dettaglio" : $label;
                        $label = $label == "Edit" ? "Modifica" : $label;
                        $label = $label == "Delete" ? "Elimina" : $label;

                        $html_onclick = " onclick=\"document.".$tblName.".action_".$tblName."_".$this->data->recordActions[$idx]->name."('".$row->id."')\"";
                        $html_button_first = '<button type="button" class="btn btn-primary"' .
                                                                        ' data-toggle="tooltip" title="'.$label.'"' .
                                                                        $html_onclick .
                                                                        '>';
                        $html_button_last = '</button>';

                        $html_li_first = '<li><a '.$html_onclick.'>';
                        $html_li_last = '</a></li>';
              
                        switch ($label) {
                        // i bottoni primari li trattiamo come pulsanti, gli altri vanno nel menu a tendina
                            case "Segui":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-star-empty"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            case "Abbandona":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-star"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            case "Invita":
                                $html_button_group .= $html_button_first . '<i class="fas fa-user-plus"></i>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            case "Contatta":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-comment"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
							case "Statistiche":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-signal"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            case "Links":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-link"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            case "Stampa":
                                $html_button_group .= $html_button_first . '<span class="glyphicon glyphicon-print"></span>' . $html_button_last;
                                $nr_prime_button ++;
                                break;
                            default:
                                $html_li_group .= $html_li_first . $label . $html_li_last;
                                $nr_menu_li ++;
                                break;
                            }

                        }
                        $min_with_px = 40*$nr_prime_button + 30;
                    ?>
                    
                    <div class="btn-group" style="min-width: <?=$min_with_px?>px;">		
                    <?=$html_button_group?>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                            <?=$html_li_group?>
                    </ul>
                    </div>		
                </td>
		<?php
		}
		
	//**************************************************************************
	protected function setRows()
		{
		$tblName = $this->data->name;

		?>
		<tbody>
			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				<tr id='row_<?=$tblName?>_<?=$row->id?>' onclick='<?="document.$tblName.rows[\"$row->id\"]"?>.changeStatus()'>
					
					<?php
					$this->setRowActions($row);
					$this->setRowData($row);
					?>
					
				</tr>
				<?php
				}
			?>
		</tbody>
		<?php
		}
		
	//**************************************************************************
	protected function setRowData(\waLibs\waTableDataRow $row)
		{

		foreach ($row->cells as $idx => $cellValue)
			{
			$this->setCell($row, $cellValue, $idx);
			}
			
		}
		
	//**************************************************************************
	protected function setCell(\waLibs\waTableDataRow $row, $cellValue, $idx)
		{
		$tblName = $this->data->name;

		$hdr = $this->data->columnHeaders[$idx];
		if (!$hdr->show)
			{
			return;
			}

		?>
		<td style='text-align: <?=$this->getAlignment($hdr)?>' class='<?=$idx == 0 ? "id_cell" : "" ?>'>
			<?php

			if ($hdr->link)
				{
				?>
				<a href='javascript:document.<?=$tblName?>.link_<?=$tblName?>_<?=$hdr->name?>("<?=$row->id?>")'>
					<?=htmlspecialchars($cellValue)?>
				</a>
				<?php
				}

			elseif (!$hdr->HTMLConversion)
				{
				echo $cellValue;
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATETIME && $hdr->format == \waLibs\waTable::FMT_TIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 0, 5);
					}
				}

			elseif ($hdr->format == \waLibs\waTable::FMT_DATE)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATE)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATETIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4) . " " .
					substr($cellValue, 11, 2) . ":" .
					substr($cellValue, 14, 2);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::TIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 0, 2) . ":" .
					substr($cellValue, 3, 2);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DECIMAL)
				{
				if (strlen($cellValue))
					{
					echo number_format($cellValue, 2, ",", ".");
					}
				}

//			elseif ($hdr->fieldType == \waLibs\waDB::INTEGER)
//				{
//				if (strlen($cellValue))
//					echo number_format ($cellValue, 0, ",", ".");
//				}

		else
			{
			echo nl2br(htmlspecialchars($cellValue));
			}
			
			?>
		</td>
		<?php

		}
		
	//**************************************************************************
	protected function getAlignment(\waLibs\waTableDataColumnHeader $hdr)
		{
		$alignment = "left";
		$alignment = $hdr->fieldType == \waLibs\waDB::DATE ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DATETIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::TIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::INTEGER ? "right" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DECIMAL ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_R ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_C ? "center" : $alignment;

		return $alignment;
		}
		
	//**************************************************************************
	protected function setTotalsRow()
		{
		if (!$this->data->totalsRow)
			return;
		
		?>
		<tfoot>
			<tr>
				
				<?php
				$this->setRowActionsHeaders();
				$this->setTotalsRowData();
				?>
				
			</tr>
		</tfoot>
		<?php
		}
		
	//**************************************************************************
	protected function setTotalsRowData()
		{

		foreach ($this->data->totalsRow->cells as $idx => $cellValue)
			{
			$hdr = $this->data->columnHeaders[$idx];
			if (!$hdr->show) continue;
			?>
			<th style='text-align: <?=$this->getAlignment($hdr)?>'>
				<?php

				if ($hdr->fieldType == \waLibs\waDB::DECIMAL)
					{
					if (strlen($cellValue))
						{
						echo number_format($cellValue, 2, ",", ".");
						}
					}
				else
					{
					echo $cellValue;
					}
				?>	
			</th>
			
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setHeaders()
		{
		
		?>
			
		<thead>
			<tr id='<?=$this->data->name?>_headers'>

				<?php
				$this->setRowActionsHeaders();
				$this->setHeadersData();
				?>

			</tr>
		</thead>
			
		<?php
		}
		
	//**************************************************************************
	protected function setHeadersData()
		{
		
		$qoe = strpos($this->data->uri, "?") === false ? "?" : "&";
		$tblName = $this->data->name;
			
		foreach ($this->data->columnHeaders as $idx => $hdr)
			{
			if (!$hdr->show)
				{
				continue;
				}
			?>
			<th 
				style='text-align: <?=$this->getAlignment($hdr)?>' 
				id='<?=$this->data->name?>_<?=$hdr->name?>'
				class='<?=$idx == 0 ? "id_cell" : "" ?>'
			>
				<?php
				if ($hdr->sort)
					{
					$sortMode = $hdr->quickSort == "asc" ? "desc" : "asc";
					echo "<a href='" . $this->data->uri . "$qoe" . "watable_qo[$tblName]=$hdr->name&watable_qom[$tblName]=$sortMode'>\n";
					echo $hdr->label;
					if ($hdr->quickSort != "no")
						{
						?>
						<img src='<?=$this->data->waTablePath?>/uis/watable_default/img/<?=$hdr->quickSort?>_order.gif' border='0'/>
						<?php
						}
					echo "</a>\n";
					}
				else
					{
					echo $hdr->label;
					}
				?>
			</th>
			<?php
			}
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		?>
		<th></th>
		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		$tblName = $this->data->name;

		?>
		
		<script type='text/javascript'>
			// inizializzazione parametri tabella <?=$tblName?>
			
			document.<?=$tblName?> = new waTable('<?=$tblName?>', '<?=$this->data->columnHeaders[0]->name?>', '<?=$this->data->exclusiveSelection ? 1 : 0 ?>', '<?=$this->data->formPage?>');

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				new waRow(document.<?=$tblName?>, '<?=$row->id?>', <?=$this->row2json($row)?>);
				<?php
				}
				
			// inizializzazione delle proprieta' delle colonne
			foreach ($this->data->columnHeaders as $idx => $hdr)
				{
				if (!$hdr->show) continue;
				?>
				new waColumn(document.<?=$tblName?>, "<?=$hdr->name?>", "<?=$hdr->label?>", "<?=$hdr->fieldType?>");
				<?php
				}
			?>

		</script>		
		
		<?php
		}
		
	//**************************************************************************
	protected function row2json(\waLibs\waTableDataRow $row)
		{
		$retval = array();
		
		foreach($row->cells as $idx => $cellValue)
			{
			$retval[$this->data->columnHeaders[$idx]->name] = $cellValue;
			}

		return json_encode($retval);
		}
		
	//**************************************************************************
	protected function getColType($col_name)
		{
		if ($idx = $this->getIdxFromName($this->data->columnHeaders, $col_name))
			{
			return $this->data->columnHeaders[$idx]->fieldType;
			}
			
		}
		
	//**************************************************************************
	// dato l'attributo name di un oggetto contenuto in un array, restituisce
	// l'indice dell'emento dell'array in cui il name è contenuto
	protected function getIdxFromName($array, $name, $name_property = "name")
		{
		foreach ($array as $idx => $item)
			{
			if ($item->$name_property == $name)
				{
				return $idx;
				}
			}
			
		return false;
		}
		
	//**************************************************************************
	protected function checkHasSortOptions()
		{
		foreach ($this->data->columnHeaders as $hdr)
			{
			if ($hdr->sort) 
				{
				return true;
				}
			}
			
		return false;
		}
		
	//**************************************************************************
	protected function checkHasFilterOptions()
		{
		foreach ($this->data->columnHeaders as $hdr)
			{
			if ($hdr->filter) 
				{
				return true;
				}
			}
			
		return false;
		}
		
	//**************************************************************************
	protected function setCaption()
		{
		if (!$this->data->title || $this->data->title == "Segnalazioni")
			{
			return;
			}
		?>
		<caption><?=$this->data->title?></caption>
		<?php
		}
		
	//**************************************************************************
	}
//******************************************************************************


