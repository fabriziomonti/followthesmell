<?php 
namespace followthesmell;
include_once __DIR__ . "/watable_actions_left.php";
	
//******************************************************************************
class watable_actions_right_view extends watable_actions_left_view
	{
	
	//**************************************************************************
	protected function setHeaders()
		{
		
		?>
			
		<thead>
			<tr id='<?=$this->data->name?>_headers'>

				<?php
				$this->setHeadersData();
				$this->setRowActionsHeaders();
				?>

			</tr>
		</thead>
			
		<?php
		}
		
	//**************************************************************************
	protected function setRows()
		{
		$tblName = $this->data->name;

		?>
		<tbody>
			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				<tr id='row_<?=$tblName?>_<?=$row->id?>' onclick='<?="document.$tblName.rows[\"$row->id\"]"?>.changeStatus()'>
					
					<?php
					$this->setRowData($row);
					$this->setRowActions($row);
					?>
					
				</tr>
				<?php
				}
			?>
		</tbody>
		<?php
		}
		
	//**************************************************************************
	protected function setTotalsRow()
		{
		if (!$this->data->totalsRow)
			{
			return;
			}
			
		?>
		<tfoot>
			<tr>
				
				<?php
				$this->setTotalsRowData();
				$this->setRowActionsHeaders();
				?>
				
			</tr>
		</tfoot>
		<?php
		}
		
	//**************************************************************************
	}
//******************************************************************************


