<?php 
namespace followthesmell;
include_once __DIR__ . "/watable_actions_left.php";
	
//******************************************************************************
class watable_actions_context_view extends watable_actions_left_view
	{
	
	//**************************************************************************
	protected function setJavascriptLink()
		{
		parent::setJavascriptLink();
		?>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_actions_context/js/watable_actions_context.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		}
		
	//**************************************************************************
	protected function setRowActions(\waLibs\waTableDataRow $row)
		{
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		parent::setJavascriptObjects();
		
		$tblName = $this->data->name;

		// costruzione del menu contestuale per ogni riga
		foreach ($this->data->rows as $row)
			{
			?>
		
			<ul id="<?=$tblName?>_contextMenu_<?=$row->id?>" class="dropdown-menu" role="menu" style="display:none" >
				<?php
				foreach ($row->enableableActions as $idx => $enabled)
					{
					if ($enabled)
						{
						$label = $this->data->recordActions[$idx]->label;
						$label = $label == "Details" ? "Dettaglio" : $label;
						$label = $label == "Edit" ? "Modifica" : $label;
						$label = $label == "Delete" ? "Elimina" : $label;
						?>
						<li>
							<a href='javascript:document.<?=$tblName?>.action_<?=$tblName?>_<?=$this->data->recordActions[$idx]->name?>("<?=$row->id?>")'>
								<?=$label?>
							</a>
						</li>
						<?php
						}
					}
				?>
			</ul>

			<script type='text/javascript'>
				jQuery("#row_<?=$tblName?>_<?=$row->id?> td").contextMenu({
					menuSelector: "#<?=$tblName?>_contextMenu_<?=$row->id?>",
					menuSelected: function (invokedOn, selectedMenu) {}
				});

			</script>		
		
			<?php
			}
			
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


