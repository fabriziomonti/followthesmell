<?php 
namespace followthesmell;
include_once __DIR__ . "/watable_actions_left.php";
	
//******************************************************************************
class watable_edit_view extends watable_actions_left_view
	{
	
	//**************************************************************************
	protected function setCssLink()
		{
		parent::setCssLink();
		?>
		<link href='ui/js/vendor/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css' rel='stylesheet'>
		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptLink()
		{
		parent::setJavascriptLink();
		?>
		<script type='text/javascript' src='ui/js/vendor/moment.js/2.14.1/moment-with-locales.min.js'></script>
		<script type='text/javascript' src='ui/js/vendor/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js'></script>
		
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_edit/js/wacolumn_edit.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_edit/js/watable_edit.js'></script>
		<?php
		}
		
	//**************************************************************************
	protected function setTable()
		{
		?>
		<form id='<?=$this->data->name?>' action='' method='post' class='watable'>
			<!--nome della tabella da inviare insieme al submit, per sapere quale tabella ha fatto submit -->
			<input type='hidden' name='watable_name' value='<?=$this->data->name?>' />

			<div class="table-responsive"> 
				<table class='table table-bordered table-striped table-hover' id='watable_<?=$this->data->name?>_main_table'>
					<?php
					$this->setHeaders();
					$this->setTotalsRow();
					$this->setRows();
					?>
				</table>
			</div>
		</form>
			
		<?php
		}
		
	//**************************************************************************
	protected function setPageActions()
		{
		?>
			
		<form action='<?=$this->data->uri?>' id='<?=$this->data->name?>_page_actions' class='watable' onsubmit='return document.<?=$this->data->name?>.quickSearch()'>
			<div class="row">
				
				<div class='col-sm-12'>
					<div class='btn-group'>
						<!--bottone di submit del modulo-->
						<button 
							type='button' 
							class='btn'
							name='<?=$this->data->name?>_submit' 
							id='<?=$this->data->name?>_submit' 
							title='Submit' 
							value='Submit'  
							onclick='document.<?=$this->data->name?>.action_Submit()'
						>
							Submit
						</button>

						<?php
						foreach ($this->data->pageActions as $action)
							{
							$label = $action->label;
							$label = $label == "New" ? "Nuovo" : $label;
							$label = $label == "Filter" ? "Filtro" : $label;
							$label = $label == "No Filter" ? "No filtro" : $label;
							?>
							<button 
								type='button' 
								class='btn'
								name='<?=$action->name?>' 
								id='<?=$action->name?>' 
								title='<?=$action->label?>' 
								value='<?=$action->label?>' 
								onclick='document.<?=$this->data->name?>.action_<?=$this->data->name?>_<?=$action->name?>()'
							>
								<?=$label?>
							</button>
							<?php
							}
							
						// bottoni di esportazione (csv, xls, pdf)
//						$this->setExportActions();
						?>
						
					</div>
				</div>
				
				<?php
				// controlli per l'immissione testo di ricerca rapida
				$this->setQuickSearchControls();
				?>
				
			</div>
		</form>
			
		
		<?php
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		?>
		<th style='text-align: center'>Delete</th>
		<th></th>
		<?php
		}
		
	//**************************************************************************
	protected function setRowActions(\waLibs\waTableDataRow $row)
		{
		$tblName = $this->data->name;

		?>
		<td style='text-align: center'>
			<?php if ($row->enableableActions[$this->getIdxFromName($this->data->recordActions, "Delete")]) : ?>
				<!-- checkbox per cancellazione -->
				<input 
					type='checkbox' 
					class='form-control'
					name='watable_input_del_chk[<?=$row->id?>]' 
					value='1' 
					onclick='document.<?=$this->data->name?>.event_CheckBoxDelete_onclick("<?=$row->id?>")' 
				>
			<?php endif;?>
			</xsl:if>
			<!-- checkbox di modifica: viene valorizzato al momento del -->
			<!-- submit se il record ha subito modifiche -->
			<input type='checkbox' name='watable_input_mod_chk[<?=$row->id?>]' value='1' style='display: none;'/>
		</td>
		
		<!--colonna delle azioni su record--> 
		<td style='width: 1%'>
			<div class='btn-group'>
				<?php
				foreach ($row->enableableActions as $idx => $enabled)
					{
					if ($enabled && 
							// eliminaimo le azioni di default
							$this->data->recordActions[$idx]->name != "Details" && 
							$this->data->recordActions[$idx]->name != "Edit" && 
							$this->data->recordActions[$idx]->name != "Delete")
						{
                        $label = $this->data->recordActions[$idx]->label;
                        $label = $label == "Details" ? "Dettaglio" : $label;
                        $label = $label == "Edit" ? "Modifica" : $label;
                        $label = $label == "Delete" ? "Elimina" : $label;
						?>
						<button  
							type='button' 
							class='btn'
							onclick='document.<?=$tblName?>.action_<?=$tblName?>_<?=$this->data->recordActions[$idx]->name?>("<?=$row->id?>")'
							>
							<?=$label?>
						</button>
						<?php
						}
					}
				?>
			</div>
		</td>
		<?php
		}
		
	//**************************************************************************
	protected function setCell(\waLibs\waTableDataRow $row, $cellValue, $idx)
		{
		$hdr = $this->data->columnHeaders[$idx];
		if ($hdr->input)
			{
			$this->setInputCell($hdr, $cellValue, $row->id);
			}
		else
			{
			parent::setCell($row, $cellValue, $idx);
			}

		}
		
	//**************************************************************************
	protected function setTotalsRow()
		{
		if (!$this->data->totalsRow)
			return;
		
		?>
		<tfoot>
			<tr>
				<th></th>
				<th></th>
				
				<?php
				$this->setTotalsRowData();
				?>
				
			</tr>
		</tfoot>
		<?php
		}
		
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		$tblName = $this->data->name;

		?>
		
		<script type='text/javascript'>
			// inizializzazione parametri tabella <?=$tblName?>
			
			document.<?=$tblName?> = new waTable_edit('<?=$tblName?>', '<?=$this->data->columnHeaders[0]->name?>', '<?=$this->data->exclusiveSelection ? 1 : 0 ?>', '<?=$this->data->formPage?>');

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				new waRow(document.<?=$tblName?>, '<?=$row->id?>', <?=$this->row2json($row)?>);
				<?php
				}

			echo "// inizializzazione delle proprieta' delle columns per gestione input\n";
			foreach ($this->data->columnHeaders as $idx => $hdr)
				{
				if (!$hdr->show) continue;
				?>
				new waColumn_edit(document.<?=$tblName?>, "<?=$hdr->name?>", "<?=$hdr->label?>", "<?=$hdr->fieldType?>", "<?=$hdr->input->type?>", "<?=$hdr->input->mandatory ? 1 : 0 ?>");
				<?php
				}
			?>

		</script>		
		
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$alignment = "left";
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_R ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_C ? "center" : $alignment;
		?>
		<td style='text-align: <?=$alignment?>'>
			
			<?php
			$method_name = "setInputCell_" . strtolower(substr($hdr->input->type, strlen("WATABLE_INPUT_")));
			if (method_exists($this, $method_name))
				{
				$this->$method_name($hdr, $cellValue, $row_id);
				}
			else
				{
				echo nl2br(htmlspecialchars($cellValue));
				}
			?>
			
		</td>
		<?php
			
		}
		
	//**************************************************************************
	protected function setInputCell_textarea(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<textarea 
			class='form-control' 
			name='<?="$hdr->name[$row_id]"?>'
		><?=$cellValue?></textarea>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_date(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		$value = strlen($cellValue) ? 
								substr($cellValue, 8, 2) . "/" .
								substr($cellValue, 5, 2) . "/" .
								substr($cellValue, 0, 4) 
							: "" ;
		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$value?>' 
				size='10'
				maxlength='10' 
				style='text-align: center;'
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'DD/MM/YYYY',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_datetime(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		$value = strlen($cellValue) ? 
								substr($cellValue, 8, 2) . "/" .
								substr($cellValue, 5, 2) . "/" .
											substr($cellValue, 0, 4) . " " .
											substr($cellValue, 11, 2) . ":" .
											substr($cellValue, 14, 2) . ":" .
											substr($cellValue, 17, 2)
							: "" ;

		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				size='19' 
				maxlength='19' 
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$value?>' 
				style='text-align: center;' 
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'DD/MM/YYYY HH:mm:SS',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_time(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				size='8' 
				maxlength='8' 
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$cellValue?>' 
				style='text-align: center' 
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'HH:mm:SS',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_boolean(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<input 
			type='checkbox' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='1'
			<?=$cellValue ? "checked='checked'" : ""?> 
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_select(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<select class='form-control' name='<?="$hdr->name[$row_id]"?>'>
			<?php foreach ($hdr->input->options as $option):?>
				<option 
					value="<?=$option->value?>"  
					<?=$cellValue == $option->value ? "selected='selected'" : ""?>  
				>
					<?=$option->text?>
			<?php endforeach; ?>
		</select>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_text(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen;
		$size = $maxlength > 20 ? 20 : $maxlength;
		?>
		<input 
			type='text' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$cellValue?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_integer(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen;
		$size = $maxlength > 20 ? 20 : $maxlength;
		?>
		<input 
			type='integer' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$cellValue?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
			style='text-align: right'
			onkeyup='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.integer_onkeyup("<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_currency(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen - 1;
		$size = $hdr->input->fieldMaxLen + 2;
		$value = number_format((float) $cellValue, $hdr->decimalDigitsNr, ",", ".");
		?>
		<input 
			type='text' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$value?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
			style='text-align: right'
			onfocus='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onfocus("<?=$row_id?>")'
			onkeyup='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onkeyup("<?=$row_id?>")'
			onblur='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onblur("<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waTableData $data)
		{
		$this->data = $data;
		$retval = array();
		
		if (is_array($_POST["watable_input_del_chk"]))
			{
			foreach ($_POST["watable_input_del_chk"] as $rec_id => $value)
				{
				$retval[$rec_id] = new \stdClass();
				$retval[$rec_id]->watable_delete = $value;
				}
			}
			
		foreach ($data->columnHeaders as $hdr)
			{
			if (!$hdr->input) continue;
			
			$method_name = "getInputValues_" . strtolower(substr($hdr->input->type, strlen("WATABLE_INPUT_")));
			if (method_exists($this, $method_name))
				{
				$values  = $this->$method_name($hdr);
				}
			else
				{
				$values  = $this->getInputValues_text($hdr);
				}
			
			$col_name = $hdr->name;
			foreach($values as $rec_id => $value)
				{
				$retval[$rec_id] = $retval[$rec_id] ? $retval[$rec_id] : new \stdClass();
				$retval[$rec_id]->$col_name = $value;
				}

			}
			
		return $retval;
			
		}
			
	//**************************************************************************
	protected function getInputValues_text(\waLibs\waTableDataColumnHeader $hdr)
		{
		$retval = array();
		foreach ($_POST[$hdr->name] as $rec_id => $value)
			{
			if ($_POST["watable_input_mod_chk"][$rec_id])
				{
				$retval[$rec_id] = $value;
				}
			}
			
		return $retval;
		}
		
	//**************************************************************************
	protected function getInputValues_date(\waLibs\waTableDataColumnHeader $hdr)
		{
		$retval = array();
		foreach ($_POST[$hdr->name] as $rec_id => $value)
			{
			if ($_POST["watable_input_mod_chk"][$rec_id])
				{
				$retval[$rec_id] = $value ? 
									substr($value, 6, 4) . "-" .
									substr($value, 3, 2) . "-" .
									substr($value, 0, 2)
									: "";
				}
			}
			
		return $retval;
		}
		
	//**************************************************************************
	protected function getInputValues_datetime(\waLibs\waTableDataColumnHeader $hdr)
		{
		$retval = array();
		foreach ($_POST[$hdr->name] as $rec_id => $value)
			{
			if ($_POST["watable_input_mod_chk"][$rec_id])
				{
				$retval[$rec_id] = $value ? 
									substr($value, 6, 4) . "-" .
									substr($value, 3, 2) . "-" .
									substr($value, 0, 2) . " " .
									substr($value, 11, 2) . ":" .
									substr($value, 14, 2) . ":" .
									substr($value, 17, 2)
									: "";
				}
			}
			
		return $retval;
		}
		
	//**************************************************************************
	protected function getInputValues_time(\waLibs\waTableDataColumnHeader $hdr)
		{
		$retval = array();
		foreach ($_POST[$hdr->name] as $rec_id => $value)
			{
			if ($_POST["watable_input_mod_chk"][$rec_id])
				{
				$retval[$rec_id] = $value ? 
									substr($value, 0, 2) . ":" .
									substr($value, 3, 2) . ":" .
									substr($value, 6, 2)
									: "";
				}
			}
			
		return $retval;
		}
		
	//**************************************************************************
	protected function getInputValues_currency(\waLibs\waTableDataColumnHeader $hdr)
		{
		$retval = array();
		foreach ($_POST[$hdr->name] as $rec_id => $value)
			{
			if ($_POST["watable_input_mod_chk"][$rec_id])
				{
				$value = str_replace(".", "", $value);
				$retval[$rec_id] = str_replace(",", ".", $value);
				}
			}
			
		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


