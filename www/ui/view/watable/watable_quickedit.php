<?php 
namespace followthesmell;
include_once __DIR__ . "/watable_edit.php";
	
//******************************************************************************
class watable_quickedit_view extends watable_edit_view
	{
	
	//**************************************************************************
	protected function setJavascriptLink()
		{
		parent::setJavascriptLink();
		?>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_quickedit/js/watable_quickedit.js'></script>
		<?php
		}
		
	//**************************************************************************
	protected function setPageActions()
		{
		$tblName = $this->data->name;
		?>
		
			
		<form action='<?=$this->data->uri?>' id='<?=$this->data->name?>_page_actions' class='watable' onsubmit='return document.<?=$this->data->name?>.quickSearch()'>
			<div class="row">
				
				<div class='col-sm-12'>
					<div class='btn-group'>
						<?php
						foreach ($this->data->pageActions as $action)
							{
							// il bottone new fa storia a se
							$function = $action->name == "New" ? 
											"document.$tblName.action_quickNew" : 
											"document.$tblName.action_$tblName" . "_$action->name";
							$label = $action->label;
							$label = $label == "New" ? "Nuovo" : $label;
							$label = $label == "Filter" ? "Filtro" : $label;
							$label = $label == "No Filter" ? "No filtro" : $label;
							?>
							<button 
								type='button' 
								class='btn'
								name='<?=$action->name?>' 
								id='<?=$action->name?>' 
								title='<?=$action->label?>' 
								value='<?=$action->label?>' 
								onclick='<?=$function?>()'
							>
								<?=$label?>
							</button>
							<?php
							}

						// bottoni di esportazione (csv, xls, pdf)
//						$this->setExportActions();
						?>
						
					</div>
				</div>
				
				<?php
				// controlli per l'immissione testo di ricerca rapida
				$this->setQuickSearchControls();
				?>
				
			</div>
		</form>
			
		
		<?php
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		?>
		<th></th>
		<?php
		}
		
	//**************************************************************************
	protected function setRowActions(\waLibs\waTableDataRow $row)
		{
		$tblName = $this->data->name;
		
		?>
		<!--colonna delle azioni su record--> 
		<td style='width: 1%'>
			<div class='btn-group'>
				<!-- checkbox di modifica: viene valorizzato al momento dell' -->
				<!-- uscita da un campo -->
				<input type='checkbox' name='watable_input_mod_chk[<?=$row->id?>]' value='1' style='display: none;'/>
				<?php
				foreach ($row->enableableActions as $idx => $enabled)
					{
					if ($enabled &&
							// eliminaimo le azioni di default
							$this->data->recordActions[$idx]->name != "Details" && 
							$this->data->recordActions[$idx]->name != "Edit")
						{
						// il bottone delete fa storia a se
						$function = $this->data->recordActions[$idx]->name == "Delete" ? 
									"document.$tblName.action_quickDelete" : 
									"document.$tblName.action_$tblName" . "_" . $this->data->recordActions[$idx]->name;
                        $label = $this->data->recordActions[$idx]->label;
                        $label = $label == "Details" ? "Dettaglio" : $label;
                        $label = $label == "Edit" ? "Modifica" : $label;
                        $label = $label == "Delete" ? "Elimina" : $label;
						?>
						<button  
							type='button' 
							class='btn'
							onclick='<?=$function?>("<?=$row->id?>")'
							>
							<?=$label?>
						</button>
						<?php
						}
					}
				?>
			</div>
		</td>
		<?php
		}
		
	//**************************************************************************
	protected function setTotalsRow()
		{
		if (!$this->data->totalsRow)
			{
			return;
			}
		?>
		<tfoot>
			<tr>
				<th></th>

				<?php
				$this->setTotalsRowData();
				?>

			</tr>
		</tfoot>
		<?php
		}
		
	//**************************************************************************
	protected function setRows()
		{
		$tblName = $this->data->name;

		// creazione dei dati della riga fittizia per la clonazione
		$row = new \waLibs\waTableDataRow();
		$row->id = "___xxx___";
		$row->cells = array();
		$row->enableableActions = array();
		foreach ($this->data->columnHeaders as $idx => $hdr)
			{
			$row->cells[$idx] = $idx == 0 ? $row->id : "";
			}
		foreach ($this->data->recordActions as $idx => $action)
			{
			$row->enableableActions[$idx] = true;
			}
		?>
		<tbody>
			<!--riga fittizia da clonare-->
			<tr id='row_<?=$tblName?>_<?=$row->id?>' onclick='<?="document.$tblName.rows[\"$row->id\"]"?>.changeStatus()' style='display: none;'>

				<?php
				$this->setRowActions($row);
				$this->setRowData($row);
				?>

			</tr>

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				<tr id='row_<?=$tblName?>_<?=$row->id?>' onclick='<?="document.$tblName.rows[\"$row->id\"]"?>.changeStatus()'>
					
					<?php
					$this->setRowActions($row);
					$this->setRowData($row);
					?>
					
				</tr>
				<?php
				}
			?>
		</tbody>
		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		$tblName = $this->data->name;

		?>
		
		<script type='text/javascript'>
			// inizializzazione parametri tabella <?=$tblName?>
			
			document.<?=$tblName?> = new watable_quickedit('<?=$tblName?>', '<?=$this->data->columnHeaders[0]->name?>', '<?=$this->data->exclusiveSelection ? 1 : 0 ?>', '<?=$this->data->formPage?>');

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				new waRow(document.<?=$tblName?>, '<?=$row->id?>', <?=$this->row2json($row)?>);
				<?php
				}

			echo "// inizializzazione delle proprieta' delle columns per gestione input\n";
			foreach ($this->data->columnHeaders as $idx => $hdr)
				{
				if (!$hdr->show) continue;
				?>
				new waColumn_edit(document.<?=$tblName?>, "<?=$hdr->name?>", "<?=$hdr->label?>", "<?=$hdr->fieldType?>", "<?=$hdr->input->type?>", "<?=$hdr->input->mandatory ? 1 : 0 ?>");
				<?php
				}
			?>

		</script>		
		
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_textarea(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<textarea 
			class='form-control' 
			name='<?="$hdr->name[$row_id]"?>'
			onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		><?=$cellValue?></textarea>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_date(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		$value = strlen($cellValue) ? 
								substr($cellValue, 8, 2) . "/" .
								substr($cellValue, 5, 2) . "/" .
								substr($cellValue, 0, 4) 
							: "" ;
		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$value?>' 
				size='10'
				maxlength='10' 
				style='text-align: center;'
				onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'DD/MM/YYYY',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_datetime(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		$value = strlen($cellValue) ? 
								substr($cellValue, 8, 2) . "/" .
								substr($cellValue, 5, 2) . "/" .
											substr($cellValue, 0, 4) . " " .
											substr($cellValue, 11, 2) . ":" .
											substr($cellValue, 14, 2) . ":" .
											substr($cellValue, 17, 2)
							: "" ;

		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				size='19' 
				maxlength='19' 
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$value?>' 
				style='text-align: center;' 
				onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'DD/MM/YYYY HH:mm:SS',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_time(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$control_id = $this->data->name . "_" . $hdr->name . "_" . $row_id;
		?>
		<div class='input-group date' id='<?=$control_id?>'>
			<input 
				type='text' 
				class='form-control'
				size='8' 
				maxlength='8' 
				name='<?="$hdr->name[$row_id]"?>' 
				value='<?=$cellValue?>' 
				style='text-align: center' 
				onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
			>
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>		
		</div>
		
		<script type="text/javascript">
            jQuery(function () 
				{
                jQuery('#<?=$control_id?>').datetimepicker
					(
						{
						format: 'HH:mm:SS',
						widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
						}
					);
				}
			);
           
        </script>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_boolean(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<input 
			type='checkbox' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='1'
			<?=$cellValue ? "checked='checked'" : ""?> 
			onclick='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_select(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		?>
		<select 
			class='form-control' 
			name='<?="$hdr->name[$row_id]"?>'
			onchange='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		>
			<?php foreach ($hdr->input->options as $option):?>
				<option 
					value="<?=$option->value?>"  
					<?=$cellValue == $option->value ? "selected='selected'" : ""?>  
				>
					<?=$option->text?>
			<?php endforeach; ?>
		</select>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_text(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen;
		$size = $maxlength > 20 ? 20 : $maxlength;
		?>
		<input 
			type='text' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$cellValue?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
			onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_integer(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen;
		$size = $maxlength > 20 ? 20 : $maxlength;
		?>
		<input 
			type='integer' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$cellValue?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
			style='text-align: right'
			onkeyup='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.integer_onkeyup("<?=$row_id?>")'
			onblur='return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	protected function setInputCell_currency(\waLibs\waTableDataColumnHeader $hdr, $cellValue, $row_id)
		{
		$maxlength = $hdr->input->fieldMaxLen - 1;
		$size = $hdr->input->fieldMaxLen + 2;
		$value = number_format((float) $cellValue, $hdr->decimalDigitsNr, ",", ".");
		?>
		<input 
			type='text' 
			class='form-control'
			name='<?="$hdr->name[$row_id]"?>' 
			value='<?=$value?>' 
			maxlength='<?=$maxlength?>' 
			size='<?=$size?>' 
			style='text-align: right'
			onfocus='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onfocus("<?=$row_id?>")'
			onkeyup='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onkeyup("<?=$row_id?>")'
			onblur='document.<?=$this->data->name?>.columns.<?=$hdr->name?>.currency_onblur("<?=$row_id?>"); return document.<?=$this->data->name?>.action_quickEdit("<?=$hdr->name?>", "<?=$row_id?>")'
		>
		<?php
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		$this->data = $data;
		
		foreach ($data->columnHeaders as $hdr)
			{
			if ($hdr->name != $_POST["watable_rpc_column_name"])
				{
				continue;
				}

			$method_name = "getInputValue_" . strtolower(substr($hdr->input->type, strlen("WATABLE_INPUT_")));
			if (method_exists($this, $method_name))
				{
				return $this->$method_name($_POST["watable_rpc_value"]);
				}

			return $_POST["watable_rpc_value"];
			}

			
		}
			
	//**************************************************************************
	protected function getInputValue_date($value)
		{
		return $value ? 
						substr($value, 6, 4) . "-" .
						substr($value, 3, 2) . "-" .
						substr($value, 0, 2)
					: "";
		}
		
	//**************************************************************************
	protected function getInputValue_datetime($value)
		{
		return $value ? 
						substr($value, 6, 4) . "-" .
						substr($value, 3, 2) . "-" .
						substr($value, 0, 2) . " " .
						substr($value, 11, 2) . ":" .
						substr($value, 14, 2) . ":" .
						substr($value, 17, 2)
					: "";
		}
		
	//**************************************************************************
	protected function getInputValue_time($value)
		{
		return $value ? 
						substr($value, 0, 2) . ":" .
						substr($value, 3, 2) . ":" .
						substr($value, 6, 2)
					: "";
		}
		
	//**************************************************************************
	protected function getInputValue_currency($value)
		{
		$value = str_replace(".", "", $value);
		return str_replace(",", ".", $value);
		}
		
	//**************************************************************************
	}
//******************************************************************************


