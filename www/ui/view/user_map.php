<?php 
namespace followthesmell;
include_once __DIR__ . "/waapplication/waapplication.php";

//******************************************************************************
class user_map_view extends waapplication_view
	{
	
	/**
	 * dati in input
	 * @var waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;

		$this->setHead();
		$this->setItem($this->data->page->items["waMenu"]);
		$this->setTitle($this->data->page->items["title"]);

		if (!$this->data->page->items["waForm"]) 
			{
			?>
			<div style="height: 2rem;"></div>
			<style type="text/css">
				html, body, #map-canvas { height: 90%; margin: 3px; padding: 0;}
			</style>
			<script type="text/javascript">
				var markers = [<?php

									// aggiungiamo sempre Agrienergia
									echo "{" .
											"id: 0," .
											"lat: 44.6881295," .
											"lng: 11.4243000," .
											"user_name:'AgriEnergia'," .
											"address:'Via Fontana, 1097 San Pietro in Casale BO, Italia'" .
											"},\n";
									foreach ($data->page->items["user_list"]->value as $user)
										{
										$notes = str_replace("\n", " - ", str_replace("\r\n", " - ", $user->notes));
										echo "{" .
												"id: $user->id," .
												"lat: $user->latitude," .
												"lng: $user->longitude," .
												"user_name:'" . addslashes("$user->first_name $user->last_name") . "'," .
												"address:'" . addslashes($user->address) . "'" .
												"},\n";
										}

								?>];
				google.maps.event.addDomListener(window, 'load', function() {document.waPage.showMap("map-canvas", markers, true)});
			</script>
			<div id="map-canvas">
			</div>

			<?php 
			}
		else
			{
			$this->setItem($this->data->page->items["waForm"]);
			}
		$this->setVersiondata($this->data->page->items["version_data"]);
		$this->setPageResources();
		$this->setJavascriptObject();
		$this->setFooter();
		
		}
		
	//**************************************************************************
	protected function setItem(\waLibs\waApplicationDataPageItem $item = null)
		{
		if (!$item)
			{
			return;
			}
			
		?>
		<div class="waapplication_<?=$item->name?>">
			<?=$item->value?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	}
//******************************************************************************


