<?php 
namespace followthesmell;
include_once __DIR__ . "/waapplication/waapplication.php";

//******************************************************************************
class frm_map_view extends waapplication_view
	{
	
	/**
	 * dati in input
	 * @var waLibs\waApplicationData
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData $data)
		{
		$this->data = $data;

		$this->setHead();
		$this->setItem($this->data->page->items["waMenu"]);
		$hit_nr = count($data->page->items["hit_list"]->value);
		$this->data->page->items["title"]->value .= $hit_nr ? " ($hit_nr)" : "";
		$this->setTitle($this->data->page->items["title"]);

		?>
		<div style="height: 2rem;"></div>
		<style type="text/css">
			html, body, #map-canvas { height: 90%; margin: 3px; padding: 0;}
		</style>
		<script type="text/javascript">
			var markers = [<?php

								// aggiungiamo sempre Agrienergia
								echo "{" .
										"id: 0," .
										"lat: 44.6881295," .
										"lng: 11.4243000," .
										"user_name:'AgriEnergia'," .
										"notes:''," .
										"registered_at:''" .
										"},\n";
								foreach ($data->page->items["hit_list"]->value as $hit)
									{
									// dati meteo (openWeather/Sebastiano/Marco)
									$openWeatherData = null;
									$sebastianoData = null;
									$marcoData = null;
									if ($hit->weather_blob) 
										{
										$openWeatherData = (Object) json_decode($hit->weather_blob, true);
										}
									if ($hit->meteo_sebastiano) 
										{
										$sebastianoData = $hit->meteo_sebastiano;
										}
									if ($hit->meteo_marco) 
										{
										$marcoData = $hit->meteo_marco;
										}
									$dir_vento = "dir. vento: " . ($openWeatherData ? $this->degToCompass($openWeatherData->wind_deg) . " (" . $openWeatherData->wind_deg . ")" : "") . "/" . ($sebastianoData ? $sebastianoData->wind_dir : "") . "/" . ($marcoData ? $this->degToCompass($marcoData->wind_dir) . " (" . $marcoData->wind_dir . ")" : "");
									$vel_vento = "vel. vento: " . ($openWeatherData ? $openWeatherData->wind_speed : "") . "/" . ($sebastianoData ? $sebastianoData->wind_speed : "") . "/" . ($marcoData ? $marcoData->wind_speed : "");
									$temp = "temp.: " . ($openWeatherData ? $openWeatherData->temp : "") . "/" . ($sebastianoData ? $sebastianoData->temp_out : "") . "/" . ($marcoData ? $marcoData->temp_out : "");
									$press = "press.: " . ($openWeatherData ? $openWeatherData->pressure : "") . "/" . ($sebastianoData ? $sebastianoData->bar : "") . "/" . ($marcoData ? $marcoData->bar : "");
									
									$notes = str_replace("\n", " - ", str_replace("\r\n", " - ", $hit->notes));
									echo	"{" .
											"id: $hit->id," .
											"lat: $hit->latitude," .
											"lng: $hit->longitude," .
											"user_name:'" . addslashes($hit->user_name) . "'," .
											"dir_vento:'" . addslashes($dir_vento) . "'," .
											"vel_vento:'" . addslashes($vel_vento) . "'," .
											"temp:'" . addslashes($temp) . "'," .
											"press:'" . addslashes($press) . "'," .
											"notes:'" . addslashes($notes) . "'," .
											"registered_at:'" . addslashes(date("d/m/Y H:i", $hit->registered_at)) . "'" .
											"},\n";
									}

							?>];
			google.maps.event.addDomListener(window, 'load', function() {document.waPage.showMap("map-canvas", markers, true)});
		</script>
		<div id="map-canvas">
		</div>

		<?php 
		$this->setPeriodNavigation();
		$this->setItem($this->data->page->items["waForm"]);
			
//		echo "\n\n<div><a href='frm_map.php'>avanti</a></div>\n\n";
		$this->setVersiondata($this->data->page->items["version_data"]);
		$this->setPageResources();
		$this->setJavascriptObject();
		$this->setFooter();
		
		}
		
	//**************************************************************************
	protected function setItem(\waLibs\waApplicationDataPageItem $item = null)
		{
		if (!$item)
			{
			return;
			}
			
		?>
		<div class="waapplication_<?=$item->name?>">
			<?=$item->value?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	protected function setPeriodNavigation()
		{
		if (!$this->data->page->items["from"]->value || !$this->data->page->items["to"]->value)
			{
			return;
			}
		
		$from = $this->data->page->items["from"]->value;
		$to = $this->data->page->items["to"]->value;
		echo "<div style='float:left'>Indietro" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) - 1, date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) - 1, date("Y", $to)) . "'>giorno</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) - 7, date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) - 7, date("Y", $to)) . "'>settimana</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from) - 1, date("j", $from), date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to) - 1, date("j", $to), date("Y", $to)) . "'>mese</a>" .
				"</div>";
		echo "<div style='float:right'>Avanti" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) + 1, date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) + 1, date("Y", $to)) . "'>giorno</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from), date("j", $from) + 7, date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to), date("j", $to) + 7, date("Y", $to)) . "'>settimana</a>" .
				" <a href='?from=" . mktime(date("G", $from), date("i", $from), date("s", $from), date("n", $from) + 1, date("j", $from), date("Y", $from)) . "&to=" . mktime(date("G", $to), date("i", $to), date("s", $to), date("n", $to) + 1, date("j", $to), date("Y", $to)) . "'>mese</a>" .
				"</div><div style='clear: both; height: 0px;'></div>";
			
			
		}
		
	//**************************************************************************
	protected function degToCompass ($num) 
		{
		$val = floor(($num / 22.5) + 0.5);
		$arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"];
		return $arr[($val % 16)];
		}

	//**************************************************************************
	}
//******************************************************************************


