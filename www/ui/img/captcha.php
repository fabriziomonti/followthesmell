<?php
#######################
#Captcha php
// trovato da Giuseppe, rimpastato da bicio
#######################

// modificato rispetto a originale per mancanza di condivisione della sessione
/*Apro la sessione*/
// session_start();

/*Definisco l'immagine che verrà utilizzata come base per il captcha*/
$img = imagecreatefromjpeg(dirname(__FILE__) . "/base_captcha.jpg");

/*Definisco il colore del testo, in questo caso il bianco*/
$testo = imagecolorallocate($img, 255, 255, 255);

/*Definisco le dimensioni e le distanze dai bordi del testo*/
//imagestring($img, 12, 12, 2, $_SESSION["WAFORM_CAPTCHA_CODE_$_GET[k]"], $testo);

include __DIR__ . "/../../config.inc.php";
$key = file_get_contents(APPL_TMP_DIRECTORY . "/captchas/$_GET[k]");
unlink(APPL_TMP_DIRECTORY . "/captchas/$_GET[k]");

imagestring($img, 12, 12, 2, $key, $testo);
header("Content-type: image/jpeg");

/*Visualizzo l'immagine*/
imagejpeg($img);

