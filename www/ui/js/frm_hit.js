//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
		
		if (!this.form.controls.address.get())
			{
			// proviamo a reverse-geolocalizzare l'indirizzo
			navigator.geolocation.getCurrentPosition(this.fillAddress);
			}
			
		// se non abbiamo id e non c'è il check per inviare la mail, allora
		// siamo nel periodo in cui l'utente non può mandare mail
		if (!this.form.controls.id && !this.form.controls.is_mail_sent) {
			var msg = "Attenzione\\n\\nnon è ancora terminato il periodo di attesa" +
					" tra la tua ultima segnalazione e la sucessiva;" +
					" potrai registrare solamente segnalazioni a fini di storicizzazione," +
					" ma non inviare il messaggio PEC."
			setTimeout('document.waPage.alert("' + msg + '")', 0);
		}

		},
		
	//-------------------------------------------------------------------------
	event_onclick_waForm_is_mail_sent: function ()
		{
		var is_mail_sent = this.form.controls.is_mail_sent.get();
		this.form.controls.message_notes.enable(is_mail_sent, true);
		this.form.controls.message_notes.set("");
			
		},
		
	//-------------------------------------------------------------------------
	fillAddress: function (position)
		{
		var geocoder = new google.maps.Geocoder();
		var latlng = { lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude) };
		geocoder.geocode({ location: latlng }, function(results, status) {
			if (status === "OK" && results[0]) {
				var ctrlValue = {address : results[0].formatted_address, address_blob : JSON.stringify(results[0])};
				document.waPage.form.controls.address.set(ctrlValue);
			}
		});
		
		},
		
	//-------------------------------------------------------------------------
	formSubmit: function (form) {
	
		if (!this.validateForm(form)) {
			return false;
		}

		var ctrls = this.form.controls; 
		var registered_at = new Date(ctrls.registered_at.get()).getTime();
		if (registered_at > (new Date()).getTime()) {
			this.alert("La data della segnalazione non può essere futura");
			this.enable_submit_buttons(form, true);
			return false;
		}
		
		if (!ctrls.is_mail_sent || !ctrls.is_mail_sent.get()) {
			return form.gotSubmitConfirm();
		}
		
		registered_at = parseInt(registered_at / 1000);
		var address = ctrls.address.get().address;
		var confirmMsg = this.form.RPC("rpc_getMessageToSend", registered_at, address, ctrls.message_notes.get(), ctrls.id_intensity.get(), ctrls.id_duration.get());
		if (confirmMsg){
			confirmMsg = "Confermi l'invio della seguente segnalazione?" + confirmMsg;
			var proxy = this;
			this.confirm(confirmMsg, function() {proxy.enable_submit_buttons(form, false); form.gotSubmitConfirm()});
		}
		this.enable_submit_buttons(form, true);
	}
			
		
	
	//-------------------------------------------------------------------------
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
