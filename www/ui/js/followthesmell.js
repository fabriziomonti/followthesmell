//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var followthesmell = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waApplication,

	//-------------------------------------------------------------------------
	// proprieta'
	name				: "followthesmell",
	title				: "FollowTheSmell",
	iHaveAWindowOpen: false,
	isUserLogged: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
		
		// se proveniamo da un target blank annulliamo l'openeer
		try {var a = window.opener.title;} catch (e) {window.opener = false};
				
		// se siamo in una finestra child, nascondiamo il menu (che serve a 
		// gaspare per fare completare il suo diavolo di recog
		if (!window.opener && window.self === window.top)
			{
			jQuery("div.waapplication_waMenu div.navbar.navbar-default").show();
			}
			
		// trapping di ESC per chiusura pagina e nuovo record
		document.onkeyup = function (event) {document.waPage.trapKBShortcuts(event);}

		// overload del metodo di submit dei form
		var proxy = this;
	    for (var li in this.forms)
			{
	    	this.forms[li].submit = function() {proxy.formSubmit(this);};
	    	this.forms[li].delete = function() {proxy.formDelete(this);};
	    	this.forms[li].cancel = function() {proxy.formCancel(this);};
			}
			
		// init tooltips
		jQuery('[data-toggle="tooltip"]').tooltip();
             
		jQuery('body').addScrollTop();
		},
	
	//-------------------------------------------------------------------------
	// ridefiniamo la apri page per usare jquery e fancybox
	openPage: function (page)
	    {
		this.iHaveAWindowOpen = true;
		
		if (this.navigationMode == this.navigationModeWindow)
			return this.parent(page, window.innerWidth, window.innerHeight);
		
		jQuery("a.iframe").fancybox(
								{
								href : page,
								type : 'iframe',
								hideOnOverlayClick: false,
								width: '96%',
								height: '96%',
								fitToView   : false,
								autoSize    : false,
//								showCloseButton: false,
								speedIn	:	0, 
								speedOut :	0,
								padding: 0,
								margin: 15,
								onCleanup: function() 
											{
											if (self.frames[0] && 
													self.frames[0].document.waPage &&
													self.frames[0].document.waPage.closePage)
												return self.frames[0].document.waPage.closePage();
											}
								}
								);
	
			jQuery("a.iframe").click();

		},
	    
	//-------------------------------------------------------------------------
	// ridefiniamo la chiudi page per usare jquery e fancybox
	closePage: function ()
	    {
		// se non siamo in una finestra child, la chiusura porta sempre a index
		if (!window.opener && window.self == window.top)
			{
			location.href = "index.php";
			}
			
		var w = opener ? opener : parent;
		w.document.waPage.iHaveAWindowOpen = false;
		if (this.navigationMode == this.navigationModeWindow)
			return this.parent();

		parent.jQuery.fancybox.close();
		return true;
		},
	    
	//-------------------------------------------------------------------------
	trapKBShortcuts: function (event)
	    {
		if (this.iHaveAWindowOpen)
			return;
	
		event = event ? event : window.event;
		
		if (event.keyCode == 78 && event.altKey)
			{
			// lasciamo la gestione alla finestra figlia
			var button = document.getElementById("New");
			if (button)
				button.click();
			}
//		if (event.keyCode == 27)
//			{
//			if (this.form && this.form.controls && this.form.controls.cmd_cancel)
//				this.form.controls.cmd_cancel.obj.click();
//			if (this.table && document.forms[this.table.nome + "_page_actions"].Close)
//				document.forms[this.table.name + "_page_actions"].Close.click();
//				
//			}
		},
		
	//-------------------------------------------------------------------------
	// azione di chiusura delle pagine figlie con tabelle
	action_waTable_Close: function ()
	    {
	    this.closePage();
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la page contenente la table senza pagezione
	action_waTable_no_pagination: function ()
	    {
		var toGo = this.removeQSParam("watable_no_pagination");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watable_no_pagination=1";
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la page contenente la table con pagezione
	action_waTable_pagination: function ()
	    {
		var toGo = this.removeQSParam("watable_no_pagination");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watable_no_pagination=0";
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la page contenente la table con pagezione
	action_waTable_Delete: function (rowId)
	    {
		var table = this.table;
		this.confirm("Confermi eliminazione?", function() {table.openPage (table.getFormUri(table.actionDelete, rowId));});
	    },
	    
	//-------------------------------------------------------------------------
	// ovviamente questo metodo viene richiamato solo in situazioni standard
	// ossia quando c'e' un form che si chiama waForm e un button di
	// annullamento dell'editing che si chiama cmd_cancel; se siete in una
	// altra situzione dovete implementare la vostra gestione dell'evento
	event_onclick_waForm_cmd_cancel: function (event)
	    {
	    this.closePage();
	    },
	    
	//-------------------------------------------------------------------------
	enable_submit_buttons: function (form, enable)
		{
		if (form.controls.cmd_submit)
			form.controls.cmd_submit.enable(enable);
		if (form.controls.cmd_submit_close)
			form.controls.cmd_submit_close.enable(enable);
		if (form.controls.cmd_cancel)
			form.controls.cmd_cancel.enable(enable);
		if (form.controls.cmd_delete)
			form.controls.cmd_delete.enable(enable);

		},
		
	//-------------------------------------------------------------------------
	formCancel: function (form)
		{
		},
		
	//-------------------------------------------------------------------------
	formDelete: function (form)
		{
		this.confirm("Confermi eliminazione?", function() {form.gotDeleteConfirm();});
		},
		
	//-------------------------------------------------------------------------
	// valida form di default: disabilita i bottoni di submit, effettua i controlli,
	// ma non procede al submit
	validateForm: function (form)
		{
		this.enable_submit_buttons(form, false);
		
		var violations = form.getFormatViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += "Valore non valido in " +  violations[i].trim() + ".\n";
				}
			this.enable_submit_buttons(form, true);
			this.alert(msg);
			return false;
			}
			
		violations = form.getMandatoryViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += violations[i].trim() + " è obbligatorio.\n";
				}
			this.enable_submit_buttons(form, true);
			this.alert(msg);
			return false;
			}
			
		return true;
		},
		
	//-------------------------------------------------------------------------
	// valida form di default: disenable i bottoni di submit
	formSubmit: function (form) {
	
		if (this.validateForm(form)) {
			form.gotSubmitConfirm();
		}
			
	},
		
	//-------------------------------------------------------------------------
	placeChanged: function(place, blobField)
		{
		if(!place)
			return;

		this.form.controls[blobField].set(JSON.stringify(place));
		},
		
	//-------------------------------------------------------------------------
	/**
	 * metodo invocato alla pressione del tasto back di una showMessage
	 */
	showMessageBackButtonPressed: function()
		{
		history.back();
		},
		
	//-------------------------------------------------------------------------
	showMessageCloseButtonPressed: function()
		{
		if (window.top !== window.self || window.opener)
			{
			this.closePage();
			}
		else
			{
			location.href = "index.php";
			}
		},
		
	//-------------------------------------------------------------------------
	// visualizza/nasconde un controllo (la show del controllo non funziona
	// per via del link all'help)
	showControl: function(show, ctrl)
		{
		ctrl.show(show, true);
		if (show)
			jQuery("#waForm_" + ctrl.name + "_control_container").show();
		else
			jQuery("#waForm_" + ctrl.name + "_control_container").hide();
		},
		
	//-------------------------------------------------------------------------
	getCountryCode: function ()
		{
		var ret = "IT";

		try {
			var blob = jQuery("#waForm_address_blob").val().trim();
			var pass = JSON.parse(blob).address_components;

			for (var i = 0; i < pass.length; i++)
				{
				for (var li = 0; li < pass[i].types.length; li++)
					{
					if (pass[i].types[li] === "country")
						{
						return pass[i].short_name.toUpperCase();
						}
					}
				}
			}
		catch (e) 
			{
				
			}
			
		return ret;
		
		},
		
	//-------------------------------------------------------------------------
	getPostalCode: function ()
		{
		var ret = jQuery("#waForm_address_postal_code").val().trim();
		if (!ret)
			{
			ret = "20100";
			}

		return ret;
		
		},
		
	//-------------------------------------------------------------------------
	getCityName: function ()
		{
		var ret = jQuery("#waForm_address_city").val().trim();
		if (!ret)
			{
			ret = "Milano";
			}

		return ret;
		
		},
		
	//-------------------------------------------------------------------------
	getAddress: function ()
		{
		var ret = jQuery("#waForm_address").val().trim();
		if (!ret)
			{
			ret = "Via della Spiga";
			}

		return ret;
		
		},
		
	//-------------------------------------------------------------------------
	showHelp: function(section, page, control, title, label)
		{
		jQuery.ajax
			(
				{
				url			: "help_ajax.php?section=" + section + "&page=" + page + "&control=" + control,
				dataType	: 'json',
				success		: function (response, textStatus, jqXHR) {document.waPage.gotHelp(response, section, page, control, title, label);},
				error		: function (jqXHR, textStatus, errorThrown) {document.waPage.alert("help: errore di sistema")},
				timeout		: 20000
				}
			);
		},
		
	//**********************************************************************
	gotHelp: function(response, section, page, control, title, label)
		{
		var text = response.text ? response.text : "";
		if (response.is_sys_admin)
			{
			text += "<br/><br/>" +
					'<button type="button" class="btn btn-primary" onclick="document.waPage.openPage(\'frm_help.php' + 
						'?id=' + (response.id ? response.id : "") + 
						'&section=' + section + 
						'&page=' + page + 
						'&control=' + control + 
						'&title=' + escape(title) + 
						'&label=' + escape(label) + 
						'\')">' + 
						'Modifica'
					'</button>';
			}
		
		
		this.alert(text);
		}

	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

