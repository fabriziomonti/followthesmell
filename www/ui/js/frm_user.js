//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'
	forceTaxCode:	false,

	//-------------------------------------------------------------------------
	event_onchange_waForm_email: function ()
		{
		var email = this.form.controls.email.get();
		if (email == "")
			{
			return;
			}
			
		var exists = this.form.RPC("rpc_emailExists", email);
		if (exists)
			{
			this.form.controls.email.set("");
			return this.alert(email + " : email già utilizzata");
			}
			
			
		},
		
	//-------------------------------------------------------------------------
	// valida form di default: disenable i bottoni di submit
	formSubmit: function (form)
		{
		// verifichiamo che l'indirizzo sia valorizzato
		if (this.form.controls.address.get() == "") 
			{
			this.form.controls.address_blob.set("");
			}
		
		return this.parent(form);
		}
	
	//-------------------------------------------------------------------------
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
