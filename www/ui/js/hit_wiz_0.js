//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class({
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	event_onclick_waForm_cmd_google: function () {
		if (this.check_mandatory()) {
			location.href = "google_login.php?r=" + this.form.controls.g_redirect.get();
		}
	},

	//-------------------------------------------------------------------------
	event_onclick_waForm_cmd_no_google: function () {
		if (this.check_mandatory()) {
			location.href = "hit_wiz_1.php";
		}
	},

	//-------------------------------------------------------------------------
	check_mandatory: function () {
		var ctrls = this.form.controls;
		var msg = '';
		if (!ctrls.is_privacy_read.get()) {
			msg += "Leggere l'informativa sulla privacy è obbligatorio\n";
		}
		if (!ctrls.is_behavoiur_read.get()) {
			msg += "Leggere le norme di comportamento è obbligatorio\n";
		}
		if (msg) {
			this.alert(msg);
			return false;
		}
		return true;
	}


	//-------------------------------------------------------------------------
});

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
