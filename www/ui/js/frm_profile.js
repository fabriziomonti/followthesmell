//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
		
		// forziamo lo svuotamento della password, che dio fulmini gli 
		// informatici
		setTimeout('document.waPage.form.controls.new_pwd.set("")', 1000);

		if (!this.form.controls.address.get())
			{
			// proviamo a reverse-geolocalizzare l'indirizzo
			navigator.geolocation.getCurrentPosition(this.fillAddress);
			}
		},
		
	//-------------------------------------------------------------------------
	fillAddress: function (position)
		{
		var geocoder = new google.maps.Geocoder();
		var latlng = { lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude) };
		geocoder.geocode({ location: latlng }, function(results, status) {
			if (status === "OK" && results[0]) {
				var ctrlValue = {address : results[0].formatted_address, address_blob : JSON.stringify(results[0])};
				document.waPage.form.controls.address.set(ctrlValue);
			}
		});
		
		},
		
	//-------------------------------------------------------------------------
	event_onchange_waForm_email: function ()
		{
		var email = this.form.controls.email.get();
		if (email == "")
			{
			return;
			}
			
		var exists = this.form.RPC("rpc_emailExists", email);
		if (exists)
			{
			this.form.controls.email.set("");
			return this.alert(email + " : email già utilizzata");
			}
			
		},
		
	//-------------------------------------------------------------------------
	formSubmit: function (form)
		{
		if (this.form.controls.confirm_pwd.get() != "") 
			{
			if (this.form.controls.confirm_pwd.get() != this.form.controls.new_pwd.get()) 
				{
				return this.alert("Password non corrispondenti");
				}
			}
		
		return this.parent(form);
		}
		
	
	//-------------------------------------------------------------------------
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
