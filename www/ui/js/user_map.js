//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization  
	// la fa il parent
	
	//-------------------------------------------------------------------------
	showMap: function (containerId, markers) 
		{
		var mapOptions = 
			{
			zoom				: 1,
			styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]}],
			zoomControlOptions	:
				{
				style	: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.TOP_LEFT
				}
			};

		var map = new google.maps.Map(document.getElementById(containerId), mapOptions);
		var lat_lng = new Array();
		var latlngbounds = new google.maps.LatLngBounds();
		var colors = ["", "00FF00", "FF0000", "000000"];
		for (var i = 0; i < markers.length; i++) 
			{
			var data = markers[i];
			var myLatlng = new google.maps.LatLng(data.lat, data.lng);
			lat_lng.push(myLatlng);
			
			var marker = new google.maps.Marker
				(
					{
					position: myLatlng,
					map		: map,
					title	: data.user_name + "\n" + 
								data.address,
					icon	: "//chart.apis.google.com/chart?chst=d_map_pin_letter_withshadow&chld=P|" + colors[data.id === 0 ? 3 : 2] + "|000000"
					}
				);
			if (i === 0) 
				{
				latlngbounds.extend(marker.position);
				var extendPoint1 = new google.maps.LatLng(latlngbounds.getNorthEast().lat() + 0.015, latlngbounds.getNorthEast().lng() + 0.01);
				var extendPoint2 = new google.maps.LatLng(latlngbounds.getNorthEast().lat() - 0.015, latlngbounds.getNorthEast().lng() - 0.01);
				latlngbounds.extend(extendPoint1);
				latlngbounds.extend(extendPoint2);					
				}

//			google.maps.event.addListener
//				(
//				marker, 
//				'click', 
//				function(marker) 
//					{
//					return function() 
//						{
//						//if (marker.email == "" && marker.url_web == "" && marker.url_facebook == "" && marker.url_twitter == "") 
//						//	document.waPage.openPage("user_frm_group_contact.php?id_group=" + marker.id);
//						//else
//							document.waPage.showMarker(marker);
//						};
//					}(data)
//				);

			}

//		if (markers.length <= 1)
//			{
//			// allarghiamo lo zoom altrimenti con un punto solo è ridicolo
//			var extendPoint1 = new google.maps.LatLng(latlngbounds.getNorthEast().lat() + 0.01, latlngbounds.getNorthEast().lng() + 0.01);
//			var extendPoint2 = new google.maps.LatLng(latlngbounds.getNorthEast().lat() - 0.01, latlngbounds.getNorthEast().lng() - 0.01);
//			latlngbounds.extend(extendPoint1);
//			latlngbounds.extend(extendPoint2);					
//			}

		map.fitBounds(latlngbounds);

		},
		
	//-------------------------------------------------------------------------
	showMarker: function (marker) 
		{
		var msg = marker.type + "\n" + 
					marker.name + "\n" + 
					marker.city + " (" + marker.province + ")\n\n" +
					(marker.email ? "e-mail: <a href='mailto:" + marker.email + "'>" + marker.email + "</a>\n" : "" ) +
					(marker.url_web ? "web: <a href='javascript:var w=window.open(\"" + marker.url_web + "\")'>" + marker.url_web + "</a>\n" : "" ) +
					(marker.url_facebook ? "facebook: <a href='javascript:var w=window.open(\"" + marker.url_facebook + "\")'>" + marker.url_facebook + "</a>\n" : "" ) +
					(marker.url_twitter ? "twitter: <a href='javascript:var w=window.open(\"" + marker.url_twitter + "\")'>" + marker.url_twitter + "</a>\n" : "" ) +
					"\n<h3><a href='javascript:document.waPage.openPage(\"user_frm_group_contact.php?id_group=" + marker.id + "\")'>Contatta i portavoce</a></h3>\n" +
					"oppure\n" + 
					"\n<h3><a href='javascript:document.waPage.openPage(\"user_frm_soft_registration.php?id_group=" + marker.id + "\")'>Aderisci al " + marker.type + "</a></h3>\n";
		
		this.alert(msg);
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
