//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
		
		if (!this.form.controls.address.get()) {
			// proviamo a reverse-geolocalizzare l'indirizzo
			navigator.geolocation.getCurrentPosition(this.fillAddress);
		}
		
		},
		
	//-------------------------------------------------------------------------
	fillAddress: function (position)
		{
		var geocoder = new google.maps.Geocoder();
		var latlng = { lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude) };
		geocoder.geocode({ location: latlng }, function(results, status) {
			if (status === "OK" && results[0]) {
				var ctrlValue = {address : results[0].formatted_address, address_blob : JSON.stringify(results[0])};
				document.waPage.form.controls.address.set(ctrlValue);
			}
		});
		
		},
		
	//-------------------------------------------------------------------------
	// override
	event_onclick_waForm_cmd_cancel: function (event)
	    {
	    location.href = "logout.php";
	    },
	    
	//-------------------------------------------------------------------------
	event_onclick_waForm_cmd_back: function ()
		{
		history.back();
		}
		
	
	//-------------------------------------------------------------------------
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
