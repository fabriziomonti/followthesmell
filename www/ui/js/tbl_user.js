//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization  
	initialize: function()
		{
		this.parent();

		// mostriamo le foto
		var proxy = this;
		jQuery("#waTable td").each(function () {
			if (this.innerHTML.indexOf('<a href="javascript:document.waTable.link_waTable_image') !== -1 && this.innerText) {
				var id = this.innerHTML.split("&quot;")[1];
				var src = proxy.table.rows[id].fields.URLfoto;
				var newHtml = this.innerHTML.split(">")[0] + ">" + 
						"<div class='user_image' style='background-image: url(" + src + ");'></div>" +
						"</a>";
				this.innerHTML = newHtml;
			}
		});
		
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Accessi: function(id)
		{
		this.openPage("tbl_access.php?id_user=" + id);
		},
		
	//-------------------------------------------------------------------------
	link_waTable_email: function(id)
		{
		location.href = "mailto:" + this.table.rows[id].fields.email;
		},
		
	//-------------------------------------------------------------------------
	link_waTable_image: function(id)
		{
		var w = window.open(this.table.rows[id].fields.URLfoto);
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
