//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: followthesmell,

	//-------------------------------------------------------------------------
	// proprieta'
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
		
		// forziamo lo svuotamento della password, che dio fulmini gli 
		// informatici
		setTimeout('document.waPage.form.controls.pwd.set("")', 1000);

		// proviamo a reverse-geolocalizzare l'indirizzo
		navigator.geolocation.getCurrentPosition(this.fillAddress);

		},
		
	//-------------------------------------------------------------------------
	fillAddress: function (position)
		{
		var geocoder = new google.maps.Geocoder();
		var latlng = { lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude) };
		geocoder.geocode({ location: latlng }, function(results, status) {
			if (status === "OK" && results[0]) {
				var ctrlValue = {address : results[0].formatted_address, address_blob : JSON.stringify(results[0])};
				document.waPage.form.controls.address.set(ctrlValue);
			}
		});
		
		
		},
		
	//-------------------------------------------------------------------------
	formCancel: function (form)
		{
		location.href = "frm_login.php";
		},
		
	//-------------------------------------------------------------------------
	formSubmit: function (form)
		{
//		if (this.form.controls.email_confirm.get() != this.form.controls.email.get()) 
//			{
//			return this.alert("Gli indirizzi email non corrispondono");
//			}
		
		if (this.form.controls.confirm_pwd.get() != this.form.controls.pwd.get()) 
			{
			return this.alert("Password non corrispondenti");
			}
		
		// verifichiamo che l'indirizzo sia valorizzato
		if (this.form.controls.address.get() == "") 
			{
			this.form.controls.address_blob.set("");
			}
		
		return this.parent(form);
		}
		
	
	//-------------------------------------------------------------------------
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
