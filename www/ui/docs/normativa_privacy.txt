INFORMATIVA SUL TRATTAMENTO DEI DATI PERSONALI AI SENSI DELL’ART. 13 DEL REG. (UE) 2016/679.
In questa pagina vengono descritte le modalità di gestione del sito riguardo il trattamento dei dati personali degli utenti che lo consultano. Si tratta di un’informativa che è resa ai sensi dell’art. 13 del Regolamento (UE) 2016/679 sulla protezione dei dati personali (RGPD) a coloro che interagiscono con i servizi di “FollowTheSmell - applicazione odorosa”

Titolare del trattamento dei dati personali degli utenti raccolti in occasione dell’utilizzazione del sito web è Fabrizio Monti, abitante in Via Raveda 465/M, San Pietro in Casale (BO).

I dati saranno trattati dal titolare del trattamento per le finalità sotto indicate. 

Finalità e base giuridica del trattamento
I dati personali dell’utente verranno trattati per le seguenti finalità:
-prestazione dei servizi richiesti dall’utente

Il Titolare tratterà i dati personali dell’utente esclusivamente per l'operatività del servizio offerto, e in nessun altro modo. La base giuridica di questo trattamento è il consenso dell’interessato rilasciato al Titolare. L’utente, in qualsiasi momento, ha il diritto di opporsi al trattamento dei dati personali che lo riguardano nonché di chiederne la cancellazione contattando il Titolare del trattamento via e-mail, all’indirizzo: privacy.followthesmell@webappls.com .
La richiesta di cancellazione dati potrà essere eseguita integralmente solo qualora l'utente non abbia prodotto, attraverso l'uso del sito, documenti di natura giuridicamente rilevante: nel qual caso, i dati saranno rimossi dalla visibilità degli altri utenti, ma rimarranno a disposizione dell'amministratore di sistema per eventuali richieste da parte dell'autorità competente, come previsto dalla normativa sulla sicurezza informatica.
Il conferimento dei dati per tale finalità è facoltativo e i dati verranno trattati sino alla revoca del consenso e/o all’esercizio del diritto di opposizione.

Luogo del trattamento dei dati
I trattamenti connessi ai servizi web di questo dominio e hanno luogo presso la predetta sede e sono curati solo da personale tecnico incaricato del trattamento e debitamente istruito.

Tipi di dati trattati
Dati di navigazione. I sistemi informatici e le procedure software preposte al funzionamento dei siti web acquisiscono alcuni dati personali la cui trasmissione è implicita nell’uso dei protocolli di comunicazione di Internet.
Si tratta di informazioni che non sono raccolte per essere associate a interessati identificati, ma che per loro stessa natura potrebbero, attraverso elaborazioni ed associazioni con dati detenuti da terzi, permettere di identificare gli utenti.
In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli utenti che si connettono al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) delle risorse richieste, l’orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) ed altri parametri relativi al sistema operativo e all’ambiente informatico dell’utente.
Questi dati vengono utilizzati al solo fine di ricavare informazioni statistiche anonime sull’uso del sito e per controllarne il corretto funzionamento e vengono cancellati immediatamente dopo l’elaborazione. I dati potrebbero essere utilizzati per l’accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito: salva questa eventualità, allo stato i dati sui contatti web non persistono per più di 48 mesi.
Dati forniti volontariamente dall’utente. L’invio facoltativo, esplicito e volontario di posta elettronica agli indirizzi indicati sui siti comporta la successiva acquisizione dell’indirizzo del mittente, necessario per rispondere alle richieste, nonché degli eventuali altri dati personali inseriti nella comunicazione.
Specifiche informative di sintesi verranno riportate o visualizzate nelle pagine del sito predisposte per particolari servizi a richiesta.
Cookies. Per le informazioni relative ai cookies presenti sul sito web si rinvia alla Cookie Policy sempre leggibile in ogni pagina del sito.

Facoltatività del conferimento dei dati
Fatto salvo quanto specificato in merito ai dati di navigazione, l’utente è libero di fornire i dati personali qualora richiesto in apposite sezioni del sito. Il loro mancato conferimento può comportare l’impossibilità di ottenere quanto richiesto.

Modalità del trattamento
I dati personali sono trattati con strumenti telematici per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti. Specifiche misure di sicurezza sono osservate per prevenire la perdita dei dati, usi illeciti o non corretti ed accessi non autorizzati.

Periodo di conservazione dei dati
I dati non saranno conservati per un periodo di tempo superiore a quello necessario per soddisfare lo scopo per cui sono stati trattati. Per determinare il periodo di conservazione appropriato, saranno presi in considerazione la quantità, la natura e la sensibilità dei dati personali, gli scopi per i quali li trattiamo e se possiamo raggiungere tali scopi con altri mezzi.
Nel momento in cui non sarà più necessario conservare i dati personali degli utenti, saranno eliminati o distrutti in modo sicuro.

Diritti degli interessati
L’utente potrà, in qualsiasi momento, esercitare i diritti di seguito indicati.
Accesso ai dati personali: ottenere la conferma o meno che sia in corso un trattamento di dati che La riguardano e, in tal caso, l’accesso alle seguenti informazioni: le finalità, le categorie di dati, i destinatari, il periodo di conservazione, il diritto di proporre reclamo ad un’autorità di controllo, il diritto di richiedere la rettifica o cancellazione o limitazione del trattamento od opposizione al trattamento stesso nonché l’esistenza di un processo decisionale automatizzato;
Richiesta di rettifica o cancellazione degli stessi o limitazione dei trattamenti che lo riguardano; per “limitazione” si intende il contrassegno dei dati conservati con l’obiettivo di limitarne il trattamento in futuro;
Opposizione al trattamento: opporsi per motivi connessi alla Sua situazione particolare al trattamento di dati per l’esecuzione di un compito di interesse pubblico o per il perseguimento di un legittimo interesse del Titolare;
Portabilità dei dati: nel caso di trattamento automatizzato svolto sulla base del consenso o in esecuzione di un contratto, l’utente ha facoltà di ricevere in un formato strutturato, di uso comune e leggibile da dispositivo automatico, i dati che lo riguardano; in particolare, i dati Le verranno forniti dal Titolare in formato .xml, o analogo;
Proporre reclamo ai sensi dell’art. 77 RGPD all’autorità di controllo competente in base alla Sua residenza abituale, al luogo di lavoro oppure al luogo di violazione dei suoi diritti; per l’Italia è competente il Garante per la protezione dei dati personali, contattabile tramite i dati di contatto riportati sul sito web http://www.garanteprivacy.it.
I predetti diritti potranno essere esercitati inviando apposita richiesta al Titolare del trattamento mediante i canali di contatto indicati dalla presente informativa.
Le richieste relative all’esercizio dei diritti dell’utente saranno evase senza ingiustificato ritardo e, in ogni modo, entro un mese dalla domanda.

Modifiche all’informativa sulla privacy
La nostra Informativa sulla privacy potrebbe subire delle modifiche nel corso del tempo. Qualsiasi modifica sarà comunicata all’utente tramite un’email o un avviso sul nostro sito web.

