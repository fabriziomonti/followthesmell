<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Segnalazione - Meteo", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		//----------------------------------------------------------------------
		$this->form->addDateTime("registered_at", "Data e ora", true);
		$ctrl = $this->form->addTextArea("weather_blob", "Dati meteo", $readOnly || !$this->user->is_sys_admin);
		if (!$record->weather_blob && $this->user->is_sys_admin)
			{
			$this->setHitWeather($record);
			}
		
		$this->form_submitButtons($this->form, $readOnly || !$this->user->is_sys_admin, false);
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from hit" .
				" where id=" . $dbconn->sqlInteger($_GET["id"]) . 
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		$this->checkLockViolation($this->form);
			
		$dbconn = $this->form->recordset->dbConnection;
		$this->form->save();
		$this->setEditorData($record);
		
		$this->saveRecordset($record->recordset);
		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
