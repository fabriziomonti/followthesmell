<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		if (!$this->user->is_sys_admin) 
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, true);
			}
		
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$title = htmlspecialchars($_GET["title"]) . 
				($_GET["label"] ? "\n" . htmlspecialchars($_GET["label"]) : "");
		$this->addItem("Help\n$title" , "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$record = $this->form->recordset->records[0];
		$readOnly = false;

		$this->form->addGeneric("waTextArea_ext", "text", "Testo", $readOnly);
		
		$this->form_submitButtons($this->form, $readOnly, false);

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select * from help" .
				" where id=" . $dbconn->sqlInteger($_GET["id"]) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, $_GET['id'] ? 1 : 0);
		if ($_GET['id'] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->section = $_GET["section"];
			$record->page = $_GET["page"];
			$record->control = $_GET["control"];
			}
		else 
			{
			$this->checkLockViolation($this->form);
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		$this->form->save();
		$this->setEditorData($record);
		
		$this->saveRecordset($record->recordset);

		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
