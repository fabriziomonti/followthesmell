<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);

		$dbconn = $this->getDBConnection();
		$sql = "select * from help" .
				" where section=" . $dbconn->sqlString($_GET["section"]) .
				" and page=" . $dbconn->sqlString($_GET["page"]) .
				" and control=" . $dbconn->sqlString($_GET["control"] ? $_GET["control"] : "") .
				" and not is_deleted";
		$help_rec = $this->getRecordset($sql, $dbconn, 1)->records[0];
		
		$response = new stdClass();
		$response->id = $help_rec->id;
		$response->text = $help_rec->text;
		$response->is_sys_admin = $this->user->is_sys_admin;
		
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=utf-8");
		$response = json_encode($response, defined("JSON_PRETTY_PRINT") ? JSON_PRETTY_PRINT : 0);	
		
		exit($response);
		}


	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
