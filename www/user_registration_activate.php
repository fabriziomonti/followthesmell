<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);

		// leggiamo il blob
		$blob = "$this->directoryTmp/registration_blobs/$_GET[k]";
		if (!is_readable($blob))
			{
			$this->redirect($this->loginPage);
			}
			
		$input = (object) unserialize(file_get_contents($blob));
		@unlink($blob);
		if (!$input->email)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa");
			}
		
		$dbconn = $this->getDBConnection();
		$sql = "select user.*" .
				" from user" .
				" where email=" . $dbconn->sqlString($input->email) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if ($recordset->records[0])
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa");
			}
		
		$user = $recordset->add();
		
		$user->email = $input->email;
		$user->first_name = $input->first_name;
		$user->last_name = $input->last_name;
		$user->email = $input->email;
		$user->pwd = $input->pwd;
//		$user->phone = $input->phone;
		// determiniamo id_city e address_blob 
		$user->address = $input->address->address;
		$this->setLocationInfo($user, $input->address->address_blob);
		$this->setEditorData($user);
		$this->saveRecordset($user->recordset);
		
		$this->user = $this->record2Object($user);
		$this->logAccess($user->recordset->dbConnection);
		$this->redirect($this->startPage);
		}

		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
