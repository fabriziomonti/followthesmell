<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	private $hit_date;
	
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->childWindow = true;
		$this->addItem("Segnalazione - Meteo", "title");
		$this->addItem($this->getTableOpenWeather());
		$this->addItem($this->getTableFellow("sebastiano"));
		$this->addItem($this->getTableFellow("marco"));
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTableOpenWeather()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT hit.*" .
				" FROM hit" .
				" where not is_deleted" .
				" and id=" . $dbconn->sqlInteger($_GET["id_hit"]);
		$rs = $this->getRecordset($sql, $dbconn);
		$record = $rs->records[0];
		if (!$record) 
			{
			return null;
			}
		$this->hit_date = $record->registered_at;
		if (!$record->weather_blob) 
			{
			return null;
			}
			
		$data = json_decode($record->weather_blob, true);
		$data["weather"] = $data["weather"][0]["description"];
		$data["dt"] = date("d/m/Y H:i:s", $data["dt"]);
		$data["sunrise"] = date("H:i:s", $data["sunrise"]);
		$data["sunset"] = date("H:i:s", $data["sunset"]);

		$table = parent::getTable([$data]);
		$table->formPage = "frm_hit_weather.php?id_hit=$_GET[id_hit]";
		$table->title = "Dati OpenWeather";
		
		//----------------------------------------------------------------------
		// azioni...
		$table->removeAction("New");
		$table->removeAction("Edit");
		$table->removeAction("Delete");

		//----------------------------------------------------------------------
		// colonne...
		foreach ($data as $key => $val)
			{
			$table->addColumn($key, $key);
			}
		
		return $table;
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTableFellow($fellow)
		{
		if (!$this->hit_date)
			{
			return null;
			}
			
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT meteo.*" .
				" FROM meteo" .
				" where date(date_time)=" . $dbconn->sqlDate($this->hit_date) .
				" and fellow=" . $dbconn->sqlString($fellow) .
				" order by abs(TIMESTAMPDIFF(second, date_time, " . $dbconn->sqlDateTime($this->hit_date) . "))";
		$rs = $this->getRecordset($sql, $dbconn, 1);
		$record = $rs->records[0];
		if (!$record) 
			{
			return null;
			}
			
		$data = $this->record2Array($record);
		$data["date_time"] = date("d/m/Y H:i:s", $data["date_time"]);

		$table = parent::getTable([$data], "tbl_$fellow");
		$table->title = "Dati " . ucfirst($fellow);
		
		//----------------------------------------------------------------------
		// azioni...
		$table->removeAction("New");
		$table->removeAction("Edit");
		$table->removeAction("Delete");

		//----------------------------------------------------------------------
		// colonne...
		foreach ($data as $key => $val)
			{
			if ($key != "id" && $key != "fellow")
				{
				$table->addColumn($key, $key);
				}
			}
			
		return $table;
		}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();