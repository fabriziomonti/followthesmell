<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
	var $from;
	var $to;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->addItem($this->getMenu());
		$weekdays = ["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
		
		$this->createForm();
		$this->from = $this->form->isToUpdate() ? $this->form->from : intval($_GET["from"]);
		$this->to = $this->form->isToUpdate() ? $this->form->to : intval($_GET["to"]);
		$this->form->inputControls["from"]->value = $this->from;
		$this->form->inputControls["to"]->value = $this->to;
		$period = "";
		if ($this->from && $this->to) 
			{
			$period = " - da " . $weekdays[date("w", $this->from)] . date(" d/m/Y H:i", $this->from);
			$period .= " a " . $weekdays[date("w", $this->to)] . date(" d/m/Y H:i", $this->to);
			}
		$this->addItem("Mappa segnalazioni $period", "title");
		$this->addItem($this->form);
		$this->addItem($this->getList(), "hit_list");
		$this->addItem($this->from, "from");
		$this->addItem($this->to, "to");
		$this->show();

		}

	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		
		//----------------------------------------------------------------------
		$this->form->addDateTime("from", "Dalla data/ora", false, true);
		$this->form->addDateTime("to", "Alla data/ora", false, true);
		
		$this->form_submitButtons($this->form, false, false, "Aggiorna");
		$this->form->getInputValues();
		}

	//**************************************************************************
	function getList()
		{
		$dbconn = $this->getDBConnection();
		// cerchiamo i points
		$sql = "SELECT hit.*," .
				" concat(user.first_name, ' ', user.last_name) as user_name," .
				" if (hit.is_mail_sent, 'si', 'no') as mail_sent" .
				" FROM hit" .
				" join user on hit.id_user=user.id" .
				" where 1" .
				" and not hit.is_deleted" .
				" and hit.registered_at >= " . $dbconn->sqlDateTime($this->from) .
				" and hit.registered_at <= " . $dbconn->sqlDateTime($this->to) .
				" order by hit.registered_at desc";
		
		$hits = $this->getRecordset($sql, $dbconn, 2000);

		$list = [];
		$places = [];
		foreach ($hits->records as $hit)
			{
			while ($places["$hit->latitude$hit->longitude"])
				{
				$offset = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$offset2 = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$hit->latitude += $offset;
				$hit->longitude += $offset2;
				}
			$places["$hit->latitude$hit->longitude"] = 1;
			$list[] = $this->record2Object($hit);
			
			// dati meteo sebastiano
			$sql = "SELECT meteo.*" .
					" FROM meteo" .
					" where date(date_time)=" . $dbconn->sqlDate($hit->registered_at) .
					" and fellow=" . $dbconn->sqlString('sebastiano') .
					" order by abs(TIMESTAMPDIFF(second, date_time, " . $dbconn->sqlDateTime($hit->registered_at) . "))";
			$meteo = $this->getRecordset($sql, $dbconn, 1)->records[0];
			if ($meteo) 
				{
				$list[count($list) - 1]->meteo_sebastiano = $this->record2Object($meteo);
				}

				
			// dati meteo marco
			$sql = "SELECT meteo.*" .
					" FROM meteo" .
					" where date(date_time)=" . $dbconn->sqlDate($hit->registered_at) .
					" and fellow=" . $dbconn->sqlString('marco') .
					" order by abs(TIMESTAMPDIFF(second, date_time, " . $dbconn->sqlDateTime($hit->registered_at) . "))";
			$meteo = $this->getRecordset($sql, $dbconn, 1)->records[0];
			if ($meteo) 
				{
				$list[count($list) - 1]->meteo_marco = $this->record2Object($meteo);
				}
				
			}

		return $list;
		}

	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
