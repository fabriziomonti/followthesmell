<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
	var $from;
	var $to;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->addItem($this->getMenu());
		$weekdays = ["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
		
		$this->createForm();
		$this->from = $this->form->isToUpdate() ? $this->form->from : intval($_GET["from"]);
		$this->to = $this->form->isToUpdate() ? $this->form->to : intval($_GET["to"]);
		$this->form->inputControls["from"]->value = $this->from;
		$this->form->inputControls["to"]->value = $this->to;
		$period = "";
		if ($this->from && $this->to) 
			{
			$period = " - da " . $weekdays[date("w", $this->from)] . date(" d/m/Y", $this->from);
			$period .= " a " . $weekdays[date("w", $this->to)] . date(" d/m/Y", $this->to);
			}
		$this->addItem("Grafico giornaliero segnalazioni $period", "title");
		$this->addItem($this->form);
		$this->addItem($this->getList(), "hit_list");
		$this->addItem($this->from, "from");
		$this->addItem($this->to, "to");
		$this->show();

		}

	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		
		//----------------------------------------------------------------------
		$this->form->addDate("from", "Dalla data", false, true);
		$this->form->addDate("to", "Alla data", false, true);
		
		$this->form_submitButtons($this->form, false, false, "Aggiorna");
		$this->form->getInputValues();
		}

	//**************************************************************************
	function getList()
		{
		$dbconn = $this->getDBConnection();
		// cerchiamo i points
		$sql = "SELECT count(*) as how_many," .
				" date(registered_at) as registration_date" .
				" FROM hit" .
				" where 1" .
				" and not hit.is_deleted" .
				" and date(hit.registered_at) >= " . $dbconn->sqlDate($this->from) .
				" and date(hit.registered_at) <= " . $dbconn->sqlDate($this->to) .

				" group by registration_date" .
				" order by registration_date";
		
		$hits = $this->getRecordset($sql, $dbconn);
		$list = [];
		foreach ($hits->records as $hit)
			{
			$list[$hit->registration_date] = $hit->how_many;
			}
			
		$from = mktime(0,0,0, date("n", $this->from), date("j", $this->from), date("Y", $this->from));
		$to = mktime(0,0,0, date("n", $this->to), date("j", $this->to), date("Y", $this->to));
		for ($li = $from; $li <= $to; $li += 24*60*60)
			{
			$list[$li] = $list[$li] ? $list[$li] : 0;
			}
			
		ksort($list);
		
		return $list;
		
		
		
		
		
		
		
		
		
		
		
		
		$sql = "SELECT hit.*," .
				" concat(user.first_name, ' ', user.last_name) as user_name," .
				" if (hit.is_mail_sent, 'si', 'no') as mail_sent" .
				" FROM hit" .
				" join user on hit.id_user=user.id" .
				" where 1" .
				" and not hit.is_deleted" .
				" and hit.registered_at >= " . $dbconn->sqlDateTime($this->from) .
				" and hit.registered_at <= " . $dbconn->sqlDateTime($this->to) .
				" order by hit.registered_at desc";
		
		$hits = $this->getRecordset($sql, $dbconn, 2000);

		$list = [];
		$places = [];
		foreach ($hits->records as $hit)
			{
			while ($places["$hit->latitude$hit->longitude"])
				{
				$offset = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$offset2 = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$hit->latitude += $offset;
				$hit->longitude += $offset2;
				}
			$places["$hit->latitude$hit->longitude"] = 1;
			$list[] = $this->record2Object($hit);
			}

		return $list;
		}

	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
