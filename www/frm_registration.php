<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		if ($this->user) 
			{
			$this->redirect($this->startPage);
			}
			
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Registrati a $this->title", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$readOnly = false;
		
		$this->form->addText("last_name", "Cognome", $readOnly, !$readOnly);
		$this->form->addText("first_name", "Nome", $readOnly, !$readOnly);
		$this->form->addEmail("email", "Email", $readOnly, !$readOnly);
//		$this->form->addEmail("email_confirm", "Ripeti email", $readOnly, !$readOnly);
		$this->form->addPassword("pwd", "Password", $readOnly, !$readOnly);
		$this->form->addPassword("confirm_pwd", "Conferma password", $readOnly, !$readOnly);
//		$this->form->addText("phone", "Telefono", $readOnly, !$readOnly);
		$this->form_geoBlock($readOnly, !$readOnly);
		$this->form->addTextArea("normativa", "Informativa privacy", true)
			->value = $this->getPrivacyText();
		$this->form->addBoolean("is_privacy_read", "Ho letto l'informativa sulla privacy", $readOnly, true);
		$this->form->addTextArea("comportamento", "Norme di comportamento", true)
			->value = $this->getBehaviourText();
		$this->form->addBoolean("is_behavoiur_read", "Ho letto le norme di comportamento", $readOnly, true);
		$this->form->addCaptcha("captcha", "Codice di controllo", false, true);
			
		$this->form_submitButtons($this->form, $readOnly, false);

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select user.*" .
				" from user" .
				" where email=" . $dbconn->sqlString($_POST["email"]) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		if ($this->form->pwd != $this->form->confirm_pwd)
			{
			$this->showMessage("Password non corrispondenti", "Password non corrispondenti");
			}
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$key = $this->saveRegistrationInput($this->form->input);
			$this->sendMailNewUser($key, $this->form->input);
			}
		else
			{
			$this->sendMailExistingUser($this->form->input);
			}
		
		$this->showMessage("Operazione conclusa correttamente", 
							"L'operazione si è conclusa correttamente." .
							"<div class='pesante'>Leggi il messaggio email che ti è stato inviato per" .
							" attivare la tua registrazione.</div>" .
							APPL_THANKS_MSG, false, false);
		}
		
	//***************************************************************************
	function saveRegistrationInput($input)
		{
		if (!is_array($input))
			{
			$input = json_decode(json_encode($input), true);
			}
			
		$key = md5(rand(0, 999999999)) . microtime(1);
		@mkdir ("$this->directoryTmp/registration_blobs");
		$input["pwd"] = $this->encryptPassword($input["pwd"]);
//		unset($input["email_confirm"]);
		unset($input["confirm_pwd"]);
		unset($input["captha_id"]);
		unset($input["captha_value"]);
		
		file_put_contents("$this->directoryTmp/registration_blobs/$key", serialize($input));
		
		return $key;
		}
		
	//***************************************************************************
	function sendMailNewUser($key, $input)
		{
		if (!is_array($input))
			{
			$input = json_decode(json_encode($input), true);
			}
			
		$protocol = $this->getProtocol();
		$subject = "Attivazione [$this->title]";
		$body = "Ciao $input[first_name],\n\n" .
				"hai chiesto di registrarti a $this->title? " .
				" Se sì, segui questo link per confermare la tua registrazione:\n\n" .
				"$protocol://$this->domain$this->httpwd/user_registration_activate.php?k=$key\n\n" .
				"Altrimenti ti preghiamo di scusarci: qualche buontempone ha usato sconsideratamente" .
				" il tuo indirizzo di posta elettronica cercando di registrarsi presso" .
				" la nostra applicazione; in questo caso non devi fare assolutamente nulla," .
				" la registrazione non avrà alcun effetto e il tuo indirizzo email" .
				" non sarà salvato in alcun modo.\n\n" .
				"Ciao dallo staff di $this->title!\n\n" .
				"(non rispondere a questo messaggio: la casella di posta non è presidiata)";
		if (!$this->sendMail($input["email"], $subject, $body))
			{
			$this->showMessage("Errore invio messaggio email", 
								"Si è verificato un errore durante l'invio del messaggio di posta elettronica." .
								" Sei pregato di cercare di ripetere l'operazione o di avvertire " .
								"<a href='mailto:" . $this->supportEmail->address . "'>l'assistenza tecnica</a>.");
			
			}
		}
		
	//***************************************************************************
	function sendMailExistingUser($input)
		{
		if (!is_array($input))
			{
			$input = json_decode(json_encode($input), true);
			}
			
		$protocol = $this->getProtocol();
		$subject = "Attivazione [$this->title]";
		$body = "Ciao $input[first_name],\n\n" .
				"hai chiesto di registrarti a $this->title, ma sei già registrato.\n\n" .
				"Se hai dimenticato o non conosci la tua password puoi crearne una nuova" .
				" mediante la procedura prevista in questa pagina:" .
				"$protocol://$this->domain$this->httpwd/frm_reset_pwd.php\n\n" .
				"Altrimenti ti preghiamo di scusarci: qualche buontempone ha usato sconsideratamente" .
				" il tuo indirizzo di posta elettronica cercando di registrarsi presso" .
				" la nostra applicazione; in questo caso non devi fare assolutamente nulla.\n\n" .
				"Ciao dallo staff di $this->title!\n\n" .
				"(non rispondere a questo messaggio: la casella di posta non è presidiata)";
		if (!$this->sendMail($input["email"], $subject, $body))
			{
			$this->showMessage("Errore invio messaggio email", 
								"Si è verificato un errore durante l'invio del messaggio di posta elettronica." .
								" Sei pregato di cercare di ripetere l'operazione o di avvertire " .
								"<a href='mailto:" . $this->supportEmail->address . "'>l'assistenza tecnica</a>.");
			
			}
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
