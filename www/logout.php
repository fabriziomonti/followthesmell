<?php
include "followthesmell.inc.php";

$appl = new followthesmell();

// verifichiamo un caso particolare in cui: 
// - l'utente arriva qui perchè ha annullato un wizard
// - l'utente era loggato quando ha iniziato il wizard
// in questo caso non facciamo logout, gli resettiamo semplicemente il wizard
if ($appl->user && $appl->sessionData["hit_wiz_steps_data"] && $appl->sessionData["hit_wiz_steps_data"]->user_was_logged)
	{
	unset($appl->sessionData["hit_wiz_steps_data"]);
	$appl->redirect($appl->startPage);
	}

$appl->logAccess(null, 1);
$appl->sessionData = array();
$appl->redirect($appl->loginPage);
