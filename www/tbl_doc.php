<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->addItem($this->getMenu());
		
		$this->addItem("Documenti/Link", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$sql = "SELECT doc.*," .
				" 'http://www.repubblica.it' as URLdoc," .
				" if (doc.link is not null and doc.link != '', 'link', '') as label_link," .
				" concat(user.first_name, ' ', user.last_name) as user_name" .
				" FROM doc" .
				
				" join user on doc.id_user=user.id" .
				" where not doc.is_deleted" .
				" order by doc.registered_at desc";

		$table = parent::getTable($sql);
		$table->formPage = "frm_doc.php";
		
		//----------------------------------------------------------------------
		// azioni...
		$table->actions["Edit"]->enablingFunction = [$this, "isEditable"];
		$table->actions["Delete"]->enablingFunction = [$this, "isEditable"];

		//----------------------------------------------------------------------
		// colonne...
		$table->addColumn("id", "ID")->aliasOf = "doc.id";
		$table->addColumn("title", "Titolo")->aliasOf = "doc.title";
		$col = $table->addColumn("doc", "Documento");
			$col->aliasOf = "doc.doc";
			$col->link = true;
		$col = $table->addColumn("label_link", "Link");
			$col->aliasOf = "doc.link";
			$col->link = true;
		$table->addColumn("user_name", "Caricato da")->aliasOf = "concat(user.first_name, ' ', user.last_name)";
		$table->addColumn("registered_at", "Data/Ora")->aliasOf = "doc.registered_at";
		
		// colonne invisibili
		$table->addColumn("URLdoc", "URLdoc", false, false, false)
			->computeFunction = array($this, "getUrlDoc");
		$table->addColumn("link", "link", false, false, false);

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	/**
	 */
	function isEditable(waLibs\watable $table)
		{
		return $this->user->is_sys_admin || $table->record->id_user == $this->user->id;
		}
		
	//*****************************************************************************
	function getUrlDoc(waLibs\waTable $table)
		{
		return parent::getUrlDoc($table->record, "doc");
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();