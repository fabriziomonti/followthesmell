<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		if (!$stepsData || !$stepsData->email)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, false);
			}
		
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC - 2", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;
		
		$lbl = "Incolla qui il codice di controllo che hai ricevuto via email (controlla anche nello spam!), oppure" .
				" se sei già registrato puoi usare la tua password. Indifferentemente, uno dei due, è uguale.";
		$this->form->addText("control_code", $lbl, $readOnly, !$readOnly);
		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Avanti >>");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		// verifichiamo se l'utente è già registrato: per fare login può usare
		// indifferentemente il codice di controllo mandato via email o la sua
		// password
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from user" .
				" where email=" . $dbconn->sqlString($stepsData->email) .
				" and not is_deleted";
		$user = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if ($user && ($stepsData->control_code == $this->form->control_code || $this->checkPassword($this->form->control_code, $user->pwd)))
			{
			// riconosciuto utente registrato
			}
		elseif (!$user && $stepsData->control_code == $this->form->control_code)
			{
			// riconosciuto utente non registrato: creazione utente
			$user = $this->getRecordset($sql, $dbconn, 1)->add();
			$user->email = $stepsData->email;
			$user->pwd = $this->encryptPassword($stepsData->control_code);
			$this->setEditorData($user);
			$this->saveRecordset($user->recordset);
			}
		else
			{
			// non riconosciuto
			$this->showMessage("Codice di controllo non corretto", "Codice di controllo non corretto", true, false);
			}

		// login
		$this->user = $this->record2Object($user);
		$this->logAccess($user->recordset->dbConnection);
			
		// andiamo allo step successivo
		$stepsData->control_code_verified = true;
		$this->redirect("hit_wiz_3.php");
		}

	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
