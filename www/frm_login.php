<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		if ($this->user) 
			{
			$this->redirect($this->startPage);
			}

		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->doLogin();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* showPage
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("$this->title Login", "title");
		$this->addItem($this->form);

		$this->addItem("<a href='hit_wiz_0.php'>Fammi fare velocemente una segnalazione via PEC a ARPAE e basta! ...</a>", "hit_wiz_link");
		$this->addItem("<a href='google_login.php'>Ho un indirizzo Gmail e voglio entrare in $this->title...</a>", "google_login_link");
		$this->addItem("<a href='frm_registration.php'>Non ho un indirizzo Gmail e mi voglio registrare a $this->title...</a>", "registration_link");
		$this->addItem("<a href='frm_reset_pwd.php'>Ho dimenticato la password...</a>", "reset_pwd_link");
		$this->addItem("<a href='home.php'>Spiegami cos'è questa roba...</a>", "home_link");
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		
		$this->form->addText("email", "E-mail", false, true);
		$this->form->addPassword("pwd", "Password", false, true);
		new waLibs\waButton($this->form, 'cmd_submit', 'Login');
		
		$this->form->getInputValues();
		}
	
	//***************************************************************************
	function doLogin()
		{
		$this->checkMandatory($this->form);
		
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from user" .
				" where email=" . $dbconn->sqlString($this->form->email) .
				" and not is_deleted";
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record)
			{
			$this->showMessage("Login non permesso", "Login non permesso");
			}

		if (!$this->checkPassword($this->form->pwd, $record->pwd))
			{
			$this->showMessage("Login non permesso", "Login non permesso");
			}

		$this->user = $this->record2Object($record);
		$this->logAccess($record->recordset->dbConnection);
		$this->redirect($this->startPage);
		}
			    
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new page();
