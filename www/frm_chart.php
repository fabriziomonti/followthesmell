<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
	var $from;
	var $to;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->addItem($this->getMenu());
		$weekdays = ["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
		
		$this->createForm();
		$this->from = $this->form->isToUpdate() ? $this->form->from : intval($_GET["from"]);
		$this->to = $this->form->isToUpdate() ? $this->form->to : intval($_GET["to"]);
		$this->form->inputControls["from"]->value = $this->from;
		$this->form->inputControls["to"]->value = $this->to;
		$period = "";
		if ($this->from && $this->to) 
			{
			$period = " - da " . $weekdays[date("w", $this->from)] . date(" d/m/Y H:i", $this->from);
			$period .= " a " . $weekdays[date("w", $this->to)] . date(" d/m/Y H:i", $this->to);
			}
		$this->addItem("Grafico orario segnalazioni $period", "title");
		$this->addItem($this->form);
		$this->addItem($this->getList(), "hit_list");
		$this->addItem($this->from, "from");
		$this->addItem($this->to, "to");
		$this->show();

		}


	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		
		//----------------------------------------------------------------------
		$this->form->addDateTime("from", "Dalla data/ora", false, true);
		$this->form->addDateTime("to", "Alla data/ora", false, true);
		
		$this->form_submitButtons($this->form, false, false, "Aggiorna");
		$this->form->getInputValues();
		}

	//**************************************************************************
	function getList()
		{
		$dbconn = $this->getDBConnection();
		// cerchiamo i points
		$sql = "SELECT count(*) as how_many," .
				" date_format(registered_at, '%H') as registration_hour" .
				" FROM hit" .
				" where 1" .
				" and not hit.is_deleted" .
				" and hit.registered_at >= " . $dbconn->sqlDateTime($this->from) .
				" and hit.registered_at <= " . $dbconn->sqlDateTime($this->to) .

				" group by registration_hour" .
				" order by registration_hour";
		
		$hits = $this->getRecordset($sql, $dbconn);
		$list = [];
		foreach ($hits->records as $hit)
			{
			$list[$hit->registration_hour] = $hit->how_many;
			}
			
		for ($li = 0; $li < 24; $li++)
			{
			$key = str_pad($li, 2, "0", STR_PAD_LEFT);
			$list[$key] = $list[$key] ? $list[$key] : 0;
			}
			
		ksort($list);
		
		return $list;
		}

	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
