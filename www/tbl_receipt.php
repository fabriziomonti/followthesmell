<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->childWindow = true;
		$this->addItem("Ricevute", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT receipt.*" .
				" FROM receipt" .
				
				" where not is_deleted" .
				" and id_hit=" . $dbconn->sqlInteger($_GET["id_hit"]) .
				" order by registered_at desc";

		$table = parent::getTable($sql);
		$table->formPage = "frm_receipt.php?id_hit=$_GET[id_hit]";
		
		//----------------------------------------------------------------------
		// azioni...
		if (!$this->user->is_sys_admin)
			{
			$table->removeAction("New");
			}
		$table->actions["Edit"]->enablingFunction = [$this, "isEditable"];
		$table->actions["Delete"]->enablingFunction = [$this, "isEditable"];

		//----------------------------------------------------------------------
		// colonne...
		$table->addColumn("id", "ID");
		$table->addColumn("sender", "Mittente");
		$table->addColumn("subject", "Oggetto");
		$table->addColumn("registered_at", "Data/Ora");
		$col = $table->addColumn("doc", "Documento", true, true, true, \waLibs\waTable::ALIGN_C);
			$col->link = true;
		
		// colonne invisibili
		$table->addColumn("URLdoc", "URLdoc", false, false, false)
			->computeFunction = array($this, "getUrlDoc");

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	/**
	 */
	function isEditable(waLibs\watable $table)
		{
		return $this->user->is_sys_admin;
		}
		
	//*****************************************************************************
	function getUrlDoc(waLibs\waTable $table)
		{
		return parent::getUrlDoc($table->record, "doc");
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();