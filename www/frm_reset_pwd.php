<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		if ($this->user) 
			{
			$this->redirect($this->startPage);
			}
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("$this->title - Reset password", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$readOnly = false;
		
		$this->form->addText("email", "Email", $readOnly, !$readOnly);
		$this->form->addCaptcha("captcha", "Codice di controllo", $readOnly, !$readOnly);
		$this->form_submitButtons($this->form, $readOnly, false);

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select user.*" .
				" from user" .
				" where email=" . $dbconn->sqlString($_POST["email"]) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if ($record)
			{
			$this->sendMailPassword($record);
			}
		
		$this->showMessage("Operazione conclusa correttamente", 
							"L'operazione si è conclusa correttamente.<br><br>" .
							"Se l'indirizzo che hai indicato è presente nel nostro archivio," .
							" riceverai la nuova password per l'accesso a $this->title.<br><br>" .
							APPL_THANKS_MSG, false, true);
		}
		
	//***************************************************************************
	function sendMailPassword(waLibs\waRecord $record)
		{
		$pwd = $this->getPassword();
		$record->pwd = $this->encryptPassword($pwd);
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		
		$subject = "Reset password [$this->title]";
		$body = "Ciao $record->first_name,\n\n" .
				"questa è la tua nuova password per entrare in $this->title:\n\n" .
				"$pwd\n\n" .
				"Sei pregato di cambiarla il prima possible, entrando nell'applicazione" .
				" e modificando il tuo profilo (Strumenti->Profilo).\n\n" .
				"Ciao dallo staff di $this->title!\n\n" .
				"(non rispondere a questo messaggio: la casella di posta non è presidiata)";
		if (!$this->sendMail($record->email, $subject, $body))
			{
			$this->showMessage("Errore invio messaggio email", 
								"Si è verificato un errore durante l'invio del messaggio di posta elettronica." .
								" Sei pregato di cercare di ripetere l'operazione o di avvertire " .
								"<a href='mailto:" . $this->supportEmail->address . "'>l'assistenza tecnica</a>.");
			
			}
		
		}
	
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
