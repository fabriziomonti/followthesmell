<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->addItem($this->getMenu());
		
		$this->addItem("Segnalazioni", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$sql = "SELECT hit.*," .
				" concat(user.first_name, ' ', user.last_name) as user_name," .
				" if (hit.is_mail_sent, 'si', 'no') as mail_sent," .
				" intensity.name as s_intensity," .
				" duration.name as s_duration," .
				" (select count(*) from receipt where receipt.id_hit=hit.id) as receipt_nr" .
				" FROM hit" .
				" join user on hit.id_user=user.id" .
				" left join intensity on hit.id_intensity=intensity.id" .
				" left join duration on hit.id_duration=duration.id" .
				" where not hit.is_deleted" .
				" order by hit.registered_at desc";

		$table = parent::getTable($sql);
		$table->formPage = "frm_hit.php";
		$table->title = "Segnalazioni";
		
		//----------------------------------------------------------------------
		// azioni...
		if (!$this->isCreatable($table))
			{
			$table->removeAction("New");
			}
		$table->actions["Edit"]->enablingFunction = [$this, "isEditable"];
		$table->actions["Delete"]->enablingFunction = [$this, "isEditable"];
		$table->addAction("Receipts", true, "Ricevute", [$this, "hasReceipts"]);
		$table->addAction("Meteo", true, "Meteo", [$this, "hasMeteo"]);

		//----------------------------------------------------------------------
		// colonne...
		$table->addColumn("id", "ID")->aliasOf = "hit.id";
		$table->addColumn("user_name", "Segnalato da")->aliasOf = "concat(user.first_name, ' ', user.last_name)";
		$ctrl = $table->addColumn("address", "Luogo");
			$ctrl->aliasOf = "hit.address";
			$ctrl->computeFunction = array($this, "getAddressNoCity");
		$table->addColumn("registered_at", "Data/Ora")->aliasOf = "hit.registered_at";
		$table->addColumn("mail_sent", "Email ARPA", true, true, true, \waLibs\waTable::ALIGN_C)->aliasOf = "if (hit.is_mail_sent, 'si', 'no')";
		$table->addColumn("s_intensity", "Intensità")->aliasOf = "intensity.name";
		$table->addColumn("s_duration", "Durata")->aliasOf = "duration.name";
		$table->addColumn("message_notes", "Note ad ARPAE")->aliasOf = "hit.message_notes";
		$table->addColumn("notes", "Note interne")->aliasOf = "hit.notes";
		$col = $table->addColumn("doc", "Documento");
			$col->aliasOf = "hit.doc";
			$col->link = true;
		
		// colonne invisibili
		$table->addColumn("URLdoc", "URLdoc", false, false, false)
			->computeFunction = array($this, "getUrlDoc");

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	/**
	 */
	function isCreatable(waLibs\watable $table)
		{
		return $this->user->is_sys_admin || $this->canUserSendPec($table->recordset->dbConnection);
		}
		
	//*****************************************************************************
	/**
	 */
	function isEditable(waLibs\watable $table)
		{
		return $this->user->is_sys_admin || ($table->record->id_user == $this->user->id && !$table->record->is_mail_sent);
		}
		
	//*****************************************************************************
	function getUrlDoc(waLibs\waTable $table)
		{
		return parent::getUrlDoc($table->record, "doc");
		}
		
	//*****************************************************************************
	function hasReceipts(waLibs\waTable $table)
		{
		return !!$table->record->receipt_nr;
		}
		
	//*****************************************************************************
	function hasMeteo(waLibs\waTable $table)
		{
		return !!$table->record->weather_blob || $this->user->is_sys_admin;
		}
		
	//*****************************************************************************
	function getAddressNoCity(waLibs\waTable $table)
		{
		$ret = $table->record->address;
		$cityBegin = stripos($ret, ", San Pietro In Casale");
		if ($cityBegin !== false) 
			{
			$ret = substr($ret, 0, $cityBegin);
			}
		return $ret;
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();