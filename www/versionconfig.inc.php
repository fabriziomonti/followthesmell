<?php
if (!defined('__VERSIONCONFIG_VARS'))
{
	define('__VERSIONCONFIG_VARS',1);
	
	define('APPL_REL', 							'1.0.26');
	define('APPL_REL_DATE', 					mktime(0,0,0, 12, 6, 2021));

	
} //  if (!defined('__VERSIONCONFIG_VARS'))
