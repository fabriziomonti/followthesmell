<?php

if (!defined('__CONFIG_VARS'))
{
		define('__CONFIG_VARS', 1);
		
		error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

		$__DIR__ = __DIR__;

		// file contenente i veri valori di configurazione, che non devono finire
		// su git, mentre il presente file rimane per documentazione
		@include "$__DIR__/myconfig.inc.php";

		// file contenente i parametri della versione (che cambiano e devono essere
		// inviati al server, a differenza di questi)
		include "$__DIR__/versionconfig.inc.php";

		define('APPL_DOMAIN', $_SERVER['HTTP_HOST']);
		define('APPL_DIRECTORY', '');
		define('APPL_TMP_DIRECTORY', "$__DIR__/../web_files/tmp");
		define('APPL_DOC_DIRECTORY', "$__DIR__/../web_files");
		define('APPL_NAME', 'followthesmell');
		define('APPL_TITLE', 'FollowTheSmell');
		define('APPL_SMTP_SERVER', 					'');
		define('APPL_SMTP_USER', 					'');
		define('APPL_SMTP_PWD', 					'');
		define('APPL_SMTP_SECURE',					'');
		define('APPL_SMTP_PORT',					'');
		define("APPL_SUPPORT_ADDR",					'{"address": "ufficiotecnico@webappls.com", "name" : "FollowTheSmell"}');
        define("APPL_INFO_ADDR",					'{"address": "ufficiotecnico@webappls.com", "name" : "FollowTheSmell"}');
		define("APPL_SUPPORT_TEL",					"051 232260");

		
		define("WAMODULO_EXTENSIONS_DIR", "$__DIR__/wamodulo_ext"); // directory estensioni classe modulo
		define("APPL_NOME_FILE_LOG_DB", "$__DIR__/../web_files/logdb/logdb.csv"); // nome file di logging scritture su db

		// define("APPL_GENERA_WADOC",				true);
		// define per la gestione della tabella help
		// define("APPL_VEDI_MENU_HELP", 			true);
		// define per la gestione del debug
		define("APPL_DEBUG", true);

		set_include_path(get_include_path() . PATH_SEPARATOR . "$__DIR__/../vendor");

		// google maps key
		define("APPL_MAPS_API_URL", "https://maps.googleapis.com/maps/api");
		define("APPL_MAPS_API_SERVER_KEY", "xxxxx");
		define("APPL_MAPS_API_BROWSER_KEY", "yyyyy");

		// formula standard usata per la chiusura dei messaggi
		define('APPL_THANKS_MSG', 	"Grazie per il tuo contributo!");
		
		// encryption security key & initialization vector
		define("APPL_ENCRYPTION_KEY", "");
		define("APPL_ENCRYPTION_IV", "");
		
		// google login
		define("APPL_GOOGLE_LOGIN_CLIENT_ID",			"");	// montienator - followthesmell
		define("APPL_GOOGLE_LOGIN_SECRET",				"");	// montienator - followthesmell
		
		// server pec
		define('APPL_PEC_SMTP_SERVER', 					'');
		define('APPL_PEC_SMTP_USER', 					'');
		define('APPL_PEC_SMTP_PWD', 					'');
		define('APPL_PEC_SMTP_SECURE',					'');
		define('APPL_PEC_SMTP_PORT',					'');
		define("APPL_PEC_SENDER",						'{"address": "fabrizio.monti@messaggipec.it", "name" : "FollowTheSmell PEC"}');

		define('APPL_PEC_POP_SERVER', 					'');
		define('APPL_PEC_POP_USER', 					APPL_PEC_SMTP_USER);
		define('APPL_PEC_POP_PWD',						APPL_PEC_SMTP_PWD);
		
		$pec_from = ["name" => "PEC FollowTheSmell", "address" => "followthesmell@messaggipec.it"];
		$pec_to =	[
						["name" => "PEC ARPAE", "address" => "fabrizio.monti@messaggipec.it"]
					];
		$pec_cc =	[
						["name" => "PEC Comune San Pietro in Casale", "address" => "fabrizio.monti@messaggipec.it"],
						["name" => "PEC Città Metropolitana BO", "address" => "fabrizio.monti@messaggipec.it"],
						["name" => "Unità Operativa Nucleo Ambientale Città Metropolitana BO", "address" => "nucleoambientalepolizia@webappls.com"],
					];
		define("APPL_PEC_FROM", serialize($pec_from));
		define("APPL_PEC_TO",	serialize($pec_to));
		define("APPL_PEC_CC",	serialize($pec_cc));

		define("APPL_PEC_SUBJECT_TEMPLATE",	"Segnalazione odori sgradevoli da stabilimento Agrienergia");
		define("APPL_PEC_MSG_TEMPLATE",		"Gentilissimi,\n\n" .
											"sono un abitante del Comune di San Pietro in Casale.\n\n" .
											"Vorrei segnalare in data [data]  alle ore [ora] la presenza di odori sgradevoli," .
											" come spesso accade, in zona\n\n" .
											"[indirizzo]\n\n" .
											"provenienti dallo Stabilimento di Agrienergia SpA (Via Fontana 1097, San Pietro in Casale).\n\n" .
											"[intensita]\n" .
											"[durata]\n\n" .
											"[note_messaggio]\n\n" .
											"Vi ringrazio dell'attenzione e auguro buon lavoro,");
		define("APPL_PEC_MSG_FOOTER",		"In fede,\n" .
											"[nome] [cognome]\n" .
											"email: [email]\n" .
											"telefono: [telefono]\n" . 
											"-------------------------------\n" .
											"Questo messaggio è stato inviato a mezzo della PEC di [titolo_applicazione]\n" .
											"([indirizzo_applicazione])");
		
		// minimo intervallo tra una hit e la sucsessiva per utente (secondi)
		define("APPL_NEW_HIT_TIME_THRESHOLD",	6*60*60);
		
		// caselle email dati meteo
		define('APPL_METEO_POP_SERVER', 					'{mail.webappls.com:995/novalidate-cert/pop3/ssl}');
		define('APPL_METEO_POP_USER_SEBASTIANO',			"test.datimeteo.sebastiano@webappls.com");
		define('APPL_METEO_POP_PWD_SEBASTIANO',				"");
		define('APPL_METEO_POP_USER_MARCO', 				"test.datimeteo.marco@webappls.com");
		define('APPL_METEO_POP_PWD_MARCO',					"");
		
} //  if (!defined('__CONFIG_VARS'))
