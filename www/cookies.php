<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		if ($this->user) 
			{
			$this->addItem($this->getMenu());
			}
		
		$this->addItem("Cookie policy", "title");
		$this->addItem(file_get_contents("cookies.html"), "cookies_text");
		$this->show();
		}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();