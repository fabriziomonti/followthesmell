<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 * attenzione: non piu usato: si usa help_ajax.php
 */
//******************************************************************************
class page extends followthesmell
	{

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);

		$this->addItem(htmlspecialchars($_GET["section"]), "section");
		$this->addItem(htmlspecialchars($_GET["page"]), "page");
		$this->addItem(htmlspecialchars($_GET["control"]), "control");
		$this->addItem(htmlspecialchars($_GET["title"]), "title");
		$this->addItem(htmlspecialchars($_GET["label"]), "label");
		$this->addItem($this->user->is_sys_admin, "is_sys_admin");
		
		$dbconn = $this->getDBConnection();
		$sql = "select * from viva_help" .
				" where section=" . $dbconn->sqlString($_GET["section"]) .
				" and page=" . $dbconn->sqlString($_GET["page"]) .
				" and control=" . $dbconn->sqlString($_GET["control"] ? $_GET["control"] : "") .
				" and not is_deleted";
		$help_rec = $this->getRecordset($sql, $dbconn, 1)->records[0];
		
		$this->addItem($help_rec->id, "id");
		$this->addItem($help_rec->text, "text");
		
		$this->show();

		}


	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
