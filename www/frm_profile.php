<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Profilo", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		//----------------------------------------------------------------------
		$this->form->addText("last_name", "Cognome", $readOnly, !$readOnly);
		$this->form->addText("first_name", "Nome", $readOnly, !$readOnly);
		$this->form->addEmail("email", "Email", $readOnly, !$readOnly);
		$ctrlP = $this->form->addPassword("new_pwd", "Nuova password (se vuoi cambiarla)", $readOnly);
		$ctrl = $this->form->addPassword("confirm_pwd", "Conferma nuova password", $readOnly);
			$ctrlP->dbBound = $ctrl->dbBound = false;
			$ctrlP->maxChars= $ctrl->maxChars = 12;
		$this->form->addText("phone", "Telefono", $readOnly);
		$this->form_geoBlock($readOnly, !$readOnly);
		$ctrl = $this->form->addUpload("image", "Avatar", $readOnly);
			$this->setUrlDoc($ctrl);
		
		$this->form_submitButtons($this->form, false, false);
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from user" .
				" where id=" . $dbconn->sqlInteger($this->user->id) . 
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if ($this->form->confirm_pwd)
			{
			if ($this->form->new_pwd != $this->form->confirm_pwd)
				{
				$this->showMessage("Password non corrispondenti", "Password non corrispondenti");
				}
			$record->pwd  =  $this->encryptPassword($this->form->new_pwd);
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		$dbconn->beginTransaction();
		// determiniamo id_city e address_blob (occhio: la chiamata va eseguita prima 
		// di ->save(), altrimenti in modulo e riga ci sono gli stessi valori)
		$this->setLocationInfo($record, $this->form->address->address_blob);
		$this->setEditorData($record);
		$this->form->save();
		$this->saveRecordset($record->recordset);

		$this->saveDoc($this->form->inputControls["image"]);
		$dbconn->commitTransaction();

		$this->user = $this->record2Object($record);
		$this->response();
		}
		
	//*****************************************************************************
	function rpc_emailExists($email)
		{
		if (!$email)
			{
			return null;
			}
		$dbconn = $dbconn ? $dbconn : $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$id = $record ? $record->id : 0;
		$sql = "select * from user" .
				" where email=" . $dbconn->sqlString($email) .
				" and id!=" . $dbconn->sqlInteger($this->user->id) .
				" and not is_deleted";
		return $this->getRecordset($sql, $dbconn, 1)->records[0] ? 1 : 0;
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
