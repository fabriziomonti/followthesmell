<?php
//*****************************************************************************
include_once (__DIR__ . "/../followthesmell.inc.php");

//*****************************************************************************
class cron extends followthesmell
	{
	
	//**************************************************************************
	function __construct()
		{
		if (defined("APPL_CRON_LOG"))
			{
			ini_set("error_log", APPL_CRON_LOG . "." . date("Ymd.His") . ".log");
			}
			
		parent::__construct(false);
		}
		
	//***************************************************************************
	/**
	* mostra un messaggio e termina l'esecuzione dello script corrente
	* @param string $title intestazione del messaggio
	* @param string $message testo del messaggio da mostrare
	* @return void
	*/
	function showMessage($title, $message, $exit = true)
		{
		$riga = "\n" .
				"type: application error\n" .
				"script: " . basename($_SERVER["SCRIPT_FILENAME"] ? $_SERVER["SCRIPT_FILENAME"] : $GLOBALS["argv"][1]) . "\n" .
				"time: " . date("H.i.s") . "\n" .
				"title: $title\n" .
				"description: $message\n\n" .
				"\n";

		if (defined("APPL_CRON_LOG"))
			{
			error_log($riga);
			}
		else
			{
			echo $riga;
			}
		
		if ($exit)
			{
			exit();
			}
			
		}
		

	//*****************************************************************************
	}

