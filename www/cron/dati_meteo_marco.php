<?php
//*****************************************************************************
//*****************************************************************************
include_once (__DIR__ . "/dati_meteo.inc.php");

//*****************************************************************************
class page extends dati_meteo
	{
	
	var $fellow = "marco";
	var $headers2skip = 1;
	var $field_separator = ";";
	var $imap_user = APPL_METEO_POP_USER_MARCO;
	var $imap_pwd = APPL_METEO_POP_PWD_MARCO;

	//*****************************************************************************
	function line2rec($line, $rec)
		{

		static $fields = array('date_time','in_temp','temp_out','wind_chill','in_dew','dew_pt','in_heat','heat_index','int_hum','out_hum','hi_speed','wind_speed','wind_dir','bar','rain','et','rain_rate','solar_rad','uvi');

		list($data, $ora) = explode(" ", $line[0]);
		list($y, $m, $d) = explode("-", $data);
		list($h, $i, $s) = explode(":", $ora);
		$rec->date_time = mktime($h * 1, $i * 1, $s * 1, $m * 1, $d * 1, $y * 1);
		for ($i = 1; $i < count($fields); $i++)
			{
			$rec->insertValue($fields[$i], $this->deformat($line[$i]));
			}
			
		}
		
	//*****************************************************************************
	function deformat($in)
		{
		$ret = str_replace(".", "", $in);
		$ret = str_replace(",", ".", $ret);
		return $ret;
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
