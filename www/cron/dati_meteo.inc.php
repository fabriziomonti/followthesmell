<?php
//*****************************************************************************
//*****************************************************************************
include_once (__DIR__ . "/cron.inc.php");

//*****************************************************************************
class dati_meteo extends cron
	{
	
	var $fellow;
	var $headers2skip;
	var $field_separator;
	var $imap_user;
	var $imap_pwd;
	
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$dbconn = $this->getDBConnection();
		
		
		if ($_GET["local"])
			{
			$this->processLocal($dbconn);
			}
		else
			{
			$this->processIMAP($dbconn);
			}
		}
		
	//*****************************************************************************
	function processLocal(\waLibs\waDBConnection $dbconn)
		{
		$files = glob("$this->directoryDoc/dati_meteo/$this->fellow/*.*");
		foreach ($files as $file)
			{
			if (is_file($file)) 
				{
				$this->processFile($dbconn, $file);
				}
			}
		}
		
	//*****************************************************************************
	function processIMAP(\waLibs\waDBConnection $dbconn)
		{
		$mbox = imap_open(APPL_METEO_POP_SERVER, $this->imap_user, $this->imap_pwd);
		
		$status = imap_status($mbox, APPL_METEO_POP_SERVER . "INBOX", SA_MESSAGES);

		for ($msg_idx = $status->messages; $msg_idx > 0; $msg_idx--)
			{
			$attachment = $this->extract_attachment($mbox, $msg_idx);
			if($attachment['is_attachment']) 
				{
				$file = "$this->directoryDoc/dati_meteo/$this->fellow/$attachment[name]";
				file_put_contents($file, $attachment["attachment"]);
				$this->processFile($dbconn, $file);
				}
			imap_delete($mbox, $msg_idx);
			}

		imap_close($mbox, CL_EXPUNGE);
		}
		
	//*****************************************************************************
	function processFile(\waLibs\waDBConnection $dbconn, $file)
		{
		$file_id = date("YmdHis_") . basename($file);
		try 
			{
			$fp = fopen($file, "r");
			for ($i = 0; $i < $this->headers2skip; $i++) 
				{
				$line = fgetcsv($fp, 0, "\t");
				}

			$sql = "select * from meteo";
			$rs = $this->getRecordset($sql, $dbconn, 0);
			$dbconn->beginTransaction();
			while ($line = fgetcsv($fp, 0, $this->field_separator))
				{
				$rec = $rs->add();
				$rec->fellow = $this->fellow;
				$this->line2rec($line, $rec);
				}
			rename($file, "$this->directoryDoc/dati_meteo/$this->fellow/archive/$file_id");
			if ($rs->save(true))
				{
				$dbconn->commitTransaction();
				}
			else {
				$this->mandaMail($file_id, $rs->errorNr() . " - " .$rs->errorMessage());
				}
			}
		catch (exception $e) 
			{
			$this->mandaMail($file_id, $e->getCode() . " - " . $e->getMessage());
			}
		}
		
	//*****************************************************************************
	function mandaMail($file_id, $error_cause)
		{
		$msg = "Errore cron dati meteo $this->fellow file id : $file_id\n\n$error_cause";
		$this->sendMail($this->supportEmail, "Errore cron dati meteo $this->fellow", $msg);
		}
		
	//*****************************************************************************
	function line2rec($line, $rec)
		{
		}
		
	//*****************************************************************************
	function extract_attachment($connection, $message_number) {

		$attachment = array(
			'is_attachment' => false,
			'name' => '',
			'attachment' => ''
		);

		$structure = imap_fetchstructure($connection, $message_number);

		if(isset($structure->parts) && count($structure->parts)) {

			for($i = 0; $i < count($structure->parts); $i++) {

				if($structure->parts[$i]->ifdparameters) {
					foreach($structure->parts[$i]->dparameters as $object) {
						if(strtolower($object->attribute) == 'filename') {
							$attachment['is_attachment'] = true;
							$attachment['name'] = $object->value;
						}
					}
				}

				if($structure->parts[$i]->ifparameters) {
					foreach($structure->parts[$i]->parameters as $object) {
						if(strtolower($object->attribute) == 'name') {
							$attachment['is_attachment'] = true;
							$attachment['name'] = $object->value;
						}
					}
				}

				if($attachment['is_attachment']) {
					$attachment['attachment'] = imap_fetchbody($connection, $message_number, $i+1);
					if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
						$attachment['attachment'] = base64_decode($attachment['attachment']);
					}
					elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
						$attachment['attachment'] = quoted_printable_decode($attachment['attachment']);
					}
					break;
				}

			}

		}

		return $attachment;

	}
	
//*****************************************************************************
	}
		
		
