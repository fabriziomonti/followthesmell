<?php
//*****************************************************************************
//*****************************************************************************
include_once (__DIR__ . "/dati_meteo.inc.php");

//*****************************************************************************
class page extends dati_meteo
	{
	var $fellow = "sebastiano";
	var $headers2skip = 2;
	var $field_separator = "\t";
	var $imap_user = APPL_METEO_POP_USER_SEBASTIANO;
	var $imap_pwd = APPL_METEO_POP_PWD_SEBASTIANO;
	
	//*****************************************************************************
	function line2rec($line, $rec)
		{

		static $fields =  array('date', 'time' ,'temp_out' ,'hi_temp' ,'low_temp' ,'out_hum' ,'dew_pt' ,'wind_speed' ,'wind_dir' ,'wind_run','hi_speed' ,'hi_dir' ,'wind_chill' ,'heat_index','thw_index','bar','rain','rain_rate','heat_d_d','cool_d_d','in_temp','int_hum','in_dew','in_heat','in_emc','in_air_density','wind_samp','wind_tx','iss_receipt','arc_int');

		list($d, $m, $y) = explode("/", $line[0]);
		list($h, $i) = explode(":", $line[1]);
		$rec->date_time = mktime($h * 1, $i * 1, 0, $m * 1, $d * 1, $y + 2000);
		for ($i = 2; $i < count($fields); $i++)
			{
			$rec->insertValue($fields[$i], $line[$i]);
			}
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
