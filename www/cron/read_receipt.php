<?php
//*****************************************************************************
//*****************************************************************************
include_once (__DIR__ . "/cron.inc.php");

//*****************************************************************************
class page extends cron
	{
	
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$dbconn = $this->getDBConnection();
		$mbox = imap_open(APPL_PEC_POP_SERVER, APPL_PEC_POP_USER, APPL_PEC_POP_PWD);
		
		$status = imap_status($mbox, APPL_PEC_POP_SERVER . "INBOX", SA_MESSAGES);

		$already_registered_cntr = 0;
		for ($msg_idx = $status->messages; $msg_idx > 0; $msg_idx--)
			{
			$headers = imap_fetchheader($mbox, $msg_idx, FT_PREFETCHTEXT);
			$message_id = $this->getFromHeaders($headers, "Message-ID");
			if ($message_id)
				{
				// verifichiamo se è già stato registrato
				$sql = "select * from receipt where message_id=" . $dbconn->sqlString($message_id);
				$rsReceipt = $this->getRecordset($sql, $dbconn, 1);
				if ($rsReceipt->records)
					{
					// messaggio già trattato; quando abbiamo letto 10 messaggi 
					// già trattati, interrompiamo il ciclo. Perchè dopo 10?
					// Perchè i messaggi non sono perfettamente in ordine cronologico,
					// quindi potremmo interrompere il ciclo prima di avere 
					// effettivamente trattato tutti i messaggi d trattare
					$already_registered_cntr++;
					if ($already_registered_cntr == 10)
						{
						break;
						}
					else 
						{
						continue;
						}
					}
					
				// pare da registrare, vediamo se troviamo la segnalazione
				$hit_message_id = $this->getFromHeaders($headers, "X-Riferimento-Message-ID");
				// $hit_message_id potrebbe essere il riferimento del nostro 
				// messaggio che si trova in accettazione e consegna
				$sql = "select id from hit where message_id=" . $dbconn->sqlString($hit_message_id);
				$hit = $this->getRecordset($sql, $dbconn, 1)->records[0];
				if (!$hit)
					{
					// proviamo a cercare nel body se troviamo il nostro pattern (protocollo)
					$ftsId = $this->getFTSid($mbox, $msg_idx);
					list($id_user, $registerd_at) = explode(".", $ftsId);
					$sql = "select id from hit where id_user=" . $dbconn->sqlInteger($id_user) .
								" and registered_at=" . $dbconn->sqlDateTime($registerd_at);
					$hit = $this->getRecordset($sql, $dbconn, 1)->records[0];
					}
				if ($hit)
					{
					$receipt = $rsReceipt->add();
					$receipt->id_hit = $hit->id;
					$receipt->message_id = $message_id;
					$receipt->doc = "message.eml";
					$receipt->sender = $this->getFromHeaders($headers, "From");
					$receipt->subject = $this->getFromHeaders($headers, "Subject");
					$receipt->registered_at = strtotime($this->getFromHeaders($headers, "Date"));
					$this->setEditorData($receipt);
					$this->saveRecordset($rsReceipt);
					// salvataggio eml
					$this->saveDocFromBuffer($receipt, "doc", "$headers\n" . imap_body($mbox, $msg_idx));
					// facciamo ripartire il contatore di quelli già registrati
					$already_registered_cntr = 0;
					}
				}
			
			}
		
		imap_close($mbox);
		}
	
	//*****************************************************************************
	function getFromHeaders(&$headers, $key)
		{
		list($before, $ret) = explode("\n$key: ", $headers, 2);
		list($ret, $after) = explode("\n", $ret, 2);
		return trim($ret);
		}
		
	//*****************************************************************************
	function getFTSid($mbox, $msg_idx)
		{
		$body = imap_body($mbox, $msg_idx);
		list($before, $ret) = explode("[_FTS_.", $body, 2);
		list($ret, $after) = explode("]", $ret, 2);
		return trim($ret);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
