<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		if (!$this->user->is_sys_admin) 
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, true);
			}
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			$this->deleteRecord($this->form->record);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Ricevuta", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		//----------------------------------------------------------------------
		$ctrl = $this->form->addUpload("doc", "EML", $readOnly, !$readOnly);
			$this->setUrlDoc($ctrl);
		$this->form->addTextArea("notes", "Note", $readOnly);
		
		$this->form_submitButtons($this->form, $readOnly, !!$record);
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from receipt" .
				" where id=" . $dbconn->sqlInteger($_GET["id"]) . 
				" and id_hit=" . $dbconn->sqlInteger($_GET["id_hit"]) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if ($_GET["id"] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->id_hit = $_GET["id_hit"];
			}
		else 
			{
			$this->checkLockViolation($this->form);
			}
			
		$dbconn->beginTransaction();
		
		// determiniamo dal documento i campi che ci servono
		$headers = file_get_contents($this->form->inputControls["doc"]->getTmpValue());
		$record->doc = "message.eml";
		$record->notes= $this->form->notes;
		$record->message_id = $this->getFromHeaders($headers, "Message-ID");
		$record->sender = $this->getFromHeaders($headers, "From");
		$record->subject = $this->getFromHeaders($headers, "Subject");
		$record->registered_at = strtotime($this->getFromHeaders($headers, "Date"));
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		$this->saveDoc($this->form->inputControls["doc"]);

		$dbconn->commitTransaction();
		$this->response();
		}
		
	//*****************************************************************************
	function getFromHeaders(&$headers, $key)
		{
		list($before, $ret) = explode("\n$key: ", $headers, 2);
		list($ret, $after) = explode("\n", $ret, 2);
		return trim($ret);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
