<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		if (!$stepsData || !$stepsData->ready)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, false);
			}
		
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->addItem("Conferma invio segnalazione a ARPAE via PEC", "title");
		$this->addItem("Stai per inviare questo messaggio", "hit_wiz_email_message_hdr");

		$this->form = $this->getForm();
		
		// creiamo un finto hit per costruire il body
		$hit = $this->getRecordset("select * from hit", $dbconn, 0)->add();
		$hit->id_user = $this->user->id;
		$hit->address = $this->user->address;
		$hit->registered_at = mktime();
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		$hit->message_notes = $stepsData->message_notes;
		$hit->id_intensity = $stepsData->id_intensity;
		$hit->id_duration = $stepsData->id_duration;
		$this->addItem($this->getPecPreview($hit), "hit_wiz_email_message");
		$this->addItem("Confermi invio?", "hit_wiz_email_message_ftr");

		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Invia");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		$dbconn = $this->getDBConnection();
		$dbconn->beginTransaction();
		
		// creiamo la hit
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		$hit = $this->getRecordset("select * from hit", $dbconn, 0)->add();
		$hit->id_user = $this->user->id;
		$hit->address = $this->user->address;
		$hit->address_blob = $this->user->address_blob;
		$hit->latitude = $this->user->latitude;
		$hit->longitude = $this->user->longitude;
		$hit->id_city = $this->user->id_city;
		$hit->registered_at = mktime();
		$hit->message_notes = $stepsData->message_notes;
		$hit->id_intensity = $stepsData->id_intensity;
		$hit->id_duration = $stepsData->id_duration;
		$hit->is_mail_sent = 1;
		$this->setEditorData($hit);

		$hit->message_id = $this->sendPec($hit);
		$this->setHitWeather($hit);
		$this->saveRecordset($hit->recordset);
		$dbconn->commitTransaction();
		
		if (!$stepsData->user_was_logged)
			{
			// l'utente non era loggato all'inizio del wizard: lo slogghiamo 
			// sennò entra nell'applicazione
			$this->logAccess($dbconn, 1);
			$this->sessionData = array();
			}
		else
			{
			// in ogni caso resettiamo gli stepsdata del wizard
			unset($this->sessionData["hit_wiz_steps_data"]);
			}
		$this->showMessage("Messaggio inviato correttamente", "Il messaggio è stato inviato correttamente", false, true);
		}

	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
