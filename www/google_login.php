<?php
include "followthesmell.inc.php";
include_once "google-api-php-client-2.2.4/vendor/autoload.php";

//*****************************************************************************
class page extends followthesmell
	{
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		if ($this->user) 
			{
			$this->redirect($this->startPage);
			}

		// creazione del google_client
		$google_client = new Google_Client();
		$google_client->setClientId(APPL_GOOGLE_LOGIN_CLIENT_ID);
		$google_client->setClientSecret(APPL_GOOGLE_LOGIN_SECRET);
		$protocol = $this->getProtocol();
		$redirect = $_GET["r"] ? "?r=$_GET[r]" : "";
		$google_client->setRedirectUri("$protocol://$this->domain$this->httpwd/google_login.php$redirect");
		$google_client->addScope("email");
		$google_client->addScope("profile");
		if ($_GET["code"])
			{
			$this->doLogin($google_client);
			}
		else
			{
			$this->redirect($google_client->createAuthUrl());
			}
			
		}
			
	//***************************************************************************
	function doLogin($google_client)
		{
		$token = $google_client->fetchAccessTokenWithAuthCode($_GET['code']);
		$google_client->setAccessToken($token['access_token']);

		// get profile info
		$google_oauth = new Google_Service_Oauth2($google_client);
		$google_account_info = $google_oauth->userinfo->get();
		if (!$google_account_info->verifiedEmail)
			{
			$this->showMessage("Login non permesso", "Login non permesso");
			}

		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from user" .
				" where email=" . $dbconn->sqlString($google_account_info->email) .
				" and not is_deleted";
		$rs = $this->getRecordset($sql, $dbconn, 1);
		if (!$rs->records)
			{
			// lo dobbiamo creare
			$record = $rs->add();
			$record->email = $google_account_info->email;
			$record->first_name = $google_account_info->givenName;
			$record->last_name = $google_account_info->familyName;
			$record->pwd = $this->encryptPassword($this->getPassword());
			// salvataggio immagine
			$pic = "";
			if ($google_account_info->picture) 
				{
				$pic = $this->url_get_contents($google_account_info->picture);
				if (pic)
					{
					$record->image = "pic.jpg";
					}
				}
			$this->setEditorData($record);
			$this->saveRecordset($record->recordset);
			if ($pic) 
				{
				$this->saveDocFromBuffer($record, "image", $pic);
				}
			}
		else 
			{
			$record = $rs->records[0];
			}

		$this->user = $this->record2Object($record);
		$this->logAccess($record->recordset->dbConnection);
		$redirect = $_GET["r"] ? base64_decode($_GET["r"]) : $this->startPage;
		$this->redirect($redirect);
		}
			    
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new page();
