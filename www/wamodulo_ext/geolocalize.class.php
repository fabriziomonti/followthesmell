<?php
namespace waLibs;

//***************************************************************************
//****  classe waTextArea_ext *******************************************************
//***************************************************************************
/**
* waGeolocalize
*
* classe per la gestione di un controllo geolocalize.
*
*/
class waGeolocalize extends waControl
	{
	public $address;
	public $address_blob;
	
	/**
	* @ignore
	* @access protected
	*/
	var $type			= 'geolocalize';
	
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	*/
	function get()
		{
		
		if ($this->form->record || !$this->value) 
			{
			$value = new \stdClass();
			$value->address = $this->form->record->address;
			$value->address_blob = $this->form->record->address_blob;
			$this->value = json_encode($value);
			}
		return parent::get();
		}
		
	//***************************************************************************
	/**
	 * salva sul campo del record il valore di input
	* @ignore
	*/	
	function input2record()
		{
		if (!$this->dbBound || !$this->form->record || $this->inputValue === null)
			{
			return;
			}
		$this->form->record->insertValue("address", $this->inputValue->address);
		$this->form->record->insertValue("address_blob", $this->inputValue->address_blob);
		}
	
	
	}	// fine classe waGeolocalize
//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************



