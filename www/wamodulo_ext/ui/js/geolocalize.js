//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe watextarea_ext
var waGeolocalize = new Class({

	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	tipo: 'geolocalize',

	address_blob: null,
	street: null,
	house_number: null,
	postal_code: null,
	city: null,
	province_short: null,
	notes: null,

	components: null,

	//-------------------------------------------------------------------------
	initialize: function (form, name, value, visible, readOnly, mandatory) {
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);

		this.selector = '#' + this.form.name + '_' + this.name + "_geolocalize_container";

		this.address_blob = this.form.obj.elements[this.name + "_address_blob"];
		
		// in questa versione i componeneti di fatto non sono gestiti; ne resta 
		// la definizione perchè troppa fatica cavarli e perchè sono comunque 
		// utili per il controllo del civico
		this.street = this.form.obj.elements[this.name + "_street"];
		this.house_number = this.form.obj.elements[this.name + "_house_number"];
		this.postal_code = this.form.obj.elements[this.name + "_postal_code"];
		this.city = this.form.obj.elements[this.name + "_city"];
		this.province_short = this.form.obj.elements[this.name + "_province_short"];
		this.notes = this.form.obj.elements[this.name + "_notes"];

		if (this.address_blob.value) {
			try {
				this.normalizeAddress();
			} catch (e) {
			}
		}

		var options = {types: ['geocode', 'establishment']};
		var autocomplete = new google.maps.places.Autocomplete(this.obj, options);

		// evento per cambio dell'autocomplete di google
		var proxy = this;
		google.maps.event.addListener(
				autocomplete,
				'place_changed',
				function () {
					proxy.placeChanged(autocomplete.getPlace());
				}
		);

		// mettiamo un namespace all'evento jquery, si sa mai che vada a inzuccarsi con quello google
		jQuery(this.obj).on("keyup." + this.obj.name, function (event) {proxy.addressKeyUp(event)});

		try {
			// mutation observer per eliminare l'autocompletion
			var observer = new MutationObserver(proxy.inputAttributesMutation);
			observer.observe(this.obj, {attributes: true});
		} catch (e) {
		}

	},

	//-------------------------------------------------------------------------
	inputAttributesMutation: function (mutationList, observer) {

		// elimina il maledetto autocompletion di google
		for (var i = 0; i < mutationList.length; i++) {
			if (mutationList[i].attributeName.toLowerCase() === "autocomplete") {
				jQuery("#" + mutationList[i].target.id).prop("autocomplete", "uzza");
				observer.disconnect();
				break;
			}
		}

	},

	//-------------------------------------------------------------------------
	addressKeyUp: function (event) {

		// stanno digitando l'indirizzo: reset fino a placeChanged
		if (event.keyCode === 9 || event.keyCode === 13) {
			return;
		}
		this.reset();

	},

	//-------------------------------------------------------------------------
	placeChanged: function (place) {
		// per default via civico e note sono disabilitati
		this.street.disabled = this.house_number.disabled = this.notes.disabled = true;

		if (!place)
			return;

		this.address_blob.value = JSON.stringify(place);
		this.blob2ctrls();

		// via civico e note sono abilitati solo se iol controllo è stato 
		// valorizzato
		this.street.disabled = this.house_number.disabled = this.notes.disabled = !this.obj.value;

		if (this.obj.value && !this.components.house_number) {
			this.form.alert("Attenzione: non hai indicato la via o il nr. civico.\n\n" +
					" Se il tuo indirizzo non prevede il numero civico, ovviamente non indicarlo e prosegui la registrazione.\n\n" +
					" Se invece sono previsti, ti invitiamo a indicarli correttamente.");
		}

	},

	//-------------------------------------------------------------------------
	/**
	 * inserisce un valore applicativo nel controllo
	 * 
	 * @param {mixed} value valore da inserire nel controllo
	 * @memberof waControl
	 */
	set: function (value) {
		if (value && value.address) {
			this.obj.value = value.address;
			this.address_blob.value = value.address_blob;
		}
		else {
			this.obj.value = "";
			this.address_blob.value = "";
		}
		this.blob2ctrls();
		// via civico e note sono abilitati solo se iol controllo è stato 
		// valorizzato
		this.street.disabled = this.house_number.disabled = this.notes.disabled = !this.obj.value;
	},

	//***************************************************************************
	/** 
	 * dati componenti dell'indirizzo tornati da googlemaps ritorna una
	 * struttura dati normalizzata (si spera)
	 * 
	 * @param array $address_components
	 * @return stdClass
	 */

	normalizeAddress: function () {
		this.components = {};

		var pass = JSON.parse(this.address_blob.value).address_components;


		for (var i = 0; i < pass.length; i++) {
			for (var li = 0; li < pass[i].types.length; li++) {
				if (pass[i].types[li] == "street_number") {
					this.components.house_number = pass[i].long_name;
					break;
				}
				if (pass[i].types[li] == "route") {
					this.components.street = pass[i].long_name;
					break;
				}
				if (pass[i].types[li] == "locality") {
					this.components.locality = pass[i].long_name;
					break;
				}
				if (pass[i].types[li] == "postal_town") {
					this.components.postal_town = pass[i].long_name;
					break;
				}
				if (pass[i].types[li] == "administrative_area_level_3" && !this.components.city) {
					this.components.city = pass[i].long_name;
					break;
				}
				if (pass[i].types[li] == "administrative_area_level_2") {
					this.components.province = pass[i].long_name;
					this.components.province_short = pass[i].short_name;
					break;
				}
				if (pass[i].types[li] == "administrative_area_level_1") {
					this.components.region = pass[i].long_name;
					this.components.region_short = pass[i].short_name;
					break;
				}
				if (pass[i].types[li] == "country") {
					this.components.country = pass[i].long_name;
					this.components.country_short = pass[i].short_name;
					break;
				}
				if (pass[i].types[li] == "postal_code") {
					this.components.postal_code = pass[i].long_name;
					break;
				}
			}
		}


		// tenatitvi di determinare city (cambia a seconda della nazione)
		if (!this.components.city) {
			this.components.city = this.components.postal_town;
		}
		if (!this.components.city) {
			this.components.city = this.components.locality;
		}
		if (!this.components.city) {
			this.components.city = this.components.province;
		}

		// certe città straniere non hanno l'equivalente della provincia
		if (!this.components.province) {
			this.components.province = "--";
			this.components.province_short = "--";
		}

	},

	//-------------------------------------------------------------------------
	/**
	 */
	blob2ctrls: function () {
		if (this.address_blob.value) {
			try {
				this.normalizeAddress();
				this.street.value = this.components.street ? this.components.street : "";
				this.house_number.value = this.components.house_number ? this.components.house_number : "";
				this.postal_code.value = this.components.postal_code ? this.components.postal_code : "";
				this.city.value = this.components.city ? this.components.city : "";
				this.province_short.value = this.components.province_short ? this.components.province_short : "";
				this.notes.value = this.components.notes ? this.components.notes : "";
			} catch (e) {
				this.reset();
			}
		} else {
			this.reset();
		}
	},

	//-------------------------------------------------------------------------
	/**
	 */
	reset: function () {
		this.components = null;
		this.address_blob.value = "";
		this.street.value = "";
		this.house_number.value = "";
		this.postal_code.value = "";
		this.city.value = "";
		this.province_short.value = "";
		this.notes.value = "";

		// per default via civico e note sono disabilitati
		this.street.disabled = this.house_number.disabled = this.notes.disabled = true;
	},

	//-------------------------------------------------------------------------
	/**
	 * restituisce il  valore applicativo contenuto nel controllo
	 * @memberof waControl
	 */
	get: function () {
		if (!this.obj.value || !this.address_blob.value)
			return null;

		return {address: this.obj.value, address_blob: this.address_blob.value};
	},

	//-------------------------------------------------------------------------
	/**
	 * a seconda dello stato /definisce la classe css di un controllo (visible, 
	 * readOnly, mandatory) 
	 * @memberof waControl
	 */
	render: function () {
		this.parent();
		this.street.disabled = this.house_number.disabled = this.notes.disabled = this.readOnly;
	},

	//-------------------------------------------------------------------------
	/**
	 * verifica che un controllo sia valorizzato correttamente
	 * @return (boolean)
	 * @memberof waControl
	 */
	formatVerify: function () {
		if (this.obj.value && !this.address_blob.value) {
			return false;
		}

		if (!this.obj.value && this.address_blob.value) {
			this.reset();
		}

		return true;
	},

	//-------------------------------------------------------------------------
	/**
	 * verifica se un controllo fisico appartiene al controllo applicativo
	 * 
	 * @param {object} obj oggetto input HTML 
	 * @return (boolean)
	 * @memberof waControl
	 */
	isMine: function (obj) {
		return obj.name == this.obj.name ||
				obj.name == this.street.name ||
				obj.name == this.house_number.name ||
				obj.name == this.postal_code.name ||
				obj.name == this.city.name ||
				obj.name == this.province_short.name ||
				obj.name == this.notes.name ||
				obj.name == this.address_blob.name;

	}

});



