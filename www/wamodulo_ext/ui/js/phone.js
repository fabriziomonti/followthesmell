//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe waPhone: input HTML destinato a contenere un numero telefonico
* 
* @class waInteger
* @extends waControl
*/

var waPhone = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	type: 'phone',
	
	// occhio: mancano get/set
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		var proxy = this;
		jQuery("#" + this.form.name + "_" + this.name + "_number").on("keyup", function (event) {proxy.onKeyUp(event);});
		
		},
		
	//-------------------------------------------------------------------------
	get: function() 
		{
		return (jQuery("#" + this.form.name + "_" + this.name + "_prefix").val() + 
				" " +
				jQuery("#" + this.form.name + "_" + this.name + "_number").val()).trim();
		},
		
	//-------------------------------------------------------------------------
	set: function(value) 
		{
		var elems = value.split(" ");
		if (elems.length >= 2)
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").val(elems[0]);
			jQuery("#" + this.form.name + "_" + this.name + "_number").val(elems[1]);
			}
		},
		
	//-------------------------------------------------------------------------
	onKeyUp: function(event) 
		{
		var numberCtrl = jQuery("#" + this.form.name + "_" + this.name + "_number");
		var re = /^[0-9]*$/;
		if (!re.test(numberCtrl.val())) 
			numberCtrl.val(numberCtrl.val().replace(/[^0-9]/g,""));	
		},
		
	//-------------------------------------------------------------------------
	mandatoryVerify: function() 
		{
		if (!this.mandatory)
			return true;
		return jQuery("#" + this.form.name + "_" + this.name + "_number").val().trim() != '';
		},
		
	//-------------------------------------------------------------------------
	formatVerify: function() 
		{
		var numberCtrl = jQuery("#" + this.form.name + "_" + this.name + "_number");
		var re = /^[0-9]*$/;
		return re.test(numberCtrl.val())
		},
		
	//-------------------------------------------------------------------------
	/**
	* a seconda dello stato /definisce la classe css di un controllo(visible, 
	* readOnly, mandatory) renderizza il controllo
	 * @ignore
	*/
	render: function() 
		{
		if (this.visible)
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").show();
			jQuery("#" + this.form.name + "_" + this.name + "_number").show();
			}
		else
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").hide();
			jQuery("#" + this.form.name + "_" + this.name + "_number").hide();
			}
		if (this.readOnly)
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").addClass("disabled").prop("disabled", true);
			jQuery("#" + this.form.name + "_" + this.name + "_number").addClass("disabled").prop("disabled", true);
			}
		else
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").removeClass("disabled").prop("disabled", false);
			jQuery("#" + this.form.name + "_" + this.name + "_number").removeClass("disabled").prop("disabled", false);
			}
		if (this.mandatory)
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").addClass("required");
			jQuery("#" + this.form.name + "_" + this.name + "_number").addClass("required");
			}
		else
			{
			jQuery("#" + this.form.name + "_" + this.name + "_prefix").removeClass("required");
			jQuery("#" + this.form.name + "_" + this.name + "_number").removeClass("required");
			}
		
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica se un controllo fisico appartiene al controllo applicativo
	* 
	* @param {object} obj oggetto input HTML 
	 * @return (boolean)
	 * @memberof waControl
	 */
	isMine: function (obj)
		{
		return obj.name == this.form.name + "_" + this.name + "_prefix" ||
				obj.name == this.form.name + "_" + this.name + "_number";

		}
		
	}
);
