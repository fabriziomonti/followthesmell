<?php
namespace waLibs;

//***************************************************************************
//****  classe waPhone ******************************************************
//***************************************************************************
/**
* waPhone
*
* classe per la gestione di un controllo phone.
*
*/
class waPhone extends waControl
	{
	public $prefix;
	public $number;
	public $prefixList;
	public $prefixListSql;
	
	/**
	* @ignore
	* @access protected
	*/
	var $type			= 'phone';
	
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	*/
	function get()
		{
		if ($this->prefixListSql)
			{
			$this->prefixList = $this->BuildListFromDBQuery();
			}
		
		$value = new \stdClass();
		$value->prefix = "";
		$value->number = "";
		$record = $this->form->recordset->records[0];
		if ($record)
			{
			$passo = explode(" ", $record->value($this->name));
			$blank = "";
			for ($i = 0; $i < count($passo) - 1; $i++)
				{
				$value->prefix .= $blank . $passo[$i];
				$blank = " ";
				}
			$value->number = $passo[count($passo) - 1];
			}
		$value->prefix_list = $this->prefixList;
		
		$this->value = json_encode($value);
		return parent::get();
		}
		
	//***************************************************************************
	/**
	* @access protected
	* @ignore
	*/
	function BuildListFromDBQuery()
		{

		// se mi viene passata una query, costruisco la lista sulla
		// base della query

		if (empty($this->form->recordset))
		// se l'applicazione non ha messo in bind un recordset alla form, non
		// abbiamo le informazioni per connetterci al db
			{
			return (array());
			}

		$rs = new waRecordset($this->form->recordset->dbConnection);
		$righe = $rs->read($this->prefixListSql);
		if ($rs->errorNr())
			{
			return (array());
			}

		$list = array();
		foreach ($righe as $riga)
			{
			// il primo campo dee essere sempre il prefisso (chiave)
			// il secondo il nome della nzione
			$key = $riga->value(0);
			for ($i = 1; $i < ($rs->fieldNr() - 1); $i++)
				{
				$key .= "|" . $riga->value($i);
				}
			$list[$riga->value(0)] = $riga->value(1);
			}

		return $list;
		}
		
	//***************************************************************************
	/**
	 * converte il valore proveniente dal post nel valore logico del controllo
	* @ignore
	*/	
	function input2inputValue($inValue)
		{
		return $this->inputValue = $inValue->prefix . " " . $inValue->number;
		}
	
	}	// fine classe waPhone
//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************



