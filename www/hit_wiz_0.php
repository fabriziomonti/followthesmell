<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		if ($this->user)
			{
			// ha già un'utenza, inutile riconoscerlo...
			$stepsData = new stdClass();
			$stepsData->email = $this->user->email;
			$stepsData->control_code_verified = true;
			$stepsData->user_was_logged = true;
			$this->sessionData["hit_wiz_steps_data"] = $stepsData;
			$this->redirect("hit_wiz_3.php");
			}

		$this->createForm();
		$this->showPage();
		}

	//*****************************************************************************
	/**
	* showPage
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC", "title");
		$this->addItem($this->form);
		$this->show();
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;
		
		$this->form->addTextArea("normativa", "Informativa privacy", true)
			->value = $this->getPrivacyText();
		$this->form->addBoolean("is_privacy_read", "Ho letto l'informativa sulla privacy", $readOnly, true);
		$this->form->addTextArea("comportamento", "Norme di comportamento", true)
			->value = $this->getBehaviourText();
		$this->form->addBoolean("is_behavoiur_read", "Ho letto le norme di comportamento", $readOnly, true);
		$ctrl = new waLibs\waText($this->form, "g_redirect");
			$ctrl->value = base64_encode("hit_wiz_3.php");
			$ctrl->visible = false;
		$this->form->addButton("cmd_google", "Ho un indirizzo Gmail...")->submit = false;
		$this->form->addButton("cmd_no_google", "Non ho un indirizzo Gmail...")->submit = false;
		
		$this->form->getInputValues();
		}
	
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new page();
