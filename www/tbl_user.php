<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->addItem($this->getMenu());
		
		$this->addItem("Utenti", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$sql = "SELECT distinct user.*," .
				" 'http://www.repubblica.it' as URLfoto," .
				" city.name as user_city_name," .
				" province.short_name as user_province_short_name," .
				" if(user.is_sys_admin, 'sì', 'no') as s_is_sys_admin," .
				" if(user.is_admin, 'sì', 'no') as s_is_admin" .
				($this->user->is_sys_admin ?
				", (select count(*) from hit where hit.id_user=user.id and not hit.is_deleted) as hit_nr" .
				", (select count(*) from access where access.id_user=user.id and not access.is_deleted and not access.flag_out) as access_nr" :
				""
				) .
				" FROM user" .
				
				" left join city on user.id_city=city.id" .
				" left join province on city.id_province=province.id" .
				" where not user.is_deleted" .
				" order by user.last_name, user.first_name";

		$table = parent::getTable($sql);
		$table->formPage = "frm_user.php";
		
		//----------------------------------------------------------------------
		// azioni...
		if (!$this->user->is_sys_admin) 
			{
			$table->removeAction("New");
			}
		$table->actions["Edit"]->enablingFunction = [$this, "isEditable"];
		$table->actions["Delete"]->enablingFunction = [$this, "isEditable"];
		if ($this->user->is_sys_admin) 
			{
			$table->addAction("Accessi", true);
			}

		//----------------------------------------------------------------------
		// colonne...
		$table->addColumn("id", "ID")->aliasOf = "user.id";
		$table->addColumn("last_name", "Cognome")->aliasOf = "user.last_name";
		$table->addColumn("first_name", "Nome")->aliasOf = "user.first_name";
		$col = $table->addColumn("email", "E-mail");
			$col->aliasOf = "user.email";
			$col->link = true;
		$table->addColumn("phone", "Telefono")->aliasOf = "user.phone";
		$table->addColumn("address", "Indirizzo")->aliasOf = "user.address";
		$table->addColumn("user_city_name", "Città")->aliasOf = "city.name";
		$table->addColumn("user_province_short_name", "Prov.", true, true, true, \waLibs\waTable::ALIGN_C)->aliasOf = "province.short_name";
		$col = $table->addColumn("image", "Immagine", true, false, false, \waLibs\waTable::ALIGN_C)->link = true;
		if ($this->user->is_sys_admin) 
			{
			$table->addColumn("hit_nr", "Nr. segnalazioni")->aliasOf = "(select count(*) from hit where hit.id_user=user.id and not hit.is_deleted)";
			$table->addColumn("access_nr", "Nr. accessi")->aliasOf = "(select count(*) from access where access.id_user=user.id and not access.is_deleted and not access.flag_out)";
			$table->addColumn("registered_at", "Data registrazione")->aliasOf = "user.registered_at";
			}
		
		
		// colonne invisibili
		$table->addColumn("URLfoto", "URLfoto", false, false, false)
			->computeFunction = array($this, "getUrlfoto");

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	/**
	 * si può editare solo un utente di cui si è admin (non portavoce)
	 */
	function isEditable(waLibs\watable $table)
		{
		return $this->user->is_sys_admin;
		}
		
	//*****************************************************************************
	function getUrlfoto(waLibs\waTable $table)
		{
		return parent::getUrlDoc($table->record, "image");
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();