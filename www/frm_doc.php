<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			$this->deleteRecord($this->form->record);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Documento", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		//----------------------------------------------------------------------
		$this->form->addText("title", "Titolo", $readOnly, !$readOnly);
		$this->form->addTextArea("abstract", "Descrizione", $readOnly);
		$ctrl = $this->form->addUpload("doc", "Documento", $readOnly);
			$this->setUrlDoc($ctrl);
		$this->form->addTextArea("link", "Link", $readOnly);
		$this->form->addTextArea("notes", "Note", $readOnly);
		
		$this->form_submitButtons($this->form, $readOnly, $record ? true : false);
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from doc" .
				" where id=" . $dbconn->sqlInteger($_GET["id"]) . 
				($this->user->is_sys_admin ? "" : " and id_user=" . $dbconn->sqlInteger($this->user->id)) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if ($_GET["id"] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->id_user = $this->user->id;
			$record->registered_at = mktime();
			}
		else 
			{
			$this->checkLockViolation($this->form);
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		$dbconn->beginTransaction();
		$this->setEditorData($record);
		$this->form->save();
		$this->saveRecordset($record->recordset);

		$this->saveDoc($this->form->inputControls["doc"]);
		$dbconn->commitTransaction();

		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
