<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		// se siamo arrivati fin qui, l'utente deve essere logged-in
		parent::__construct(true);
		
		// verifica threshold
		$this->checkThreshold();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC - 3", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;
		
		$this->form->addText("last_name", "Cognome", $readOnly, !$readOnly)->value = $this->user->last_name;
		$this->form->addText("first_name", "Nome", $readOnly, !$readOnly)->value = $this->user->first_name;
		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Avanti >>");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
			
		$stepsData->last_name = $this->form->last_name;
		$stepsData->first_name = $this->form->first_name;
		// andiamo allo step successivo
		$this->redirect("hit_wiz_4.php");
		}

	//***************************************************************************
	function checkThreshold()
		{
		if (!$this->canUserSendPec())
			{
			// se l'utente non era loggato all'inizio della procedura lo slogghiamo,
			// altrimenti si ritrova nella procedura e non capisce perchè
			if (!$this->sessionData["hit_wiz_steps_data"]->user_was_logged)
				{
				$this->logAccess(null, 1);
				$this->sessionData = array();
				}
			else
				{
				unset($this->sessionData["hit_wiz_steps_data"]);
				}
			$ore = intval(APPL_NEW_HIT_TIME_THRESHOLD / (60*60));
			$min = intval((APPL_NEW_HIT_TIME_THRESHOLD % (60*60)) / 60);
			$msg = "Attenzione\n\nnon è ancora trascorso il periodo di attesa" .
					" di $ore,$min ore dalla tua ultima segnalazione: non è" .
					" possibile inviare il messaggio PEC in questo momento.";
			$this->showMessage("Tempo di attesa non ancora trascorso", $msg, false, true);
			}
		}

	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
