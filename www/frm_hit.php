<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		if (!$this->user->first_name || !$this->user->last_name)
			{
			$this->showMessage("Mancano dati", "Mancano dati al tuo profilo: non puoi procedere a registrare segnalazioni." .
								" Completa i tuoi dati nella pagina <i>Strumenti->Profilo</i>", false, true);
			}
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			if (!$this->isEditable($this->form->record))
				{
				$this->showMessage("Impossibile procedere", "Impossibile modificare una registrazione dopo che è stata inviata la segnalazione ad ARPA", false, true);
				}
			$this->deleteRecord($this->form->record);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Segnalazione", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		
		// il record è editabile se
		// - siamo in inserimento
		// - oppure utente è admin
		// - oppure utente è proprietario e non è mai stata mandata pec segnalazione
		$readOnly = !$this->isEditable($record);
		
		//----------------------------------------------------------------------
		$this->form->addDateTime("registered_at", "Data e ora", $readOnly, !$readOnly)->value = mktime();

		// prevalorizziamo con l'indirizzo utente
		$value = new \stdClass();
		$value->address = $this->user->address;
		$value->address_blob = $this->user->address_blob;
		$this->form_geoBlock($readOnly, !$readOnly)->value = json_encode($value);
		
		$this->form->addOption("id_intensity", "Intensità", $readOnly, !$readOnly)->sql = "select id, description from intensity where not is_deleted order by ordinal";
		$this->form->addOption("id_duration", "Durata", $readOnly, !$readOnly)->sql = "select id, description from duration where not is_deleted order by ordinal";
		$this->form->addTextArea("message_notes", "Note da aggiungere al messaggio", $readOnly);
		
		$ctrl = $this->form->addUpload("doc", "Documento/Immagine", $readOnly);
			$this->setUrlDoc($ctrl);
		$this->form->addTextArea("notes", "Note interne", $readOnly);
		
		$this->form->addBoolean("is_mail_sent", "Spedisci mail", $readOnly)->value = 1;
		
		$this->form_submitButtons($this->form, $readOnly, $record && !$readOnly);
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from hit" .
				" where id=" . $dbconn->sqlInteger($_GET["id"]) . 
				($this->user->is_sys_admin ? "" : " and id_user=" . $dbconn->sqlInteger($this->user->id)) .
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if ($_GET["id"] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}
		if (!$this->isEditable($recordset->records[0]))
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		if (!$this->isEditable($this->form->record))
			{
			$this->showMessage("Impossibile procedere", "Impossibile modificare una registrazione dopo che è stata inviata la segnalazione ad ARPA", false, true);
			}
		
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		$send_mail = true;
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->id_user = $this->user->id;
			}
		else 
			{
			$this->checkLockViolation($this->form);
			// in modifica non mandiamo mai la pec, in nessun caso
			$send_mail = false;
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		$dbconn->beginTransaction();
		// determiniamo id_city e address_blob (occhio: la chiamata va eseguita prima 
		// di ->save(), altrimenti in modulo e riga ci sono gli stessi valori)
		$this->setLocationInfo($record, $this->form->address->address_blob);
		$this->form->save();
		$this->setEditorData($record);
		
		// se l'utente non aveva ancora un indirizzo glielo aggiungiamo
		if (!$this->user->address)
			{
			$sql = "select * from user where id=" . $dbconn->sqlInteger($this->user->id);
			$user = $this->getRecordset($sql, $dbconn, 1)->records[0];
			$user->address = $record->address;
			$user->address_blob = $record->address_blob;
			$user->latitude = $record->latitude;
			$user->longitude = $record->longitude;
			$user->id_city = $record->id_city;
			$this->setEditorData($user);
			$this->saveRecordset($user->recordset);
			$this->user = $this->record2Object($user);
			}

		if ($send_mail && $this->form->is_mail_sent)
			{
			$record->message_id = $this->sendPec($record);
			}

		if (!$record->weather_blob)
			{
			// troppo delicato farlo a ogni modifica; nel caso ci pensa l'admin
			$this->setHitWeather($record);
			}
		$this->saveRecordset($record->recordset);

		$this->saveDoc($this->form->inputControls["doc"]);

		$dbconn->commitTransaction();
		$this->response();
		}
		
	//***************************************************************************
	function isEditable($record)
		{
		return $this->user->is_sys_admin || !$record || ($record->id_user == $this->user->id && !$record->is_mail_sent);
		}
		
	//*****************************************************************************
	function rpc_getMessageToSend($registered_at, $address, $message_notes, $id_intensity, $id_duration)
		{
		// creiamo un finto hit per costruire il body
		$hit = $this->getRecordset("select * from hit", $dbconn, 0)->add();
		$hit->id_user = $this->user->id;
		$hit->address = $address;
		$hit->registered_at = $registered_at;
		$hit->message_notes = $message_notes;
		$hit->id_intensity = $id_intensity;
		$hit->id_duration = $id_duration;
		return $this->getPecPreview($hit);
		
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
