<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		if (!$stepsData || !$stepsData->last_name)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, false);
			}
		
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC - 4", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;

		$ctrl = $this->form_geoBlock($readOnly, !$readOnly);
		if ($this->user->address && $this->user->address_blob)
			{
			// prevalorizziamo con l'indirizzo utente
			$value = new \stdClass();
			$value->address = $this->user->address;
			$value->address_blob = $this->user->address_blob;
			$ctrl->value = json_encode($value);
			}
		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Avanti >>");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		
		// aggiorniamo l'utente
		$dbconn = $this->getDBConnection();
		$dbconn->beginTransaction();
		$sql = "select *" .
				" from user" .
				" where email=" . $dbconn->sqlString($this->user->email) .
				" and not is_deleted";
		$user = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$user)
			{
			// e come cavolo è possibile??? 
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, false);
			}

		$user->first_name = $stepsData->first_name;
		$user->last_name = $stepsData->last_name;
		$user->insertValue("address", $this->form->address->address);
		$this->setLocationInfo($user, $this->form->address->address_blob);
		$this->setEditorData($user);
		$this->saveRecordset($user->recordset);
		$this->user = $this->record2Object($user);

		$dbconn->commitTransaction();
		$stepsData->step_4 = true;
		// andiamo allo step successivo
		$this->redirect("hit_wiz_4.2.php");
			
		}

	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
