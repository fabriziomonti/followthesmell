<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC - 1", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;
		
		$this->form->addEmail("email", "La tua email", $readOnly, !$readOnly);
		$this->form->addCaptcha("captcha", "Inserisci il codice di controllo che vedi nel riquadro nero", $readOnly, !$readOnly);
		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Avanti >>");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$stepsData = new stdClass();
		$stepsData->email = $this->form->email;
		$stepsData->control_code = $this->getPassword();
		$this->sessionData["hit_wiz_steps_data"] = $stepsData;
		
		$subject = "Codice di controllo [$this->title]";
		$body = "Ciao,\n\n" .
				"hai attivato la procedura per inviare una nuova segnalazione di cattivo odore a ARPAE? " .
				" Se sì, copia questo codice di controllo e incollalo nel modulo che te lo richiede:\n\n" .
				"$stepsData->control_code\n\n" .
				"Altrimenti ti preghiamo di scusarci: qualche buontempone ha usato sconsideratamente" .
				" il tuo indirizzo di posta elettronica; in questo caso non devi fare assolutamente nulla," .
				" la registrazione non avrà alcun effetto e il tuo indirizzo email" .
				" non sarà salvato in alcun modo.\n\n" .
				"Ciao dallo staff di $this->title!\n\n" .
				"(non rispondere a questo messaggio: la casella di posta non è presidiata)";
		if (!$this->sendMail($stepsData->email, $subject, $body))
			{
			$this->showMessage("Errore invio messaggio email", 
								"Si è verificato un errore durante l'invio del messaggio di posta elettronica." .
								" Sei pregato di cercare di ripetere l'operazione o di avvertire " .
								"<a href='mailto:" . $this->supportEmail->address . "'>l'assistenza tecnica</a>.");
			
			}
		
		$this->redirect("hit_wiz_2.php");
		}

	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
