<?php
error_reporting(E_ALL & ~ (E_NOTICE | E_DEPRECATED | E_STRICT ));

//****************************************************************************************
// intercetta warning declation of...
if (PHP_MAJOR_VERSION >= 7) {
    set_error_handler(function ($errno, $errstr) {
       return strpos($errstr, 'Declaration of') === 0;
    }, E_WARNING);
}

//****************************************************************************************
include_once (__DIR__ . "/defines.inc.php");
include_once (__DIR__ . "/config.inc.php");
include_once (__DIR__ . "/walibs4/waapplication/waapplication.class.php");


//****************************************************************************************
class followthesmell extends waLibs\waApplication
	{
	var $directoryDoc;
	var $sessionData;
	var $fileConfigDB = '';
	var $debug = false;
	
	/**
	* contiene i dati dell'operatore loggato alla sessione
	*/
	var $user = array();
	
	var $userPreferences;
	
	var $startPage = 'tbl_hit.php';
	var $loginPage = 'frm_login.php';
	
	// indica se stiamo facendo vedere qualcosa in una finestra figlia
	var $childWindow = false;

	public static $applicationInstance;
	
	//****************************************************************************************
	/**
	* costruttore
	*
	*/
	function __construct($checkUser = true)
		{
		$this->useSession = true;
		$this->domain = APPL_DOMAIN;
		$this->httpwd = APPL_DIRECTORY;
		$this->directoryTmp = APPL_TMP_DIRECTORY;
		$this->name = APPL_NAME;
		$this->title = APPL_TITLE;
		$this->version = APPL_REL;
		$this->versionDate = APPL_REL_DATE;
		$this->smtpServer = APPL_SMTP_SERVER;
		$this->smtpUser = APPL_SMTP_USER;
		$this->smtpPassword = APPL_SMTP_PWD;
		$this->smtpSecurity = APPL_SMTP_SECURE;
		$this->smtpPort = APPL_SMTP_PORT;
		$this->supportEmail = json_decode(APPL_SUPPORT_ADDR);
		$this->infoEmail = json_decode(APPL_INFO_ADDR);
		
		$this->directoryDoc = APPL_DOC_DIRECTORY;
		$this->debug = defined("APPL_DEBUG") && APPL_DEBUG;
		
		$this->fileConfigDB = __DIR__ . "/dbconfig.inc.php";
		
		$this->init();
		
		$this->sessionData = &$_SESSION;
		$this->user = &$this->sessionData['user'];
		
		self::$applicationInstance = $this;
		
		// la modalità di navigazione di default è
		// - per i pc a iframe
		// - per i mobile a finestre
		if (!isset($this->sessionData["is_mobile"]))
			{
			include_once ("Mobile-Detect/Mobile_Detect.php");
			$detect = new Mobile_Detect;
			$this->sessionData["is_mobile"] = $detect->isMobile();
			}

		// impostazioni delle preferenze
		$default_navigation_mode = $this->sessionData["is_mobile"] ? waLibs\waApplication::NAV_WINDOW : waLibs\waApplication::NAV_INNER;
		$default_table_actions = $this->sessionData["is_mobile"] ? "watable_actions_left" : "watable_actions_context";
        $this->userPreferences = unserialize(base64_decode($_COOKIE[$this->name . "_prefs"]));
		$this->userPreferences["navigation_mode"] = isset($this->userPreferences["navigation_mode"]) ? $this->userPreferences["navigation_mode"] : $default_navigation_mode;
		$this->userPreferences["table_actions"] = isset($this->userPreferences["table_actions"]) ? $this->userPreferences["table_actions"] : $default_table_actions;
		$this->userPreferences["max_table_rows"] = $this->userPreferences["max_table_rows"] ? $this->userPreferences["max_table_rows"] : waLibs\waTable::LIST_MAX_REC;		
		$this->navigationMode = $this->userPreferences["navigation_mode"];
		
		// verifica validità dell'utente loggato
		$this->checkUser($checkUser);
		
		
		}
	
	//*****************************************************************************
	/**
	* @return waConnessiondDB
	*/
	function getDBConnection($fileConfigDB = '')
		{
		return parent::getDBConnection($fileConfigDB ? $fileConfigDB : $this->fileConfigDB);
		}
		
	//*****************************************************************************
	function record2Array(waLibs\waRecord $record)
		{
		$retval = array();
		$rs = $record->recordset;
		for ($i = 0; $i < $rs->fieldNr(); $i++)
			{
			$retval[strtolower($rs->fieldName($i))] = $record->$i;
			}
		
		return $retval;
		}
		
	//*****************************************************************************
	function record2Object(waLibs\waRecord $record)
		{
		return (object) $this->record2Array($record);
		}
		
	//*****************************************************************************
	function encryptPassword($password, $rand = null, $hash = null)
		{
		$sha512 = "$6$";
		$rand = $rand ? $rand : rand(1000,100000);
		$rounds =  "rounds=$rand$";
		if (!$hash)
			{
			$hash =   "";
			$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.';
			for($i=0;$i < 16;$i++)
				{
				$hash .= $characters[rand(0, strlen($characters)-1)];
				}
			}	
		$hash .= "$";
			
		$salt = $sha512.$rounds.$hash;
		return crypt($password,$salt);
		}	

	//*****************************************************************************
	function checkPassword($inputUser, $password)
		{
		$pass = explode("$", $password);
		$hash = array_pop($pass);
		$salt = implode("$", $pass);
		$salt .= "$";
		if (!(crypt($inputUser, $salt) == $password))
			{
			return false;
			}
		return true;
		}

	//***************************************************************************
	// ritorna una chiave personale da scrivere sul record
	//***************************************************************************
	function getPassword()
		{
		$mt = microtime();
		$elems = explode(" ", microtime());
		$pwd = chr((substr($elems[1], -1) + ord('A'))) . substr($elems[0], 2, 4) . substr($elems[1], -3);
		$pwd = substr($pwd, 0, -1) . chr(substr($pwd, -1) + ord('l'));
		return $pwd;
		}
		
	//***************************************************************************
	// verifica che una password sia strong
	//***************************************************************************
	function isPasswordStrong($password)
		{
		$r1='/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/';
		
		return (boolean) preg_match($r1, $password);
		}
		
	//*****************************************************************************
	function setEditorData(waLibs\waRecord $record)
		{
		if (!$record->value(0))
			{
			$record->inserted_at = time();
			}
		$record->edited_at = time();
		$record->id_user_edit = $this->user ? $this->user->id : 0;
		$record->ip_user_edit = $_SERVER['REMOTE_ADDR'] == null ? "" : $_SERVER['REMOTE_ADDR'];
		}

	//**************************************************************************
	// verifica che un form soddisfi i requisiti di obbligatorietà, 
	// altrimenti interrompe l'operazione di salvataggio con un messaggio
	function checkMandatory(waLibs\waForm $form)
		{
		if (!$form->checkMandatory())
			{
			$this->showMandatoryError();
			}
		}

	//*****************************************************************************
	function checkLockViolation(waLibs\waForm $form)
		{
		if ($form->record->value($form->fieldNameModId) != $form->getModId())
			{
			$this->showMessage("Errore violazione lock", "Errore di violazione del lock del record: il record e'" .
					" stato modificato da un altro operatore tra il momento " .
					" della tua lettura e il momento della tua scrittura." .
					" Sei pregato di ricaricare la pagina di modifica e" .
					" ripetere le modifiche che hai effettuato");
			}
		}

	//***************************************************************************
	/**
	* 
	* @return string
	*/
	function url_get_contents($url)
		{
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$buffer = curl_exec($curl);
		curl_close($curl);		
		
		return $buffer;
		}

	//***************************************************************************
	/**
	 * restituisce il protocllo di connessione (http/s)
	*/
	function getProtocol()
		{
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";

		return $protocol;
		}
		
	//*************************************************************************
	// ritorna l'url di un documento che ha una corrispondenza in una riga di db
	function primitiveGetUrlDoc($table, $field_name, $recPK, $field_value)
		{
		if (!$field_value)
			{
			return;
			}

		$ext = pathinfo($field_value, PATHINFO_EXTENSION);
		$fs_doc = "$this->directoryDoc/$table/$field_name/$recPK.$ext";
		
		$params["t"] = $table;
		$params["c"] = $field_name;
		$params["k"] = $recPK;
		$params["e"] = $ext;
		$params["n"] = base64_encode(openssl_encrypt(session_id(), 'aes-256-cbc', APPL_ENCRYPTION_IV, 0, APPL_ENCRYPTION_IV));
		return $this->getProtocol() . 
				"://$this->domain$this->httpwd/downloaddoc.php.$ext" .
				"?p=" . base64_encode(serialize($params)) .
				"&t=" . @filemtime($fs_doc);
		}
		
	//*****************************************************************************
	// logga le scritture del database su file sequenziale; questa funzione e'
	// la callback per il DB driver (vedi parametro $WADB_LOG_CALLBACK_FNC in
	// dbconfig.inc.php
	/**
	 * @todo attenzione!
	 * 
	 * @param type $sql
	 * @return type
	 */
	function logDBWrite($sql)
		{
		if (!defined("APPL_NOME_FILE_LOG_DB") || !APPL_NOME_FILE_LOG_DB)
			return;
			
		$record = array(
						date("Y-m-d H:i:s"),
						$this->user->id,
						$this->user->email ,
						$_SERVER['REMOTE_ADDR'],
						$_SERVER['PHP_SELF'],
						$sql
					);
		
		$fp = fopen(APPL_NOME_FILE_LOG_DB . "." . date("Ymd"), "a");
		fputcsv($fp, $record);
		fclose($fp);
		}
		
	//***************************************************************************** 
	function logAccess(waLibs\waDBConnection $dbconn = null, $out = 0)
		{
		$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
		$sql = "INSERT INTO access (id_user, section, flag_out, inserted_at, edited_at, id_user_edit, ip_user_edit) VALUES (" .
					$dbconn->sqlInteger($this->user->id) . "," .
					$dbconn->sqlString($this->sectionName) . "," .
					$dbconn->sqlInteger($out) . "," .
					$dbconn->sqlDateTime(time()) . "," .
					$dbconn->sqlDateTime(time()) . "," .
					$dbconn->sqlInteger($this->user->id) . "," .
					$dbconn->sqlString($_SERVER['REMOTE_ADDR']) . ")";
		$this->dbExecute($sql, $dbconn);
		
		}
		
	//***************************************************************************
	/** 
	 * dati componenti dell'indirizzo tornati da googlemaps ritorna una
	 * struttura dati normalizzata (si spera)
	 * 
	 * @param array $address_components
	 * @return stdClass
	 */
		
	function normalizeAddress($address_components)
		{
		$address = new stdClass();

		foreach ($address_components as $component)
			{
			if (in_array("street_number", $component->types))
				{
				$address->house_number = $component->long_name;
				}
			elseif (in_array("route", $component->types))
				{
				$address->street = $component->long_name;
				}
			elseif (in_array("locality", $component->types))
				{
				$address->locality = $component->long_name;
				}
			elseif (in_array("postal_town", $component->types))
				{
				$address->postal_town = $component->long_name;
				}
			elseif (in_array("administrative_area_level_3", $component->types))
				{
				$address->city = $component->long_name;
				}
			elseif (in_array("administrative_area_level_2", $component->types))
				{
				$address->province = $component->long_name;
				$address->province_short = $component->short_name;
				}
			elseif (in_array("administrative_area_level_1", $component->types))
				{
				$address->region = $component->long_name;
				$address->region_short = $component->short_name;
				}
			elseif (in_array("country", $component->types))
				{
				$address->country = $component->long_name;
				$address->country_short = $component->short_name;
				}
			elseif (in_array("postal_code", $component->types))
				{
				$address->postal_code = $component->long_name;
				}
			elseif (in_array("notes", $component->types))
				{
				$address->notes = $component->long_name;
				}
			}

		// tenatitvi di determinare city (cambia a seconda della nazione)
		if (!$address->city)
			{
			$address->city = $address->postal_town;
			}
		if (!$address->city)
			{
			$address->city = $address->locality;
			}
		if (!$address->city)
			{
			$address->city = $address->province;
			}
			
		// certe città straniere non hanno l'equivalente della provincia
		if (!$address->province)
			{
			$address->province = "--";
			$address->province_short = "--";
			}
			
		return $address;
		}
		
	//*****************************************************************************
	/**
	 * dati componenti dell'indirizzo tornati da googlemaps
	 * crea su db le informazioni (country, region, province, city)
	 * e ritorna l'id_city
	 * 
	 * @return int
	 */
	function getIdCity($address_components, \waLibs\waDBConnection $dbconn = null)
		{
		$address = $this->normalizeAddress($address_components);
		if (!$address->city || !$address->province || !$address->region || !$address->country)
			{
			return;
			}

		// cerchiamo lo stato
		$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
		$sql = "select * from country where name=" . $dbconn->sqlString($address->country);
		$rs = $this->getRecordset($sql, $dbconn, 1);
		$country = $rs->records[0];
		if (!$country)
			{
			$country = $rs->add();
			$country->name = $address->country;
			$country->short_name = substr($address->country_short, 0, $country->recordset->fieldMaxLength("short_name"));
			$this->setEditorData($country);
			$this->saveRecordset($country->recordset);
			}
		$address->id_country = $country->id;
		
		// cerchiamo la regione
		$sql = "select * from region where id_country=" . $dbconn->sqlInteger($country->id) .
					" and name=" . $dbconn->sqlString($address->region);
		$rs = $this->getRecordset($sql, $dbconn, 1);
		$region = $rs->records[0];
		if (!$region)
			{
			$region = $rs->add();
			$region->id_country = $country->id;
			$region->name = $address->region;
			$region->short_name = substr($address->region_short, 0, $region->recordset->fieldMaxLength("short_name"));
			$this->setEditorData($region);
			$this->saveRecordset($region->recordset);
			}
		$address->id_region = $region->id;
				
		// cerchiamo la provincia
		$sql = "select * from province where id_region=" . $dbconn->sqlInteger($region->id) .
					" and name=" . $dbconn->sqlString($address->province);
		$rs = $this->getRecordset($sql, $dbconn, 1);
		$province = $rs->records[0];
		if (!$province)
			{
			$province = $rs->add();
			$province->id_region = $region->id;
			$province->name = $address->province;
			$province->short_name = substr($address->province_short, 0, $province->recordset->fieldMaxLength("short_name"));
			$this->setEditorData($province);
			$this->saveRecordset($province->recordset);
			}
		$address->id_province = $province->id;
				
		// cerchiamo la città
		$sql = "select * from city where id_province=" . $dbconn->sqlInteger($province->id) .
					" and name=" . $dbconn->sqlString($address->city);
		$rs = $this->getRecordset($sql, $dbconn, 1);
		$city = $rs->records[0];
		if (!$city)
			{
			$city = $rs->add();
			$city->id_province = $province->id;
			$city->name = $address->city;
			$city->postal_code = $address->postal_code;
			$this->setEditorData($city);
			$this->saveRecordset($city->recordset);
			}
			
		return $city->id;

		}
		
	//***************************************************************************
	// a partire dalle coordinate postate in un modulo, cerca e valorizza il
	// id_city della riga (ovviamente modulo e riga devono implementare
	// i campi/controlli latitude-longitude-id_city)
	function setLocationInfo(\waLibs\waRecord $record, $address_blob)
		{
		$address_info = json_decode($address_blob);
		if (!$address_info)
			{
			// indirizzo vuoto
			return $record->address = $record->address_blob = $record->latitude = $record->longitude = $record->id_city = null;
			}
		if ($address_info->geometry->location->lat == $record->latitude && $address_info->geometry->location->lng  == $record->longitude)
			{
			// indirizzo uguale al precedente
			return;
			}

		$record->id_city = $this->getIdCity($address_info->address_components, $record->recordset->dbConnection);
		$record->latitude = $address_info->geometry->location->lat;
		$record->longitude = $address_info->geometry->location->lng;
//		$record->address = $record->address ? $record->address : $address_info->formatted_address;
		$record->address_blob = $address_blob;
		}
		
	//***************************************************************************
	/** restituisce i dati di un indirizzo
	 * 
	 * @return stdClass
	 */
		
	function geoLocalize($address)
		{
		$address = urlencode($address);
		$url = APPL_MAPS_API_URL . "/geocode/json" .
				"?address=$address" . 
				"&result_type=street_address" .
				"&language=it-IT" .
				"&key=" . APPL_MAPS_API_SERVER_KEY;
		$data = $this->url_get_contents($url);
		$data = json_decode($data);
		$data = json_encode($data->results[0]);

		return $data;
		}

	//***************************************************************************
	function sendMailPassword(waLibs\waRecord $record)
		{
		$pwd = $this->getPassword();
		$record->pwd = $this->encryptPassword($pwd);
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		
		$this->updateOpendcn($record);

		$mail_data = new stdClass();
		$mail_data->app_title = $this->title;
		$mail_data->user_name = $record->first_name;
		$mail_data->user_username = $record->username ? $record->username : $record->email;
		$mail_data->user_password = $pwd;
		$mail_data->login_page = $this->getProtocol() . "://$this->domain$this->httpwd/$this->loginPage";
		
		$this->sendMail("send_password", $mail_data, true, $record->email, $this->supportEmail);
		}
	
	//*****************************************************************************
	/**
	 */
	function getAddressComponent(waLibs\waRecord $record, $component_name, $address_blob_field_name = "address_blob")
		{
		if ($record->$address_blob_field_name)
			{
			$address = json_decode($record->$address_blob_field_name);
			$components = $address->address_components;
			foreach ($components as $component)
				{
				foreach ($component->types as $type)
					{
					if ($type == $component_name)
						{
						return $component->long_name;
						}
					}
				}
			}
		}
		
	//***************************************************************************
	function number_format($number, $decimal_nr = 2)
		{
		return number_format($number, $decimal_nr, ",", ".");
		}
		
	//*****************************************************************************
	// restituisce il testo della normativa privacy
	//*****************************************************************************
	function getPrivacyText()
		{
		return file_get_contents(__DIR__ . "/ui/docs/normativa_privacy.txt");
		}

	//***************************************************************************
	function getBehaviourText()
		{
		return "Registrandomi presso questa applicazione mi impegno a:\n\n" .
				"- produrre e diffondere solo notizie vere\n" .
				"- non essere offensivo o violento nei confronti di nessuno\n" .
				"- accettare che l'amministratore di sistema sospenda la mia iscrizione" .
					" qualora verificasse il mancato rispetto delle regole precedenti.";
		}
		
	//****************************************************************************************
	/**
	* verifica se un indirizzo email è valido
	*
	* @ignore
	* @return string
	*/
	function checkMailAddress($address)
		{
	    if (strlen($address) < 7) 
			{
			return false;
			}
			
	    $arr = explode("@", $address);
	    if (count($arr) != 2) 
			{
			return false;
			}
		
	    $user = $arr[0];
	    $domain = $arr[1];

	    // check dei caratteri dello user
	    if (strlen($user) < 2) 
			{
			return false;
			}
	    for ($char_cntr = 0; $char_cntr < strlen($user); $char_cntr++)
	        {
	        $to_check = substr($user, $char_cntr, 1);
	        if (! preg_match("/([0-9]|[A-Z]|-|_|\.)/i", $to_check)) 
				{
				return false;
				}
	        }

	    // check dei caratteri del dominio
	    $dom_array = explode(".", $domain);
	    if (count($dom_array) < 2) 
			{
			return false;
			}
		
	    for ($cntr = 0; $cntr < count($dom_array); $cntr++)
	        {
	        if (strlen($dom_array[$cntr]) < 2) 
				{
				return false;
				}
	        for ($char_cntr = 0; $char_cntr < strlen($dom_array[$cntr]); $char_cntr++)
	            {
	            $to_check = substr($dom_array[$cntr], $char_cntr, 1);
	            if (! preg_match("/([0-9]|[A-Z]|-)/i", $to_check)) 
					{
					return false;
					}
	            }
	        }

		return true;
			
		}
		
		
	//***************************************************************************
	/**
	* @return void
	*/
	function getMenu()
		{
		if ($this->childWindow)
			{
			return false;
			}

		$m = new waLibs\waMenu();
		$m->open();

			$m->openSection("Home", "$this->httpwd/home.php");
			$m->closeSection();

			$m->openSection("Segnalazioni", "$this->httpwd/tbl_hit.php");
				$m->addItem("Lista", "$this->httpwd/tbl_hit.php");
				$m->addItem("Procedura guidata", "$this->httpwd/hit_wiz_0.php");
			$m->closeSection();

			$m->openSection("Mappe");
				$m->addItem("Oggi", "$this->httpwd/frm_map.php?from=" . mktime(0,0,0) . "&to=" . mktime(23,59,59));
				$m->addItem("Ieri", "$this->httpwd/frm_map.php?from=" . mktime(0,0,0, date("n"), date("j") - 1) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
				$m->addItem("Settimana", "$this->httpwd/frm_map.php?from=" . mktime(0,0,0, date("n"), date("j") - 7) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
				$m->addItem("Mese", "$this->httpwd/frm_map.php?from=" . mktime(0,0,0, date("n") - 1, date("j")) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
				$m->addItem("Periodo", "$this->httpwd/frm_map.php");
			$m->closeSection();

			$m->openSection("Grafici");
				$m->openSection("Orario");
					$m->addItem("Oggi", "$this->httpwd/frm_chart.php?from=" . mktime(0,0,0) . "&to=" . mktime(23,59,59));
					$m->addItem("Ieri", "$this->httpwd/frm_chart.php?from=" . mktime(0,0,0, date("n"), date("j") - 1) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
					$m->addItem("Settimana", "$this->httpwd/frm_chart.php?from=" . mktime(0,0,0, date("n"), date("j") - 7) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
					$m->addItem("Mese", "$this->httpwd/frm_chart.php?from=" . mktime(0,0,0, date("n") - 1, date("j")) . "&to=" . mktime(23,59,59, date("n"), date("j") - 1));
					$m->addItem("Periodo", "$this->httpwd/frm_chart.php");
				$m->closeSection();
				$m->openSection("Giornaliero");
					$m->addItem("Settimana", "$this->httpwd/frm_day_chart.php?from=" . mktime(0,0,0, date("n"), date("j") - 7) . "&to=" . mktime(0,0,0, date("n"), date("j") - 1));
					$m->addItem("Mese", "$this->httpwd/frm_day_chart.php?from=" . mktime(0,0,0, date("n") - 1, date("j")) . "&to=" . mktime(0,0,0, date("n"), date("j") - 1));
					$m->addItem("Periodo", "$this->httpwd/frm_day_chart.php");
				$m->closeSection();
			$m->closeSection();

			$m->openSection("Documenti", "$this->httpwd/tbl_doc.php");
			$m->closeSection();

			$m->openSection("Utenti", "$this->httpwd/tbl_user.php");
				$m->addItem("Lista", "$this->httpwd/tbl_user.php");
				$m->addItem("Mappa", "$this->httpwd/user_map.php");
			$m->closeSection();

			// eventuale segnalazione mancanza dati profilo
			$options = new stdClass();
			if (!$this->user->first_name || !$this->user->last_name || !$this->user->address)
				{
				$options->badge = "!";
				$options->badge_class = "red";
				}
			$m->openSection("Strumenti", "", "", $options);
				$m->addItem("Profilo", "javascript:document.waPage.openPage(\"frm_profile.php\")", "", $options);
				$m->addItem("Preferenze", "javascript:document.waPage.openPage(\"frm_preferences.php\")");
				$m->addItem("Modello PEC", "javascript:document.waPage.openPage(\"frm_pec_template.php\")");
				$m->addItem("Logout", "$this->httpwd/logout.php");
			$m->closeSection();
			
		$m->close();

		include_once __DIR__ . "/ui/view/wamenu/wamenu.php";
		$m->view = new \followthesmell\wamenu_view();
		
		return $m;
		}
		
	//*****************************************************************************
	// verifica l'user loggato
	//*****************************************************************************
	/**
	 * 
	 * @param boolean $checkUser se l'user deve essere verificato
	 */
	function checkUser($checkUser)
		{
		if ($this->user)
			{
			return;
			}
			
		if ($checkUser)
			{
			$this->redirect($this->loginPage);
			}
			
		}
		
	//*****************************************************************************
	// crea il blocco dei bottoni standard di una form
	//*****************************************************************************
	function form_submitButtons(waLibs\waForm $form, 
									$readOnly = false, 
									$showDeleteButton = true,
									$cmdOkCaption = 'Registra', 
									$cmdCancelCaption = 'Annulla', 
									$cmdDeleteCaption = 'Elimina',
									$cmdCloseCaption = 'Chiudi')
		{
		if (!$readOnly)
			{
			$okCtrl = new waLibs\waButton($form, 'cmd_submit', $cmdOkCaption);
			}
		if ($readOnly)
			$cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCloseCaption);
		else 
			$cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCancelCaption);
		$cancelCtrl->cancel = true;
		$cancelCtrl->submit = false;
	
		if ($showDeleteButton && ! $readOnly)
			{
			$ctrl = new waLibs\waButton($form, 'cmd_delete', $cmdDeleteCaption);
			$ctrl->delete = true;
			}
			
		}
			
	//***************************************************************************
	/**
	* -
	*/
	function getPageTitle()
		{
		return $this->items["title"] ? $this->items["title"]->value : "";
		}
		
	//***************************************************************************
	/**
	* -
	* @return waLibs\waTable
	*/
	function getTable($sqlOrArray, $name = null, $type = null)
		{
		if (stripos($type, "quickedit") !== false)
			{
			$table = new waLibs\waTable_quickedit($sqlOrArray, $this->fileConfigDB);
			}
		elseif (stripos($type, "edit") !== false)
			{
			$table = new waLibs\waTable_edit($sqlOrArray, $this->fileConfigDB);
			}
		else
			{
			$table = new waLibs\waTable($sqlOrArray, $this->fileConfigDB);
			}
			
		$table->name = $name ? $name : $table->name;
		
		$view_file = __DIR__ . "/ui/view/" . pathinfo($_SERVER["PHP_SELF"], PATHINFO_FILENAME) . "_$table->name.php";
		if (file_exists($view_file))
			{
			// se esiste un file che si chiama come pagina+table, allora quello
			// contiene la view specifica della table
			include_once $view_file;
			$view_class = "\\followthesmell\\" . pathinfo($view_file, PATHINFO_FILENAME) . "_view";
			$table->view = new $view_class();
			}
		else
			{
			// view di default
			$viewType = $this->userPreferences["table_actions"];

			include_once __DIR__ . "/ui/view/watable/$viewType.php";
			$class = "\\followthesmell\\$viewType" . "_view";
			$table->view = new $class();
			}
		
		$table->listMaxRec = $this->userPreferences["max_table_rows"] ? 
								$this->userPreferences["max_table_rows"] : 
								waLibs\waTable::LIST_MAX_REC;
			
		// $table->title = $this->getPageTitle();
		$table->removeAction("Details");
		
		// se viene passato il nome, allora significa che è una seconda tabella
		// quindi non ha senso il bottone chiudi (è già sulla prima). E' un po' una
		// schifezza, ma dovrebbe funzionare...
		if ($this->childWindow && $name === null)
			{
			
			$table->addAction("Close", false, "Chiudi");
			// portiamo il bottone "chiudi" in prima posizione
			$this->moveTableActionToTop($table, "Close");
			}

		return $table;
		}
		
	//***************************************************************************
	/**
	* sposta un'azione prima delle altre
	* 
	*/
	function moveTableActionToTop(waLibs\waTable $table, $actionName)
		{
		$swappo[$actionName] = $table->actions[$actionName];
		foreach ($table->actions as $k => $v)
			{
			if ($k != $actionName)
				{
				$swappo[$k] = $v;
				}
			}
		$table->actions = $swappo;
		
		}
		
	//***************************************************************************
	/**
	* -
	* @return waLibs\waForm
	*/
	function getForm($destinationPage = null, $name = null)
		{
		$form = new waLibs\waForm($destinationPage, $this);
		$form->loadExtensions(WAMODULO_EXTENSIONS_DIR);
		$form->fieldNameModId = "edited_at";
		$form->name = $name ? $name : $form->name;
		
		$view_file = __DIR__ . "/ui/view/" . pathinfo($_SERVER["PHP_SELF"], PATHINFO_FILENAME) . "_$form->name.php";
		if (file_exists($view_file))
			{
			// se esiste un file che si chiama come pagina+form, allora quello
			// contiene la view specifica della form
			include_once $view_file;
			$view_class = "\\followthesmell\\" . pathinfo($view_file, PATHINFO_FILENAME) . "_view";
			$form->view = new $view_class();
			}
		else
			{
			// view di default
			include_once __DIR__ . "/ui/view/waform/waform.php";
			$form->view = new \followthesmell\waform_view();
			}

		return $form;
		}
		
	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	*/
	function show()
		{
		$version_data = (object) array("nr" => $this->version, 
			"date" => date("Y-m-d", $this->versionDate),
			"supportEmailAddress" => $this->supportEmail->address);
		$this->addItem($version_data, "version_data");
		$this->addItem(APPL_MAPS_API_BROWSER_KEY, "google_maps_api_key");
		$this->addItem($this->sessionData["is_mobile"], "is_mobile");
		$this->addItem($this->user ? 1 : 0, "is_user_logged");
		$this->addPageHelpItem();
		
		if (!$this->view)
			{
			$view_file = __DIR__ . "/ui/view/" . pathinfo($_SERVER["PHP_SELF"], PATHINFO_BASENAME);
			if (file_exists($view_file))
				{
				// se esiste un file che si chiama come la pagina, allora quello
				// contiene la view specifica della pagina
				include_once $view_file;
				$view_class = "\\followthesmell\\" . pathinfo($view_file, PATHINFO_FILENAME) . "_view";
				$this->view = new $view_class();
				}
			else
				{
				// view di default
				include_once __DIR__ . "/ui/view/waapplication/waapplication.php";
				$this->view = new \followthesmell\waapplication_view();
				}
			}
			
		waLibs\waApplication::show();
		exit();
		}
		
	//*************************************************************************
	// alza il flag di help online sulla pagina 
	function addPageHelpItem()
		{
		if ($this->user->is_sys_admin)
			{
			// un sys-admin la vede sempre
			return $this->addItem(true, "help");
			}
			
		$dbconn = $this->getDBConnection();
		$sql = "select * from help" .
				" where section=" . $dbconn->sqlString($this->sectionName) .
				" and page=" . $dbconn->sqlString(pathinfo($_SERVER["PHP_SELF"], PATHINFO_FILENAME)) .
				" and control=" . $dbconn->sqlString("") .
				" and text is not null" . 
				" and text!=" . $dbconn->sqlString("") .
				" and not is_deleted";
		$help = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if ($help)
			{
			$this->addItem(true, "help");
			}

		}
		
	//*************************************************************************
	// override per intercettare eventuali form e aggiungere il flag di help
	// alle labels
	public function addItem($item, $name = '', $type = 'string')
		{
		if (is_a($item, "waLibs\\waForm"))
			{
			$page = pathinfo($_SERVER["PHP_SELF"], PATHINFO_FILENAME);
			foreach ($item->labels as $label) 
				{
				$label->help = false;
				}
			if ($this->user->is_sys_admin)
				{
				foreach ($item->labels as $label) 
					{
					$label->help = (object) ["section" => $this->sectionName, "page" => $page, "title" => $this->getPageTitle()];
					}
				}
			else
				{
				$dbconn = $this->getDBConnection();
				$sql = "select * from help" .
						" where section=" . $dbconn->sqlString($this->sectionName) .
						" and page=" . $dbconn->sqlString($page) .
						" and control!=" . $dbconn->sqlString("") .
						" and text!=" . $dbconn->sqlString("") .
						" and text is not null" . 
						" and not is_deleted" .
						" order by control";
				$help = $this->getRecordset($sql, $dbconn);
				foreach ($help->records as $rec)
					{
					if ($item->labels[$rec->control])
						{
						$item->labels[$rec->control]->help = (object) ["section" => $this->sectionName, "page" => $page, "title" => $this->getPageTitle()];
						}
					}
				}
			}
		
		parent::addItem($item, $name, $type);
		}
		
	//*************************************************************************
	// definisce l'url del documento all'interno di un controllo waUpload
	function setUrlDoc(waLibs\waUpload $ctrl)
		{
		$record = $ctrl->form->recordset->records[0];
		if ($record && $record->value($ctrl->name))
			{
			$ctrl->showPage =  $this->getUrlDoc($record, $ctrl->name);
			}
		}
		
	//***************************************************************************** 
	// elimina eventuali documenti salvati inprecedenza
	//***************************************************************************** 
	function deleteDoc(waLibs\waUpload $ctrl, \waLibs\waRecordset $rs = null)
		{
		$rs = $rs ? $rs : $ctrl->form->recordset;
		$id = $rs->records[0] ? $rs->records[0]->value(0) : $rs->dbConnection->lastInsertedId();
		$pattern = "$this->directoryDoc/" . $rs->tableName(0) . "/$ctrl->name/$id.*";
		$files = glob($pattern);
		if ($files)
			{
			foreach ($files as $file)
				{
				@unlink($file);
				}
			}
		}
		
	//***************************************************************************** 
	// salva l'eventuale documento allegato
	//***************************************************************************** 
	function saveDoc(waLibs\waUpload $ctrl, \waLibs\waRecordset $rs = null)
		{
		// salvataggio del documento (se c'e'...)
		if ($ctrl->isToDelete())
			{
			// cancelliamo un eventuale documento esistente
			$this->deleteDoc($ctrl, $rs);
			}
		elseif ($ctrl->getUploadError())
			{
			$this->showMessage("Errore caricamento file", "Si e' verificato l'errore " .
					$ctrl->getUploadError() .
					" durante il caricamento del documento $ctrl->name." .
					" Si prega di avvertire l'assistenza tecnica.", false, true);
			}
		elseif ($ctrl->isToSave())
			{
			$this->deleteDoc($ctrl);
			$rs = $rs ? $rs : $ctrl->form->recordset;
			$dest = "$this->directoryDoc/" . $rs->tableName(0);
			@mkdir($dest);
			$dest .= "/$ctrl->name";
			@mkdir($dest);

			$id = $rs->records[0] ? $rs->records[0]->value(0) : $rs->dbConnection->lastInsertedId();
			$dest = "$dest/$id." . pathinfo($ctrl->inputValue, PATHINFO_EXTENSION);
			if (!$ctrl->saveFile($dest))
				{
				$this->showMessage("Errore spostamento file", "Si e' verificato un errore " .
						" durante lo spostamento del documento $ctrl->name." .
						" Si prega di avvertire l'assistenza tecnica.", false, true);
				}
		if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1" || $_SERVER["REMOTE_ADDR"] == "::1") 
			{
			chmod($dest, 0660);
			}
				
			// controllo sulla lunghezza del nome del file
			if (strlen($ctrl->inputValue) > $rs->fieldMaxLength($ctrl->name))
				{
				$record = $rs->records[0];
				$record->insertValue($ctrl->name, substr($ctrl->inputValue, 0, $rs->fieldMaxLength($ctrl->name) - 5) . "." . pathinfo($ctrl->inputValue, PATHINFO_EXTENSION));
				$this->saveRecordset($rs);
				}
			}

		return true;			
		}
		
	//***************************************************************************** 
	// salva un documento non associato a un controllo di upload
	//***************************************************************************** 
	function saveDocFromBuffer(\waLibs\waRecord $record, $fld_name, $buffer)
		{
		$dest = "$this->directoryDoc/" . $record->recordset->tableName(0);
		@mkdir($dest);
		$dest .= "/$fld_name";
		@mkdir($dest);
		$id = $record->value(0);
		$dest = "$dest/$id." . pathinfo($record->$fld_name, PATHINFO_EXTENSION);
		file_put_contents($dest, $buffer);
		if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1" || $_SERVER["REMOTE_ADDR"] == "::1") 
			{
			chmod($dest, 0660);
			}
		
		}
	
	//***************************************************************************
	/**
	* -
	*/
	function getTitleContext($table, $description_field_name = "name", $key_name = "")
		{
		/**
		 * @todo sistemare
		 */
		$key_name = $key_name ? $key_name : "id_$table";
		$key = $_GET[$key_name];
		if (!$key)
			{
			return "";
			}
		$dbconn = $this->getDBConnection();
		$sql = "select $description_field_name from $table where id=" . $dbconn->sqlString($key);
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record)
			{
			return "";
			}

		return "\n" . htmlspecialchars($record->value(0));
		
		}

	//***************************************************************************
	function updateMM(waLibs\waRecord $record, $manyTableName)
		{
		$dbconn = $record->recordset->dbConnection;
		$one = $record->recordset->tableName("id");
		$table_mm = $one . "_" . $manyTableName;
		$one_id_name = "id_" . $one;
		$many_id_name = "id_$manyTableName";
		$sql = "delete from $table_mm where $one_id_name=" . $dbconn->sqlInteger($record->id);
		$this->dbExecute($sql, $dbconn);
		
		if (!is_array($this->form->$many_id_name))
			{
			return;
			}
			
		foreach ($this->form->$many_id_name as $many_id)
			{
			$sql = "INSERT INTO $table_mm ($one_id_name, $many_id_name) VALUES (" .
					$dbconn->sqlInteger($record->id) . "," .
					$dbconn->sqlInteger($many_id) . ")";
			$this->dbExecute($sql, $dbconn);
			}
		}
		
	//***************************************************************************
	/**
	 * crea il blocco standard per la gestione di una geo location
	 */
	function form_geoBlock($readOnly = false, $mandatory = false, $form = null, $label = "Indirizzo (geolocation)")
		{
		$form = $form ? $form : $this->form;
		return $form->addGeneric("waGeolocalize", "address", $label, $readOnly, $mandatory);
//		$this->form->addText("address", "Indirizzo (geolocation)", $readOnly, $mandatory);
//		$ctrl = new waLibs\waTextArea($this->form, "address_blob");
//			$ctrl->visible = false;
//			$ctrl->mandatory = $mandatory; 
//		$ctrl = new waLibs\waCurrency($this->form, "latitude");
//			$ctrl->visible = false;
//			$ctrl->mandatory = $mandatory; 
//			$ctrl->decimalNr = 6;
//		$ctrl = new waLibs\waCurrency($this->form, "longitude");
//			$ctrl->visible = false;
//			$ctrl->mandatory = $mandatory; 
//			$ctrl->decimalNr = 6;
		}
		
	//*************************************************************************
	// ritorna il path (locale) di un documento che ha una corrispondenza in 
	// una riga di db
	function getPathDoc(waLibs\waRecord $record, $fieldName, $table = '', $idName = '', $isAliasOf = '')
		{
		$table = $table ? $table : $record->recordset->tableName(0);
		$recPK = $idName ? $record->$idName : $record->value(0);

		if (!$record->$fieldName)
			{
			return;
			}

		$isAliasOf = $isAliasOf ? $isAliasOf : $fieldName;
		$ext = pathinfo($record->$fieldName, PATHINFO_EXTENSION);
		return "$this->directoryDoc/$table/$isAliasOf/$recPK.$ext";
		}
		
	//*************************************************************************
	// ritorna l'url di un documento che ha una corrispondenza in una riga di db
	/**
	 * 
	 * @param waLibs\waRecord $record
	 * @param string $fieldName
	 * @param string $table se vuoto prennde la table della chiave primaria, altrimenti la table data (evidentemente fk)
	 * @param string $idName se vuoto prennde il nome della chiave primaria del record; altrimenti il nome della chiave data (evidentemente fk)
	 * @return string
	 */
	function getUrlDoc(waLibs\waRecord $record, $fieldName, $table = '', $idName = '')
		{
		$table = $table ? $table : $record->recordset->tableName(0);
		$recPK = $idName ? $record->value($idName) : $record->value(0);
		return $this->primitiveGetUrlDoc($table, $fieldName, $recPK, $record->$fieldName);
		}
		
	//**************************************************************************
	// l'eliminazione standard di un record è logica, non fisica
	function deleteRecord(waLibs\waRecord $record, $end_script = true)
		{
		if (!$record)
			{
			return;
			}

		$record->is_deleted = 1;
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		if ($end_script)
			{
			$this->response();
			}
		}

	//***************************************************************************
	function getPecSubject(waLibs\waRecord $hit)
		{
		$ret = APPL_PEC_SUBJECT_TEMPLATE;
		if ($this->sessionData->message_subject_template)
			{
			$ret = $this->sessionData->message_subject_template;
			}
		else 
			{
			// inefficiente!!
			$dbconn = $hit->recordset->dbConnection;
			$sql = "select * from pec_template where pec_template.id_user=" . $dbconn->sqlInteger($this->user->id);
			$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
			if ($record && trim($record->message_subject_template)) 
				{
				$ret = $record->message_subject_template;
				$this->sessionData->message_subject_template = $record->message_subject_template;
				$this->sessionData->message_template = $record->message_template;
				}
			}
		return trim($ret) . " [_FTS_.$hit->id_user.$hit->registered_at]";
		
		}

	//***************************************************************************
	function getPecBody(waLibs\waRecord $hit)
		{
		$message = APPL_PEC_MSG_TEMPLATE;
		$dbconn = $hit->recordset->dbConnection;
		if ($this->sessionData->message_template)
			{
			$message = $this->sessionData->message_template;
			}
		else 
			{
			// inefficiente!!
			$sql = "select * from pec_template where pec_template.id_user=" . $dbconn->sqlInteger($this->user->id);
			$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
			if ($record && trim($record->message_template)) 
				{
				$message = $record->message_template;
				$this->sessionData->message_template = $record->message_template;
				}
			}
		$message = str_replace("[data]", date("d/m/Y", $hit->registered_at), $message);
		$message = str_replace("[ora]", date("H:i", $hit->registered_at), $message);
		$message = str_replace("[indirizzo]", $hit->address, $message);
		$message = trim($message);
		$message = str_replace("[intensita]", $hit->id_intensity ? "Intensità: " . $this->getIntensityDescription($hit->id_intensity, $dbconn) : "", $message);
		$message = str_replace("[durata]",    $hit->id_duration ?  "Durata   : " . $this->getDurationDescription($hit->id_duration, $dbconn) : "", $message);
		$message_notes = trim($hit->message_notes);
		$message = str_replace("[note_messaggio]", $message_notes, $message);
		$message = trim($message);
		$message = str_replace("\r\n", "\n", $message);
		while (strpos($message, "\n\n\n") !== false)
			{
			$message = str_replace("\n\n\n", "\n\n", $message);
			}
		
		return $message . $this->getPecFooter();
		
		}

	//***************************************************************************
	function getIntensityDescription($id_intensity, waLibs\waDBConnection $dbconn = null)
		{
		$ret = "";
		if ($id_intensity)
			{
			$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
			$sql = "select description from intensity where id=" . $dbconn->sqlInteger($id_intensity);
			$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
			$ret = $record ? $record->description : "";		
			}
		return $ret;
		}
		
	//***************************************************************************
	function getDurationDescription($id_duration, waLibs\waDBConnection $dbconn = null)
		{
		$ret = "";
		if ($id_duration)
			{
			$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
			$sql = "select description from duration where id=" . $dbconn->sqlInteger($id_duration);
			$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
			$ret = $record ? $record->description : "";		
			}
		return $ret;
		}
		
	//***************************************************************************
	function getPecFooter()
		{
		$footer = "\n\n" . APPL_PEC_MSG_FOOTER;
		$footer = str_replace("[nome]", $this->user->first_name, $footer);
		$footer = str_replace("[cognome]", $this->user->last_name, $footer);
		$footer = str_replace("[email]", $this->user->email, $footer);
		if (trim($this->user->phone)) 
			{
			$footer = str_replace("[telefono]", $this->user->phone, $footer);
			}
		else
			{
			$footer = str_replace("telefono: [telefono]\n", "", $footer);
			
			}
		$footer = str_replace("[titolo_applicazione]", $this->title, $footer);
		$footer = str_replace("[indirizzo_applicazione]", $this->getProtocol() . "://$this->domain$this->httpwd/home.php", $footer);
		
		return $footer;
		
		}

	//***************************************************************************
	function getPecPreview(waLibs\waRecord $hit)
		{
		$from = unserialize(APPL_PEC_FROM);
		$text = "<pre>";
		$text .= "Da: $from[name] [$from[address]]\n";
		$tos = unserialize(APPL_PEC_TO);
		foreach($tos as $to) {
			$text .= "A : $to[name] [$to[address]]\n";
		}
		$ccs = unserialize(APPL_PEC_CC);
		foreach($ccs as $cc) {
			$text .= "CC: $cc[name] [$cc[address]]\n";
		}
		$text .= "CC: " . $this->user->first_name . " " . $this->user->last_name . " [" . $this->user->email . "]\n";
		
		$text .= "Oggetto: " . $this->getPecSubject($hit) . "\n\n";
		$text .= $this->getPecBody($hit);
		$text .= "</pre>";
		
		return $text;
		}

	//***************************************************************************
	function setHitWeather(waLibs\waRecord $hit)
		{
		try 
			{
			$url = "https://api.openweathermap.org/data/2.5/onecall/timemachine" .
					"?lat=" . $hit->latitude .
					"&lon=" . $hit->longitude .
					"&dt=" . $hit->registered_at .
					"&appid=31ad471b469102e8a61333561c1d1496" .
					"&units=metric";
			$result = $this->url_get_contents($url);
			// del risultato dobbiamo prendere solo l'oggetto current
			$result = json_decode($result);
			$hit->weather_blob = json_encode($result->current, JSON_PRETTY_PRINT);
			}
		catch (Exception $e) 
			{
			// qualsiasi cosa succeda ce ne fottiamo, è troppo importante salvare 
			// la segnalazione
			}
		}

	//***************************************************************************
	function sendPec(waLibs\waRecord $record)
		{
		
		$mail = new PHPMailer(true);
		$mail->CharSet = "UTF8";
		$mail->SMTPAutoTLS = false;
		$mail->Timeout = 10;
		$mail->Mailer = "smtp";
		$mail->Host = APPL_PEC_SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Username = APPL_PEC_SMTP_USER;
		$mail->Password = APPL_PEC_SMTP_PWD;
		$mail->SMTPSecure = APPL_PEC_SMTP_SECURE;
		$mail->Port = APPL_PEC_SMTP_PORT;
		$from = unserialize(APPL_PEC_FROM);
		$mail->From = $from["address"];
		$mail->FromName = $from["name"];
		$tos = unserialize(APPL_PEC_TO);
		foreach($tos as $to) {
			$mail->addAddress($to["address"], $to["name"]);
		}
		$ccs = unserialize(APPL_PEC_CC);
		foreach($ccs as $cc) {
			$mail->addCC($cc["address"], $cc["name"]);
		}
		$mail->addCC($this->user->email, $this->user->first_name . " " . $this->user->last_name);
		$mail->Subject = $this->getPecSubject($record);
		$mail->Body = $this->getPecBody($record);
		$esito = false;
		try 
			{
			$esito = @$mail->Send();
			$uzza = 4;
			}
		catch (phpmailerException $e)
			{
			}
		catch (Exception $e)
			{
			throw $e;
			}
			
		if (!$esito)
			{
			$this->showMessage("Errore invio messaggio email", 
								"Si è verificato un errore durante l'invio del messaggio di posta elettronica." .
								" Sei pregato di cercare di ripetere l'operazione o di avvertire " .
								"<a href='mailto:" . $this->supportEmail->address . "'>l'assistenza tecnica</a>.");
			}
		
		return $mail->getLastMessageID();
		}
	
	//***************************************************************************
	// restituisce l'ora dell'ultima segnalazione fatta dall'utente
	function canUserSendPec(waLibs\waDBConnection $dbconn = null)
		{
		$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
		$sql = "select registered_at" .
				" from hit" .
				" where id_user=" . $dbconn->sqlInteger($this->user->id) . 
				" and is_mail_sent" .
				" and not is_deleted" .
				" order by registered_at desc";
		$hit = $this->getRecordset($sql, $dbconn, 1)->records[0];
		$last = $hit ? $hit->registered_at : 0;
		return $last + APPL_NEW_HIT_TIME_THRESHOLD > mktime() ? false : true;
		
		}		
		

	//**************************************************************************
	} 	// fine classe
	
//***************************************************************************

