<?php
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			$this->deleteRecord($this->form->record);
			unset($this->sessionData->message_subject_template);
			unset($this->sessionData->message_template);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Modello PEC", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		//----------------------------------------------------------------------
		$this->form->addText("message_subject_template", "Oggetto", $readOnly, !$readOnly)->value = APPL_PEC_SUBJECT_TEMPLATE;
		$this->form->addTextArea("message_template", "Corpo", $readOnly, !$readOnly)->value = APPL_PEC_MSG_TEMPLATE;
		$ctrl = $this->form->addTextArea("message_footer", "Piede", true);
			$ctrl->dbBound = false;
			$ctrl->value = $this->getPecFooter();
		
		$this->form_submitButtons($this->form, false, false);
		$preview = new waLibs\waButton($this->form, 'cmd_preview', "Anteprima");
			$preview->cancel = false;
			$preview->submit = false;
		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select *" .
				" from pec_template" .
				" where pec_template.id_user=" . $dbconn->sqlInteger($this->user->id) . 
				" and not is_deleted";
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);

		// verifichiamo che ci siano i pattern obbligatori
		$message_template = $this->form->message_template;
		if (strpos($message_template, "[data]") === false || 
				strpos($message_template, "[ora]") === false || 
				strpos($message_template, "[indirizzo]") === false)
			{
			$this->showMessage("Mancano segnaposto", "Operazione non permessa: i segnaposto [data] [ora] [indirizzo] sono obbligatori", true, false);
			}
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->id_user = $this->user->id;
			}
		else 
			{
			$this->checkLockViolation($this->form);
			}
			
		$this->setEditorData($record);
		$this->form->save();
		$this->saveRecordset($record->recordset);
		
		$this->sessionData->message_subject_template = $record->message_subject_template;
		$this->sessionData->message_template = $record->message_template;

		$this->response();
		}
		
	//*****************************************************************************
	function rpc_getPreview()
		{
		// creiamo un finto hit per costruire il body
		$hit = $this->getRecordset("select * from hit", $this->form->recordset->dbConnection, 0)->add();
		$hit->id_user = $this->user->id;
		$hit->address = $this->user->address;
		$hit->registered_at = mktime();
		return $this->getPecPreview($hit);
		
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
