<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		if (!$this->user->is_sys_admin) 
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, true);
			}
		
		$this->childWindow = true;
		$user = $this->getTitleContext("user", "concat(last_name, ' ', first_name)");
		$this->addItem("Accessi $user", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT access.*" .
				" FROM access" .
				
				" where id_user=" . $dbconn->sqlInteger($_GET["id_user"]) .
				" and not access.is_deleted" .
				" and not access.flag_out" .
				" order by access.inserted_at desc";

		$table = parent::getTable($sql);
		
		//----------------------------------------------------------------------
		// azioni...
		$table->removeAction("New");
		$table->removeAction("Edit");
		$table->removeAction("Delete");

		//----------------------------------------------------------------------
		// colonne...
		$table->addColumn("id", "ID")->aliasOf = "access.id";
		$table->addColumn("inserted_at", "Data/Ora")->aliasOf = "access.inserted_at";
		$table->addColumn("ip_user_edit", "IP")->aliasOf = "access.ip_user_edit";

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();