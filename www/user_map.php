<?php
//******************************************************************************
include "followthesmell.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		

	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);

		$this->addItem($this->getMenu());

		$this->addItem("Mappa utenti", "title");
		$this->addItem($this->getList(), "user_list");
		$this->show();

		}

	//**************************************************************************
	function getList()
		{
		$dbconn = $this->getDBConnection();
		// cerchiamo i points
		$sql = "SELECT id, first_name, last_name, address, latitude, longitude" .
				" FROM user" .
				" where not is_deleted" .
				" and latitude is not null and latitude != ''" .
				" and longitude is not null and longitude != ''";
				 
		$users = $this->getRecordset($sql, $dbconn, 2000);

		$list = [];
		$places = [];
		foreach ($users->records as $user)
			{
			while ($places["$user->latitude$user->longitude"])
				{
				$offset = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$offset2 = (rand(0,1000)/10000000) * (rand(0,1) % 2 == 0 ? 1 : -1);
				$user->latitude += $offset;
				$user->longitude += $offset2;
				}
			$places["$user->latitude$user->longitude"] = 1;
			$list[] = $this->record2Object($user);
			}

		return $list;
		}

	//*****************************************************************************
	}

// fine classe page
//*****************************************************************************
// istanzia la pagina
new page();
