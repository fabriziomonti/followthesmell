<?php
//*****************************************************************************
include "followthesmell.inc.php";

//*****************************************************************************
class page extends followthesmell
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct()
		{
		parent::__construct(true);
		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		if (!$stepsData || !$stepsData->step_4)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false, false);
			}
		
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->goAhead();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Invia una segnalazione a ARPAE via PEC - 5\nAggiungi informazioni e note opzionali al messaggio", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;
		
		$this->form->addOption("id_intensity", "Intensità", $readOnly, !$readOnly)->list = $this->getIntensityList();
		$this->form->addOption("id_duration", "Durata", $readOnly, !$readOnly)->list = $this->getDurationList();
		$this->form->addTextArea("message_notes", "Note da aggiungere al messaggio", $readOnly);

		$ctrl = $this->form->addButton("cmd_back", "<< Indietro");
			$ctrl->submit = false;
		$ctrl = $this->form->addButton("cmd_cancel", "Annulla");
			$ctrl->cancel = true;
			$ctrl->submit = false;
		$this->form->addButton("cmd_submit", "Avanti >>");

		$this->form->getInputValues();
		}

	//***************************************************************************
	function goAhead()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$stepsData = &$this->sessionData["hit_wiz_steps_data"];
		
		// aggiorniamo l'utente
		$dbconn = $this->getDBConnection();
		$dbconn->beginTransaction();

		$stepsData->ready = true;
		$stepsData->message_notes = $this->form->message_notes;
		$stepsData->id_intensity = $this->form->id_intensity;
		$stepsData->id_duration = $this->form->id_duration;
		// andiamo allo step successivo
		$this->redirect("hit_wiz_5.php");
			
		}

	//***************************************************************************
	function getIntensityList()
		{
		$ret = [];
		$dbconn = $this->getDBConnection();
		$sql = "select id, description from intensity where not is_deleted order by ordinal";
		$rs = $this->getRecordset($sql, $dbconn);
		foreach ($rs->records as $rec)
			{
			$ret[$rec->id] = $rec->description;
			}
		return $ret;
		}
		
	//***************************************************************************
	function getDurationList()
		{
		$ret = [];
		$dbconn = $this->getDBConnection();
		$sql = "select id, description from duration where not is_deleted order by ordinal";
		$rs = $this->getRecordset($sql, $dbconn);
		foreach ($rs->records as $rec)
			{
			$ret[$rec->id] = $rec->description;
			}
		return $ret;
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
