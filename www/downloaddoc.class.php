<?php
//*****************************************************************************

class downloaddoc
	{

	//***************************************************************************
	function __construct(followthesmell $app)
		{
		$params = unserialize(base64_decode($_GET["p"]));
		
		if (isset($params["n"]))
			{
			$session_id = openssl_decrypt(base64_decode($params["n"]), 'aes-256-cbc', APPL_ENCRYPTION_IV, 0, APPL_ENCRYPTION_IV);
			if ($session_id != session_id())
				{
				exit();
				}
			}
			
		$filename = "$app->directoryDoc/$params[t]/$params[c]/$params[k].$params[e]";

		if (!is_readable($filename)) return;
		
		if ($_GET["round"])
			{
			$this->roundThumb($filename);
			}

		$basename = basename($filename);

		header("Pragma: ");
		header("Expires: Fri, 15 Aug 1980 18:15:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0, false");
		if (function_exists("mime_content_type"))
			{
			$mime = mime_content_type($filename);
			}
		else
			{
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			$mime = $finfo->file($filename);
			}

		if ($mime)
			{
			header("Content-Type: $mime");
			header("Content-Disposition: inline; filename=\"$basename\"");
			}
		else
			{
			header("Content-Disposition: attachment; filename=\"$basename\";");
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: binary");
			}

		$w = intval($_GET["w"]);
		if ($w)
			{
			// ridimensione immagine
			$this->createthumb($filename, $w);
			}
		else
			{
			// mandiamo il content-length solo se non dobbiamo ridimentsionare
			header('Content-Length: '. filesize($filename));
			readfile($filename);
			}
		}

	//***************************************************************************
	function roundThumb($filename)
		{
		$image_s = imagecreatefromjpeg($filename);

		$width = imagesx($image_s);
		$height = imagesy($image_s);

		$w = intval($_GET["w"]);
		$newwidth = $w ? $w : 80;
		$newheight = $newwidth;

		$image = imagecreatetruecolor($newwidth, $newheight);
		imagealphablending($image,true);
		imagecopyresampled($image,$image_s,0,0,0,0,$newwidth,$newheight,$width,$height);

		// create masking
		$mask = imagecreatetruecolor($newwidth, $newheight);

		$transparent = imagecolorallocate($mask, 255, 0, 0);
		imagecolortransparent($mask, $transparent);

		imagefilledellipse($mask, $newwidth/2, $newheight/2, $newwidth, $newheight, $transparent);

		$red = imagecolorallocate($mask, 0, 0, 0);
		imagecopy($image, $mask, 0, 0, 0, 0, $newwidth, $newheight);
		// imagecopymerge($image, $mask, 0, 0, 0, 0, $newwidth, $newheight,100);
		imagecolortransparent($image, $red);
		imagefill($image,0,0, $red);

		// output and free memory
		header('Content-type: image/png');
		imagepng($image);
		imagedestroy($image);
		imagedestroy($mask);	
		
		exit();
		}
		
	//*****************************************************************************
	// $thumb_y di fatto non è mai usato, comanda sempre il width e l'height
	// è riproporzionato di conseguenza; è lasciato per eventuali sviluppi futuri
	function createthumb($image_source, $thumb_x, $thumb_y = 0)
		{
		$thumb_x = (int) $thumb_x;
		$thumb_y = (int) $thumb_y;
		
		if (!$thumb_y && !$thumb_x) 
			return readfile($image_source);

		$image_properties = getimagesize($image_source);
		if ($image_properties[2] != IMAGETYPE_JPEG) 
			return readfile($image_source);
		
		$src_image = imagecreatefromjpeg($image_source);
		$src_x = $image_properties[0];
		$src_y = $image_properties[1];
		
		if ($thumb_x > APPL_IMG_MAX_WIDTH)
			$thumb_x = APPL_IMG_MAX_WIDTH;
		
		if (!$thumb_y) 
			{
			$scalex = $thumb_x/($src_x-1);
			if ($scalex >= 1) 
				return readfile($image_source);
			$thumb_y = intval($src_y*$scalex);
			} 
		elseif (!$thumb_x) 
			{
			$scaley = $thumb_y/($src_y-1);
			if ($scaley >= 1) 
				return readfile($image_source);
			$thumb_x = intval($src_x*$scaley);
			}
			
			
		$dest_image = imagecreatetruecolor($thumb_x, $thumb_y);

		if (imagecopyresampled($dest_image, $src_image, 0, 0, 0, 0, $thumb_x, $thumb_y, $src_x, $src_y)) 
			{
			imagejpeg($dest_image, null, 100);
			}
		else
			readfile($image_source);

		imagedestroy($src_image);
		imagedestroy($dest_image);
			

		} # end createthumb

	//*****************************************************************************
	}

