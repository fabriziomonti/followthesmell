SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE access (
  id int(11) NOT NULL,
  id_user int(11) NOT NULL,
  section varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  flag_out int(1) NOT NULL DEFAULT 0,
  ip varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  notes text CHARACTER SET utf8 DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE city (
  id int(11) NOT NULL,
  id_province int(11) NOT NULL,
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  short_name varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  time_zone varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE country (
  id int(11) NOT NULL,
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  short_name varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sigla',
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE doc (
  id int(11) NOT NULL,
  id_user int(11) NOT NULL,
  doc varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  link text COLLATE utf8_unicode_ci DEFAULT NULL,
  title varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  abstract text COLLATE utf8_unicode_ci DEFAULT NULL,
  registered_at datetime NOT NULL DEFAULT current_timestamp(),
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE duration (
  id int(11) NOT NULL,
  ordinal int(1) NOT NULL,
  name varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  description varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  notes text CHARACTER SET utf8 DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO duration (id, ordinal, `name`, description, notes, is_deleted, inserted_at, edited_at, id_user_edit, ip_user_edit) VALUES
(1, 1, 'breve', 'breve - da 10 a 30 minuti.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(2, 2, 'media', 'media - da 30 minuti a 1 ora.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(3, 3, 'lunga', 'lunga - oltre 1 ora.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '');

CREATE TABLE `help` (
  id int(11) NOT NULL,
  section varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  page varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  control varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  text longtext COLLATE utf8_unicode_ci NOT NULL,
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE hit (
  id int(11) NOT NULL,
  id_user int(11) NOT NULL,
  doc varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  address text COLLATE utf8_unicode_ci DEFAULT NULL,
  address_blob text COLLATE utf8_unicode_ci DEFAULT NULL,
  latitude decimal(9,6) DEFAULT NULL,
  longitude decimal(9,6) DEFAULT NULL,
  id_city int(11) DEFAULT NULL,
  registered_at datetime NOT NULL DEFAULT current_timestamp(),
  is_mail_sent int(1) NOT NULL DEFAULT 0,
  id_intensity int(11) DEFAULT NULL,
  id_duration int(11) DEFAULT NULL,
  message_notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  message_id varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  weather_blob text COLLATE utf8_unicode_ci DEFAULT NULL,
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE intensity (
  id int(11) NOT NULL,
  ordinal int(1) NOT NULL,
  name varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  description varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  notes text CHARACTER SET utf8 DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO intensity (id, ordinal, `name`, description, notes, is_deleted, inserted_at, edited_at, id_user_edit, ip_user_edit) VALUES
(1, 1, 'bassa', 'bassa - odore debole, non persistente, appena percettibile.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(2, 2, 'media', 'media - odore chiaramente fastidioso, ma non da far venire il voltastomaco.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(3, 3, 'alta', 'alta - odore acre e/o intenso come il pattume che sta lì da giorni e nauseante.', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '');

CREATE TABLE meteo (
  id int(11) NOT NULL,
  fellow varchar(20) NOT NULL DEFAULT 'sebastiano',
  date_time datetime DEFAULT NULL,
  temp_out decimal(3,1) DEFAULT NULL,
  hi_temp decimal(3,1) DEFAULT NULL,
  low_temp decimal(3,1) DEFAULT NULL,
  out_hum int(2) DEFAULT NULL,
  dew_pt decimal(3,1) DEFAULT NULL,
  wind_speed decimal(3,1) DEFAULT NULL,
  wind_dir varchar(3) DEFAULT NULL,
  wind_run decimal(4,2) DEFAULT NULL,
  hi_speed decimal(3,1) DEFAULT NULL,
  hi_dir varchar(3) DEFAULT NULL,
  wind_chill decimal(3,1) DEFAULT NULL,
  heat_index decimal(3,1) DEFAULT NULL,
  thw_index decimal(3,1) DEFAULT NULL,
  bar decimal(5,1) DEFAULT NULL,
  rain decimal(3,1) DEFAULT NULL,
  rain_rate decimal(4,1) DEFAULT NULL,
  heat_d_d decimal(4,3) DEFAULT NULL,
  cool_d_d decimal(4,3) DEFAULT NULL,
  in_temp decimal(3,1) DEFAULT NULL,
  int_hum int(2) DEFAULT NULL,
  in_dew decimal(3,1) DEFAULT NULL,
  in_heat decimal(3,1) DEFAULT NULL,
  in_emc decimal(4,2) DEFAULT NULL,
  in_air_density decimal(5,4) DEFAULT NULL,
  wind_samp int(3) DEFAULT NULL,
  wind_tx int(1) DEFAULT NULL,
  iss_receipt decimal(4,1) DEFAULT NULL,
  arc_int int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE pec_template (
  id int(11) NOT NULL,
  id_user int(11) NOT NULL,
  message_template text COLLATE utf8_unicode_ci DEFAULT NULL,
  message_subject_template varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE province (
  id int(11) NOT NULL,
  id_region int(11) NOT NULL,
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  short_name varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sigla',
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE receipt (
  id int(11) NOT NULL,
  id_hit int(11) NOT NULL,
  message_id varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  doc varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  sender varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  subject varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  registered_at datetime NOT NULL DEFAULT current_timestamp(),
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE region (
  id int(11) NOT NULL,
  id_country int(11) NOT NULL,
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  short_name varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sigla',
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user` (
  id int(11) NOT NULL,
  pwd varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  email varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  first_name varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  last_name varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  image varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  phone varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  address text COLLATE utf8_unicode_ci DEFAULT NULL,
  address_blob text COLLATE utf8_unicode_ci DEFAULT NULL,
  latitude decimal(9,6) DEFAULT NULL,
  longitude decimal(9,6) DEFAULT NULL,
  id_city int(11) DEFAULT NULL,
  is_sys_admin int(1) NOT NULL DEFAULT 0,
  is_admin int(1) NOT NULL DEFAULT 0,
  registered_at datetime NOT NULL DEFAULT current_timestamp(),
  notes text COLLATE utf8_unicode_ci DEFAULT NULL,
  is_deleted int(1) DEFAULT 0,
  inserted_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  edited_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  id_user_edit int(11) NOT NULL DEFAULT 0,
  ip_user_edit varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE access
  ADD PRIMARY KEY (id),
  ADD KEY id_user (id_user),
  ADD KEY section (section);

ALTER TABLE city
  ADD PRIMARY KEY (id),
  ADD KEY id_province (id_province);

ALTER TABLE country
  ADD PRIMARY KEY (id),
  ADD KEY userId (name);

ALTER TABLE doc
  ADD PRIMARY KEY (id),
  ADD KEY id_user (id_user),
  ADD KEY registered_at (registered_at);

ALTER TABLE duration
  ADD PRIMARY KEY (id);

ALTER TABLE `help`
  ADD PRIMARY KEY (id),
  ADD KEY URI (section);

ALTER TABLE hit
  ADD PRIMARY KEY (id),
  ADD KEY id_user (id_user),
  ADD KEY registered_at (registered_at),
  ADD KEY message_id (message_id);

ALTER TABLE intensity
  ADD PRIMARY KEY (id);

ALTER TABLE meteo
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY date_time (date_time,fellow) USING BTREE;

ALTER TABLE pec_template
  ADD PRIMARY KEY (id),
  ADD KEY id_user (id_user);

ALTER TABLE province
  ADD PRIMARY KEY (id),
  ADD KEY id_region (id_region);

ALTER TABLE receipt
  ADD PRIMARY KEY (id),
  ADD KEY id_hit (id_hit) USING BTREE,
  ADD KEY message_id (message_id);

ALTER TABLE region
  ADD PRIMARY KEY (id),
  ADD KEY id_country (id_country);

ALTER TABLE `user`
  ADD PRIMARY KEY (id),
  ADD KEY email (email) USING BTREE;


ALTER TABLE access
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE city
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE country
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE doc
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE duration
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `help`
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE hit
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE intensity
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE meteo
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE pec_template
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE province
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE receipt
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE region
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `user`
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
